/**
 * @format
 */

import 'react-native-gesture-handler';

import React from 'react';

import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {ThemeProvider} from 'styled-components';
import theme from './src/data/theme';
import {NavigationContainer} from '@react-navigation/native';
import Consumer, { ConfigProvider } from './src/components/Global/Contexts/ConfigContext'
import TasksModal from './src/screens/tasks/TasksModal';

const linking = {
  prefixes: ['cosoapp://'],
  config: {
    AuthLoading:{
      screens:{
        ChangePassword: 'api/password/edit'
      }
    }
  },
};

const Root = (props) =>  {

  return (
    <ConfigProvider>
      <NavigationContainer linking={linking}>
        {/* Can delete the ThemePRovider component and wrap App wirh the Navigation Container */}
        <ThemeProvider theme={theme}>
          <App/>
        </ThemeProvider>
      </NavigationContainer>
    </ConfigProvider>
  );


};

AppRegistry.registerComponent(appName, () => Root);
