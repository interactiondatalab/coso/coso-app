/**
 * @format
 */

import React, {useEffect} from 'react';
import MainNavigation from './navigation/MainNavigation';
import SplashScreen from 'react-native-splash-screen'
import { Platform, StatusBar } from 'react-native';

//DEV
console.disableYellowBox = true;

const App = () => {
  useEffect(()=>{
    SplashScreen.hide();
  })
  return (
    <>
      {/* Hide at launch of the app */}
      <StatusBar hidden={true}/>
      <MainNavigation />
    </>
  )
};

export default App;
