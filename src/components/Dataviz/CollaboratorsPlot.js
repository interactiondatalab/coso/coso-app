import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { useFocusEffect } from '@react-navigation/native'
import { VictoryBar, VictoryChart, VictoryTheme, VictoryAxis } from "victory-native";
import { getStatsCollaborators } from '../../services/api/stats/functions';
import colors from '../../global/constants';


export default function CollaboratorsPlot({props}) {
  const [data, setData] = useState([])
  const [totalCollaborators, setTotalCollaborators] = useState(0)

  function refreshData() {
    getStatsCollaborators().then(
      res=>{
      if (res.status===200) {
        const data = res.data.collaborators;
        data.sort(function (a, b) {
          return a.count - b.count;
        })

        setData(data.slice(-5));
        setTotalCollaborators(res.data.collaborators.length)
      }
    })
  }

  useFocusEffect(
    React.useCallback(() => {
      refreshData();
    }, [totalCollaborators])
  )

  useEffect(() => {
    refreshData();
  }, []);

  return data ? (
    <View style={styles.container}>
      <Text style={styles.description}>
        Total number of collaborators: <Text style={styles.bold}>{totalCollaborators}</Text>
      </Text>
      {data.length > 0 &&
        <View>
          <Text style={styles.plotTitle}>
            Your top 5 collaborators
          </Text>
          <VictoryChart>
            <VictoryBar
              horizontal
              data={data}
              labels={({ datum }) => datum.count}
              padding={{ top: 0, bottom: 0, left: 0, right: 0 }}
              style={{ data: { fill: colors.primaryButton } }}
              scale={{x: "linear", y: "log"}}
              x="username"
              y="count" />
              <VictoryAxis style={{
                  axis: {stroke: "transparent"},
                  ticks: {stroke: "transparent"},
                  tickLabels: { fill:"transparent"}
              }} />
              {
                data.map((d, i) => {
                return (
                  <VictoryAxis dependentAxis
                    key={i}
                    label={d.username}
                    orientation="top"
                    style={{
                      axis: {stroke: "transparent"},
                      axisLabel: {fontSize: 17, fill: "#666", padding: 15, },
                      tickLabels: { fill: "none" } }}
                    axisValue={d.username}

                  />
                );
              })
            }
          </VictoryChart>
        </View>
      }
    </View>
  ) : (
    <View style={styles.container}>
      Loading ...
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#f5fcff"
  },
  description: {
    fontFamily: 'Montserrat-Regular',
    // paddingHorizontal: 50,
    marginTop: 20,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17
  },
  bold: {
    fontWeight: "bold",
    fontSize: 20,
    color: colors.primaryButton,
  },
  plotTitle: {
    fontFamily: 'Montserrat-Regular',
    // paddingLeft: 50,
    fontWeight: 'bold',
    marginTop: 40,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 22
  },
});
