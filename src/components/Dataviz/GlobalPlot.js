import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { useFocusEffect } from '@react-navigation/native'
import { VictoryBar, VictoryChart, VictoryTheme, VictoryAxis } from "victory-native";
import { getStatsGlobal } from '../../services/api/stats/functions';
import colors from '../../global/constants';

export default function GlobalPlot({props}) {
  const [data, setData] = useState([])
  const [totalTasks, setTotalTasks] = useState(0)
  const [totalTeams, setTotalTeams] = useState(0)
  const [totalCollaborations, setTotalCollaborations] = useState(0)
  const [topFiveUser, setTopFiveUser] = useState([])

  function refreshData() {
    getStatsGlobal().then(
      res=>{
        console.log(res.data.top5_streak)
      if (res.status===200) {
        setTotalTasks(res.data.logs);
        setTotalTeams(res.data.teams);
        setTotalCollaborations(res.data.collaborations);
        const data = res.data.top5_streak;

        data.sort(function (a, b) {
              return b.longest_streak - a.longest_streak;
          }
        ).reverse()
        setTopFiveUser(data);
      }
    })
  }

  useFocusEffect(
    React.useCallback(() => {
      refreshData();
    }, [totalTasks])
  )


  useEffect(() => {
    refreshData();
  }, []);

  return data ? (
    <View style={styles.container}>
      <Text style={styles.description}>
        There are <Text style={styles.bold}>{totalTeams}</Text> teams using CoSo!
      </Text>
      <Text style={styles.description}>
        There has been <Text style={styles.bold}>{totalTasks}</Text> tasks recorded!
      </Text>
      <Text style={styles.description}>
        We recorded <Text style={styles.bold}>{totalCollaborations}</Text> collaborations!
      </Text>
      <View>
        <Text style={styles.plotTitle}>
          Top 5 streaks
        </Text>
        <VictoryChart>
          <VictoryBar
            horizontal
            data={topFiveUser}
            labels={({ datum }) => datum.longest_streak}
            padding={{ top: 0, bottom: 0, left: 0, right: 0 }}
            style={{ data: { fill: colors.primaryButton } }}
            scale={{x: "linear", y: "log"}}
            x="username"
            y="longest_streak" />
            <VictoryAxis style={{
                axis: {stroke: "transparent"},
                ticks: {stroke: "transparent"},
                tickLabels: { fill:"transparent"}
            }} />
            {
              topFiveUser.map((d, i) => {
              return (
                <VictoryAxis dependentAxis
                  key={i}
                  label={d.username}
                  orientation="top"
                  style={{
                    axis: {stroke: "transparent"},
                    axisLabel: {fontSize: 17, fill: "#666", padding: 15, },
                    tickLabels: { fill: "none" } }}
                  axisValue={d.username}

                />
              );
            })
          }
        </VictoryChart>
      </View>

    </View>
  ) : (
    <View style={styles.container}>
      Loading ...
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#f5fcff"
  },
  description: {
    fontFamily: 'Montserrat-Regular',
    // paddingHorizontal: 50,
    marginTop: 20,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17
  },
  bold: {
    fontWeight: "bold",
    fontSize: 20,
    color: colors.primaryButton,
  },
  plotTitle: {
    fontFamily: 'Montserrat-Regular',
    // paddingLeft: 50,
    fontWeight: 'bold',
    marginTop: 40,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 22
  },
});
