import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text, Alert } from "react-native";
import { VictoryPie, VictoryChart, VictoryTheme, VictoryAxis, VictoryLabel } from "victory-native";
import { getStatsStreak } from '../../services/api/stats/functions';
import colors from '../../global/constants';
import { useFocusEffect } from '@react-navigation/native'

export default function StreakPlot({props}) {
  const [longestStreak, setLongestStreak] = useState(0)
  const [currentStreak, setCurrentStreak] = useState(0)
  const [data, setData] = useState([])

  function refreshData() {
    getStatsStreak().then(
      res=>{
      if (res.status===200) {
        console.log('data',res.data)
        setLongestStreak(res.data.longest_streak)
        setCurrentStreak(res.data.current_streak)
        var percent = 5;
        if (longestStreak > 0) {
          if (currentStreak > 0) {
            percent = 100*(currentStreak/longestStreak);
          }
        }
        const data = [{x: 1, y: percent}, {x: 2, y: 100 - percent}]
        setData(data);
      }
    })
  }

  useFocusEffect(
    React.useCallback(() => {
      refreshData();
    }, [currentStreak])
  )

  useEffect(() => {
    refreshData();
  }, []);

  return data ? (
    <View style={styles.container}>
      <Text style={styles.plotTitle}>
        { currentStreak > 0 ? "You are on a roll!" : "Start your streak!"}
      </Text>
      <Text style={styles.description}>Log tasks for consecutive days to increase your streak</Text>
      <Text style={styles.description}>
        Your longest streak is <Text style={styles.bold}>{longestStreak}</Text> days! {currentStreak != longestStreak && "\nCan you beat it?"}
      </Text>
      <View>
        <VictoryChart width={400} height={400}>
          <VictoryPie
          standalone={false}
          data={[{x: 1, y: 100*(currentStreak/longestStreak)}, {x: 2, y: 100 - (100*(currentStreak/longestStreak))}]}
          width={400} height={400}
          innerRadius={120}
          cornerRadius={0}
          labels={() => null}
          style={{
            data: { fill: ({ datum }) => {
              if (datum.x === 1) {
                if (datum.y > 99) {
                  return "gold"
                }
                else if (datum.y === 5) {
                  return "red"
                }
                else {
                  return colors.primaryButton;
                }
              }
              else {
                return "lightgrey";
              }
            }
            }
          }}
          />
          <VictoryLabel
                  textAnchor="middle" verticalAnchor="middle"
                  x={200} y={180}
                  text={`Current Streak`}
                  style={{ fontSize: 25 }}
          />
          <VictoryLabel
                  textAnchor="middle" verticalAnchor="middle"
                  x={200} y={220}
                  text={currentStreak}
                  style={{ fontSize: 40, fontWeight: 'bold',  color: colors.primaryButton}}
          />
          <VictoryAxis style={{
              axis: {stroke: "transparent"},
              ticks: {stroke: "transparent"},
              tickLabels: { fill:"transparent"}
          }} />

        </VictoryChart>
      </View>

    </View>
  ) : (
    <View style={styles.container}>
      Loading ...
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#f5fcff"
  },
  description: {
    fontFamily: 'Montserrat-Regular',
    // paddingHorizontal: 50,
    marginTop: 20,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17
  },
  bold: {
    fontWeight: "bold",
    fontSize: 20,
    color: colors.primaryButton,
  },
  plotTitle: {
    fontFamily: 'Montserrat-Regular',
    // paddingLeft: 50,
    fontWeight: 'bold',
    marginTop: 40,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 22
  },
});
