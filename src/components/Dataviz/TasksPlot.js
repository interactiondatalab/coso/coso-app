import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import { useFocusEffect } from '@react-navigation/native'
import { VictoryBar, VictoryChart, VictoryTheme, VictoryAxis } from "victory-native";
import { getStatsTasks } from '../../services/api/stats/functions';
import colors from '../../global/constants';

export default function TasksPlot({props}) {
  const [data, setData] = useState([])
  const [totalTasks, setTotalTasks] = useState(0)

  function refreshData() {
    getStatsTasks().then(
      res=>{
      if (res.status===200) {
        const data = res.data.tasks_counts;
        data.sort(function (a, b) {
          return a.count - b.count;
        })
        setData(data.slice(-5));
        console.log("ici")
        console.log(res.data.total_tasks)
        setTotalTasks(res.data.total_tasks)
      }
    })
  }

  useFocusEffect(
    React.useCallback(() => {
      refreshData();
    }, [totalTasks])
  )

  useEffect(() => {
    refreshData();
  }, []);


  return data ? (
    <View style={styles.container}>
      <Text style={styles.description}>
        Total number of logs: <Text style={styles.bold}>{totalTasks}</Text>
      </Text>
      {data.length > 0 &&
      <View>
        <Text style={styles.plotTitle}>
          Your top 5 tasks
        </Text>
        <VictoryChart>
          <VictoryBar
            horizontal
            data={data}
            labels={({ datum }) => datum.count}
            padding={{ top: 0, bottom: 0, left: 0, right: 0 }}
            style={{ data: { fill: colors.primaryButton } }}
            scale={{x: "linear", y: "log"}}
            x="name"
            y="count" />
            <VictoryAxis style={{
                axis: {stroke: "transparent"},
                ticks: {stroke: "transparent"},
                tickLabels: { fill:"transparent"}
            }} />
            {
              data.map((d, i) => {
              return (
                <VictoryAxis dependentAxis
                  key={i}
                  label={d.name}
                  orientation="top"
                  style={{
                    axis: {stroke: "transparent"},
                    axisLabel: {fontSize: 17, fill: "#666", padding: 15, },
                    tickLabels: { fill: "none" } }}
                  axisValue={d.name}

                />
              );
            })
          }
        </VictoryChart>
      </View>
      }

    </View>
  ) : (
    <View style={styles.container}>
      Loading ...
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#f5fcff"
  },
  description: {
    fontFamily: 'Montserrat-Regular',
    // paddingHorizontal: 50,
    marginTop: 20,
    textAlign: 'left',
    lineHeight: 30,
    fontSize: 17
  },
  bold: {
    fontWeight: "bold",
    fontSize: 20,
    color: colors.primaryButton,
  },
  plotTitle: {
    fontFamily: 'Montserrat-Regular',
    fontWeight: 'bold',
    marginTop: 40,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 22
  },
});
