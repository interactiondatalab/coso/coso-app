import React from 'react';
import styled from 'styled-components/native';
import {TouchableOpacity, StyleSheet, Text, ActivityIndicator} from 'react-native';
import colors, {screen, shadowStyles} from '../../../global/constants';

const LittleButton = (props) => {
  return (
    <>
    {
          props.loading ? <ActivityIndicator style={{alignSelf:'flex-end', marginRight: 30}} size={'small'}  color={colors.primaryButton}/>
          :<TouchableOpacity
          onPress={() => props.toDo()}
          style={[styles.wrapper, props.customStyle]}>
          
            <Text style={[styles.textStyle, props.igem && {color: colors.inactiveText}]}>{props.label}</Text>
        </TouchableOpacity>
        }
    </>
    
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.primaryButton,
    paddingVertical: 11,
    paddingHorizontal: 18,
    alignSelf: 'center',
    alignContent: 'center',
    borderRadius: 9,
    ...shadowStyles,
    alignSelf: 'flex-end',
  },
  textStyle: {
    fontFamily: 'Roboto-Medium',
    color: colors.white,
    fontSize: 14,
    textAlign: 'center',
    letterSpacing: 1.25,
  },
});

export default LittleButton;
