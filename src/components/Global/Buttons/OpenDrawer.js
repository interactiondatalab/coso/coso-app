import React, { useEffect } from 'react';
import {View, StyleSheet, Text, Image,TouchableOpacity} from 'react-native';
import colors, {screen, backButtonStyles, shadowStyles} from '../../../global/constants';

export const OpenDrawer = (props) => {

   /*  useEffect(()=>
        console.log(props)
    ) */

    let yes = () => console.log('yes')

    let content = () =>{
        if (props.clickable) {
            return(
                <TouchableOpacity onPress={yes} style={[styles.buttonPress, props.style]} {...props}>
                    <View style={styles.button}>
                    <Text style={styles.plus}>+</Text>
                    </View>
                </TouchableOpacity>
            )
        }else{
            return(
                <View style={[styles.buttonPress, props.style,]} {...props}>
                    <View style={[styles.button, {backgroundColor: colors.inactiveText}]}>
                    <Text style={styles.plus}>+</Text>
                    </View>
                </View>
            )
        }
    }
    return (
        <>
            {content()}
        </>

    );
};

const styles = StyleSheet.create({
    buttonPress:{
        width: 65,
        height: 65,
        borderWidth: 3,
        alignSelf: 'center',
        zIndex: 100,
        ...shadowStyles
    },
    button: {
        width: Platform.OS === 'ios' ? 55:60,
        height: Platform.OS === 'ios' ? 55:60,
        marginBottom: Platform.OS === 'ios' ? 3:-5,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: colors.primaryButton
    },
    plus:{
        color: colors.white,
        fontSize: 30,
        lineHeight: 25,
        paddingTop: 6,
        paddingLeft: 1.5,
    }
});
