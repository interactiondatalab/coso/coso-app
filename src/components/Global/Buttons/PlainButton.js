import React from 'react';
import styled from 'styled-components/native';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import colors, {screen} from '../../../global/constants';

const PlainButton = (props) => {
  return (
    <TouchableOpacity
      onPress={() => props.toDo()}
      style={[styles.wrapper, props.customStyle]}>
      <Text style={styles.textStyle}>{props.label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    bottom: 15,
    backgroundColor: colors.primaryButton,
    width: screen.w - 100,
    paddingVertical: 11,
    paddingHorizontal: 16,
    alignSelf: 'center',
    alignContent: 'center',
    borderRadius: 9,
  },
  textStyle: {
    fontFamily: 'Roboto-Medium',
    color: colors.white,
    fontSize: 14,
    textAlign: 'center',
    letterSpacing: 1,
  },
});

export default PlainButton;
