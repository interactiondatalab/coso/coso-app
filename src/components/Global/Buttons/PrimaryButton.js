import React from 'react';
import {TouchableOpacity, StyleSheet, Text, View, ActivityIndicator} from 'react-native';
import colors, {screen, shadowStyles} from '../../../global/constants';

const PrimaryButton = (props) => {
  return props.inactive ? (
    <View style={[styles.wrapper, props.customStyle, styles.inactive]}>
      <Text style={[styles.textStyle, styles.inactiveText]}>{props.label}</Text>
    </View>
  ) : (
    props.loading ? <ActivityIndicator style={[styles.indicator, props.customStyle]} size="large" color={colors.primaryButton} />:
    <TouchableOpacity
      onPress={() => props.toDo()}
      style={[styles.wrapper, props.customStyle]}>
      <Text style={styles.textStyle}>{props.label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.primaryButton,
    width: screen.w - 100,
    paddingVertical: 17,
    paddingHorizontal: 16,
    alignSelf: 'center',
    alignContent: 'center',
    borderRadius: 9,
    marginBottom: 17,
    ...shadowStyles,
  },
  indicator:{
    paddingVertical: 17,
    paddingHorizontal: 16,
    alignSelf: 'center',
    marginBottom: 17,
  },
  inactive: {
    backgroundColor: colors.inactiveButton,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
  },
  inactiveText: {
    color: colors.inactiveText,
  },
  textStyle: {
    fontFamily: 'Roboto-Medium',
    color: colors.white,
    fontSize: 14,
    textAlign: 'center',
    letterSpacing: 1,
  },
});

export default PrimaryButton;
