import React from 'react';
import styled from 'styled-components/native';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import colors, {screen, shadowStyles} from '../../../global/constants';

const SecondaryButton = (props) => {
  return (
    <TouchableOpacity
      onPress={() => props.toDo()}
      style={[styles.wrapper, props.customStyle]}>
      <Text style={styles.textStyle}>{props.label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
    width: screen.w - 100,
    paddingVertical: 16,
    paddingHorizontal: 16,
    alignSelf: 'center',
    alignContent: 'center',
    borderRadius: 9,
    marginBottom: 17,
    borderWidth: 1,
    borderColor: colors.primaryButton,
  },
  textStyle: {
    fontFamily: 'Roboto-Medium',
    color: colors.primaryButton,
    fontSize: 14,
    textAlign: 'center',
    letterSpacing: 1,
  },
});

export default SecondaryButton;
