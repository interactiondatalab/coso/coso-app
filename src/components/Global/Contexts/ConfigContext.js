import React, { Component, createContext } from "react";

// Provider and Consumer are connected through their "parent" context
const ConfigContext = createContext();

const Provider = ConfigContext.Provider
const Consumer = ConfigContext.Consumer

// Provider will be exported wrapped in ConfigProvider component.
class ConfigProvider extends Component {
  state = {
    currentDate: new Date(),
    date: new Date(),
    opened : false,
    clickable: true,
    date: new Date(),
    toggleOpening : () =>{
      const setTo = !this.state.opened;
      this.setState({ opened: setTo });
    },
    toggleClickable: (value)=>{
      this.setState({ clickable: value });
    },
    changeDate: (value)=>{
      this.setState({ date: value });
    },
    reloadDataFunction: ()=>{},
    reloadData: (fct)=>{
      this.setState({
        reloadDataFunction: fct
      })
    }

  };

  componentDidMount = ()=>{
    this.setState({
      date:  new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth(), this.state.currentDate.getDate(), 0,0,0,0),
    })
  }

  /* We stored the functions we need to use somewhere
  * in the app to be accessible from the Consumer component
  */

  render() {
    return (
      <Provider
        value={{
          opened: this.state.opened,
          toggleOpening: this.state.toggleOpening,
          clickable:  this.state.clickable,
          toggleClickable: this.state.toggleClickable,
          date: this.state.date,
          changeDate: this.state.changeDate,
          reloadData: this.state.reloadData,
          reloadDataFunction: this.state.reloadDataFunction
        }}
      >
        {this.props.children}
      </Provider>
    );
  }
}

export { ConfigContext, ConfigProvider };

// I make this default since it will probably be exported most often.
export default Consumer;
