import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Picker } from '@react-native-picker/picker';
import colors, {screen} from './../../../global/constants';


const Dropdown = ( {value, setValue, choices, error, customStyle, focusFunction, prompt, defaultValue} ) => {

  const [pickerList, setPickerList] = useState([])
  const [clicked, setClicked] = useState(false)

  useEffect(() => {
    console.log("CHOICES")
    console.log({choices})
    if (choices) {
      const tmp = choices.map((item, i) => (
          <Picker.Item color={colors.lightGrey} label={item} value={item} />
      ));
      setPickerList(tmp);
    }
  }, [choices])

  return (
    <View
      style={[
        styles.wrapper,
        customStyle,
        error == true
          ? {borderWidth: 2, borderColor: colors.red}
          : null
      ]}>
      <Picker
        selectedValue={value}
        style={styles.picker}
        prompt={prompt}
        itemStyle={styles.itemStyle}
        onValueChange={(itemValue, itemIndex) => {setClicked(true); setValue(itemValue)}}
      >
        <Picker.Item enabled={!clicked} color={colors.lightGrey} label={defaultValue} value={defaultValue} />
        {pickerList}
      </Picker>
    </View>
  )
}

export default Dropdown;

const styles = StyleSheet.create({
  wrapper: {
    width: screen.w-100,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderRadius: 0,
    paddingBottom: 10,
    paddingLeft: 5,
    marginBottom: 40,
    borderWidth: 1,
    borderColor: colors.borderColor,
    /* borderColor: colors.lightGrey, */
    borderRadius: 4,
    justifyContent: 'center',
    paddingHorizontal: 16,
    position: 'relative',
  },
  label: {
    position: 'absolute',
    top: -10,
    left: 10,
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.primaryButton,
    backgroundColor: colors.paleGrey,
    paddingHorizontal: 6,
  },
  input: {
    color: colors.black,
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
  },
  itemStyle: {
    color: colors.lightGrey
  }
});
