import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import colors, {screen, backButtonStyles} from '../../../global/constants';
import { isIphoneXorAbove } from '../../../global/functions';

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

export const HeaderMain = (props) => {


  let goBack = () => {
    alert('back');
  };
  return (
    <View style={styles.wrapper}>
      {props.goBack && (
        <TouchableOpacity onPress={goBack} style={styles.back}>
          <Image
            source={require('./../../../assets/icons/arrowBack.png')}
            resizeMode={'contain'}
            style={styles.backButton}
          />
        </TouchableOpacity>
      )}

      <Text style={styles.title}>
        {
          props.title ? props.title:props.sameDay ? 'Today, ':props.dayLabel+', '
        }
        {
          props.title ? '':props.day+' '+monthNames[props.month]
        }
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    width: screen.w,
    height: 50,
    marginTop: isIphoneXorAbove() ? 0:15,
    justifyContent: 'center',
  },
  title: {
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    color: colors.titleBlack,
    position: 'absolute',
    letterSpacing: 0.25,
  },
  back: {},
  backButton: {
    width: 14,
    height: 20,
    marginLeft: 25,
  },
});
