import React from 'react';
import {Image, StyleSheet} from 'react-native';

export default TabBarIcon = (props) => {
  let link;
  switch (props.iconName) {
    case 'home':
      link = props.focused
        ? require('./../../../assets/icons/homeFocused.png')
        : require('./../../../assets/icons/home.png');
      break;
    case 'charts':
      link = props.focused
        ? require('./../../../assets/icons/chartsFocused.png')
        : require('./../../../assets/icons/charts.png');
      break;
    case 'assignments':
      link = props.focused
        ? require('./../../../assets/icons/assignmentsFocused.png')
        : require('./../../../assets/icons/assignments.png');
      break;
    case 'settings':
      link = props.focused
        ? require('./../../../assets/icons/settingsFocused.png')
        : require('./../../../assets/icons/settings.png');
      break;
    default:
      break;
  }

  return <Image source={link} resizeMode="contain" style={[
    styles.icon, 
    props.gap ? {marginRight: 10}:null,
    props.secondGap ? {marginLeft: 10}:null
  ]}/>;
};

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
});
