import React, {Component, useState, useRef, useEffect} from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import colors, {screen} from './../../../global/constants';

const CustomInput = (props) => {
  const [text, setText] = useState('');
  const [focused, setFocused] = useState(false);
  const [visibility, setVisibility] = useState(false);

  manageVisibility = () => {
    setVisibility(!visibility);
  };

  return (
    <View style={styles.helpWrapper}>
      <View
        style={[
          styles.wrapper,
          props.customStyle,
          props.error == true
            ? {borderWidth: 2, borderColor: colors.red}
            : focused && {borderWidth: 2, borderColor: colors.primaryButton},
            props.textField ? {width: screen.w-75, borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, borderRadius: 0, paddingBottom: 10, paddingLeft: 5}:{height: 60},
          props.password && styles.password,

        ]}>
        {!props.noLabel && focused && !props.textField===true && (
          <Text
            style={[
              styles.label,
              props.error == true ? {color: colors.red} : null,
            ]}>
            {props.label}
          </Text>
        )}
        <TextInput
            style={[styles.input, props.password && styles.passwordInput, props.textField ? null:{height: 40,}]}
          placeholder={props.placeholder}
          multiline={props.textField ? true:false}
          placeholderTextColor={colors.lightGrey}
          secureTextEntry={props.password && !visibility}
          onKeyPress={props.onKeyFunction}
          autoCapitalize = 'none'
          keyboardType={props.keyboardType ? props.keyboardType:'default'}
          onChangeText={(text) => {
            setText(text);
            props.update(props.stateName, text);
          }}
          value={props.value ? props.value:text }
          defaultValue={text}
          onFocus={() => {
            setFocused(true);
            props.focusFunction && props.focusFunction();
          }}
          onBlur={() => {
            setFocused(false);
            props.blurFunction && props.blurFunction();
          }}
        />
        {props.password && (
          <TouchableOpacity
            onPress={manageVisibility}
            style={styles.visibilityButton}>
            <Image
              source={
                visibility
                  ? require('./../../../assets/icons/visibilityOff.png')
                  : require('./../../../assets/icons/visibility.png')
              }
              resizeMode={'contain'}
              style={styles.visibility}
            />
          </TouchableOpacity>
        )}
      </View>
        {props.helpText && <Text style={styles.textHelp}>{props.helpText}</Text>}
    </View>
  );
};

export default CustomInput;

const styles = StyleSheet.create({
  wrapper: {
    width: screen.w - 100,
    borderWidth: 1,
    borderColor: colors.borderColor,
    /* borderColor: colors.lightGrey, */
    borderRadius: 4,
    justifyContent: 'center',
    paddingHorizontal: 16,
    position: 'relative',
  },
  password: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 0,
  },
  passwordInput: {
    //width: '90%',
    width: screen.w - 160,
    //color: colors.lightGrey,
  },
  label: {
    position: 'absolute',
    top: -10,
    left: 10,
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.primaryButton,
    backgroundColor: colors.paleGrey,
    paddingHorizontal: 6,
  },
  input: {
    color: colors.black,
    fontSize: 16,
    fontFamily: 'Montserrat-Regular',
  },
  visibilityButton: {
    paddingRight: 8,
  },
  visibility: {
    width: 25,
    height: 60,
  },
  textHelp: {
    fontSize: 12,
    margin: 0
  },
  helpWrapper: {
    marginBottom: 40,
  }
});
