import React, {useEffect, useState} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import colors, { screen } from '../../global/constants';
import { getIsRead } from '../../services/api/news/functions';

const NewsSelect = (props) =>{
    const [done, setDone] = useState(false)
    const [news, setNews] = useState(props.news)

    useEffect(() => {
      setNews(props.news)
    }, [props.news])

    let getText = () => {
      return(
        news.unread ?
          <Text style={styles.unreadtext}>Unread</Text>:
          <View style={{flexDirection:'row'}}>
            <Image
                source={require('./../../assets/icons/check.png')}
                resizeMode={'contain'}
                style={styles.iconAction}
            />
            <Text style={styles.readtext}>Read</Text>
          </View>
      )
    }

    console.log(news)

    return (
        <TouchableOpacity onPress={props.openNews} style={[styles.wrapper, props.new ? {backgroundColor: colors.blueLight}:null]}>
            <View style={styles.status}>
              {getText()}
              <Text style={styles.date}>{news.created_at.substring(0,10)}</Text>
            </View>
            <View style={styles.infos}>
              <Text style={styles.title}>{news.title}</Text>
              <Text style={styles.details} numberOfLines={1}>{news.content}</Text>
            </View>
            <View
              style={{
                borderBottomColor: colors.lightGrey,
                borderBottomWidth: 0.2,
                marginTop:20
              }}
            />
        </TouchableOpacity>
    );
}

export default NewsSelect

const styles= StyleSheet.create({
    wrapper:{
        width: screen.w,
        paddingHorizontal: 20,
        paddingVertical: 5,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        // marginBottom: 5
    },
    status:{
      marginLeft:20,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    iconLabel:{
        width: 25,
        height: 25,
        marginBottom: 17
    },
    iconAction:{
        width: 18,
        height: 18,
        marginTop: 0,
        marginRight:5,
        color: colors.primaryButton
    },
    unreadtext:{
      color: colors.lightGrey

    },
    readtext:{
      color: colors.primaryButton
    },
    date: {
      color: colors.lightGrey,
      alignSelf: 'flex-end'
    },
    infos:{
        marginLeft: 20,
        width: '80%'
    },
    title:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 16,
        letterSpacing: .22,
        color: colors.titleBlack,
        marginBottom: 5,
    },
    details:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        letterSpacing: .4,
        color: colors.lightGrey
    }
})
