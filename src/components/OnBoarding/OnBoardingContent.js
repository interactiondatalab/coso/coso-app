import { Platform } from 'react-native';

const slides = [
  {
    image: require('./../../assets/images/onboarding1.png'),
    title: 'The iGEM network experience',
    subtitle:
      "Hello iGEMer!\nLet's explore your team dynamics with CoSo to better understand what makes a great team in science and engineering!",
  },
  {
    image: require('./../../assets/images/onboarding2.png'),
    title: 'Create your science logbook',
    subtitle:
      'CoSo is a daily journaling app that helps you log your tasks during your iGEM adventure. Tell us more about what you did and who you worked with by logging your daily achievements.',
  },
  {
    image: require('./../../assets/images/onboarding4.png'),
    title: 'Benefits',
    subtitle:
      'Who doesn’t dream of knowing how to best tune their team dynamics? By using this app you will help us understand the mechanics of team performance, so that you can be an inspiration for future iGEM teams!',
  },
  {
    image: require('./../../assets/images/onboarding5.png'),
    title: 'Safe & Secure',
    subtitle:
      'All information you will provide is and will remain confidential. All collected data are anonymized, only the global dynamics of your team will be communicated.',
  },
  {
    image: require('./../../assets/images/onboarding3.png'),
    title: 'Get analytics on your work',
    subtitle:
      'We will highlight your team interactions and progress throughout the several steps of the competition',
    // buttonLabel: Platform.OS === 'ios' ?  "Allow bluetooth":"Let's start",
    buttonLabel: "Let's start",
    buttonTarget: 'home',
  },
];

export default slides;
