import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Linking} from 'react-native';
import colors, {screen} from './../../../global/constants';

const PrivacyAndTerms = (props) => {
  let join = () =>{
    Linking.openURL('https://igem.org/Privacy_Policy')
  }
  return (
    <View
      style={[
        styles.links,
        props.keyboard ? {marginBottom: 25} : props.mb ? props.mb : {marginBottom: screen.h / 6+30},
      ]}>
      <TouchableOpacity style={styles.skipContainer} onPress={join}>
        <Text style={styles.link}>Privacy policy</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.skipContainer} onPress={join}>
        {/* <Text style={styles.link}>Terms of use</Text> */}
      </TouchableOpacity>
    </View>
  );
};

export default PrivacyAndTerms;

const styles = StyleSheet.create({
  links: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    width: screen.w - 30,
  },
  link: {
    fontFamily: 'Montserrat-Medium',
    color: colors.lightGrey,
    fontSize: 12,
    marginRight: 20,
  },
});
