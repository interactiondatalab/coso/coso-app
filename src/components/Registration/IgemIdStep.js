import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, Text, Keyboard, Animated, Image, Alert, Linking} from 'react-native';
import colors, {
  defaultScreenStyles,
  screen,
  shadowTopStyles,
} from './../../global/constants';
import PrimaryButton from './../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../components/Global/TextInput/CustomInput';
import LittleButton from '../../components/Global/Buttons/LittleButton';
import PrivacyAndTerms from './ClickableLinks/PrivacyAndTerms';
import {isIphoneXorAbove} from '../../global/functions';
import { getUserByIgemId } from '../../services/api/signup/functions';

const IgemIdStep = (props) => {
  const [igem, setIgem] = useState('');
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false)

  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 600,
      useNativeDriver: false
    }).start();
  });

  let updateState = (stateLabel, value) => {
    setIgem(value);
  };

  let validate = async () => {
    setLoading(true)

    if (igem !== '') {
      let result = await getUserByIgemId(igem.trim()).then(
        (res)=>{
          console.log(res)
          if (res.status === 200) {
            props.register("username", igem.trim())
            props.manageStep(2)
          }else{
            setLoading(false)
            setError(true)
          }
        }
      ).catch(
        e=>console.error(e)
      )
      //let result = 'Raph'
      //console.log('result',result)


    } else{
      setLoading(false)
      Alert.alert("iGEM username", "The field is empty.")
    }
    setLoading(false)
    Keyboard.dismiss();
  };

  let gotoUsername = async () => {
    props.manageStep(1)
  };

  return (
    <>
      <Animated.View
        style={[
          styles.content,
          {marginBottom: props.marginContent, opacity: fadeAnim},
        ]}>
        <Text style={styles.titleRegistration}>What's your iGEM username ?</Text>
        <CustomInput
          error={error}
          placeholder={'Ex: elfuche23'}
          label={'iGEM Username'}
          update={updateState}
          customStyle={styles.inputStyle}
        />

        <Text style={[styles.error, props.keyboardOpen && {marginBottom: 0}]}>
          {error === true ? 'Oups! We cannot find your iGEM username.' : null}
        </Text>
        <Text onPress={()=>{

          Alert.alert(
            "Retrieve your iGEM username",
            "Get your iGEM team name, look for it on the site and find your username in the list.",
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Open site", onPress: () => Linking.openURL('https://igem.org/Team_List?year=2020') }
            ],
            { cancelable: false }
          );
        }} style={styles.howTo}>How to retrieve my iGEM username?</Text>
        {/* <Text style={styles.howTo}>I don’t have an iGEM username.</Text> */}
        {props.keyboardOpen === true ? null : (
          <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
            <LittleButton
              toDo={gotoUsername}
              label={`No iGEM username`}
              igem={true}
              customStyle={{alignSelf: 'flex-start', backgroundColor: colors.lightPrimary, marginRight: 20}}
            />

            <LittleButton
              loading={loading}
              toDo={validate}
              label={'Next'}
              customStyle={{marginRight: -1}}
            />
          </View>
        )}
      </Animated.View>
      <PrivacyAndTerms keyboard={props.keyboardOpen} />
      {props.keyboardOpen === true ? (
        <View style={styles.validationBand}>
          <LittleButton
            loading={loading}
            toDo={validate}
            label={'Next'}
            customStyle={{marginRight: -1}}
          />
        </View>
      ) : null}
      {
        !props.keyboardOpen && <Image
        source={require('./../../assets/images/signupIllu.png')}
        resizeMode={'cover'}
        style={styles.bottomIllu}
      />
      }
    </>
  );
};

export default IgemIdStep;

const styles = StyleSheet.create({
  content: {
    // marginBottom: screen.h / 4.5,
    width: screen.w,
    paddingHorizontal: 50,
  },
  avoiding: {
    width: screen.w,
  },
  titleRegistration: {
    fontSize: 25,
    textAlign: 'left',
    width: screen.w - 100,
    color: colors.titleBlack,
    marginBottom: 40,
  },
  inputStyle: {
    marginBottom: 0,
  },
  error: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.red,
    marginTop: 5,
    marginLeft: 16,
  },
  howTo: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.primaryButton,
    marginTop: 10,
    marginBottom: 40,
  },
  validationBand: {
    backgroundColor: colors.white,
    width: screen.w,
    height: 60,
    justifyContent: 'center',
    paddingRight: 10,
    ...shadowTopStyles,
  },
  bottomIllu: {
    width: screen.w,
    height: screen.h / 4,
    position: 'absolute',
    bottom: isIphoneXorAbove() ? -screen.h / 15 : -screen.h / 15,
  },
});
