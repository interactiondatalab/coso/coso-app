import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, Text, Keyboard, Animated, Image, Linking, Alert} from 'react-native';
import colors, {
  defaultScreenStyles,
  screen,
  shadowTopStyles,
} from './../../global/constants';
import {validateEmail, isIphoneXorAbove} from './../../global/functions';
import PrimaryButton from './../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../components/Global/TextInput/CustomInput';
import LittleButton from '../../components/Global/Buttons/LittleButton';
import PrivacyAndTerms from './ClickableLinks/PrivacyAndTerms';
import CheckBox from '@react-native-community/checkbox';

const PassWordStep = (props) => {
  const [password, setPassword] = useState('');
  const [error, setError] = useState(false);
  const [errorChecked, setErrorChecked] = useState(false);
  const [errorChecked2, setErrorChecked2] = useState(false);
  const [toggleCheckBox, setToggleCheckBox] = useState(false)
  const [toggleCheckBox2, setToggleCheckBox2] = useState(false)

  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 600,
      useNativeDriver: false
    }).start();
  });

  let updateState = (stateLabel, value) => {
    setPassword(value);
  };

  testPassword = () => {
    if (password.length < 7) {
      setError(true);
      return false;
    } else {
      setError(false);
      return true;
    }
    //setError(!validateEmail(email));
  };

  resetError = () => {
    setError(false);
  };

  let validate = () => {
    if (testPassword()) {
      if (toggleCheckBox) {
        if(toggleCheckBox2){
          props.register("password", password, true)
        }else{
          setErrorChecked2(true)
        }
      }else{
        setErrorChecked(true)
      }
    }
    Keyboard.dismiss();
  };

  check = () =>{
    setToggleCheckBox(true)
    setErrorChecked(false)
  }

  check2 = () =>{
    setToggleCheckBox2(true)
    setErrorChecked2(false)
  }

  return (
    <>
      <Animated.View
        style={[
          styles.content,
          {marginBottom: 20, opacity: fadeAnim},
        ]}>
        <Text style={styles.titleRegistration}>
          Now let’s secure your account.
        </Text>
        <Text style={styles.subtitle}>Create your password</Text>
        <CustomInput
          error={error}
          placeholder={'Enter your password'}
          label={'Password'}
          update={updateState}
          customStyle={styles.inputStyle}
          onKeyFunction={testPassword}
          password={true}
        />
        {props.keyboardOpen === true || error === true ? (
          <Text style={[styles.error, !error && styles.indication, props.keyboardOpen && {marginBottom: 0}]}>
            8 characters minimum.
          </Text>
        ) :null}
        <View style={[styles.checkBoxWrapperFirst,errorChecked&&{borderColor: colors.red, borderWidth: 1,
        }]}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            boxType={'square'}
            style={styles.checkBox}
            lineWidth={2}
            tintColor={colors.lightGrey}
            onCheckColor={colors.primaryButton}
            onTintColor={colors.primaryButton}
            onValueChange={() => toggleCheckBox ? setToggleCheckBox(false) : check()}
          />
          <Text style={styles.textCheck}>
            I have read and accepted the <Text style={styles.highlight} onPress={()=>Linking.openURL('https://igem.org/Privacy_Policy')}>privacy policy</Text> and <Text style={styles.highlight} onPress={()=>Linking.openURL("https://drive.google.com/file/d/1PKw4vCiw756M4PT_THdoOzIGdsXpbQhw/view?usp=sharing")}>the Information form</Text>
          </Text>
        </View>
        <View style={[styles.checkBoxWrapper,errorChecked2&&{borderColor: colors.red, borderWidth: 1,
        }]}>
          <CheckBox
            disabled={false}
            value={toggleCheckBox2}
            boxType={'square'}
            style={styles.checkBox}
            lineWidth={2}
            tintColor={colors.lightGrey}
            onCheckColor={colors.primaryButton}
            onTintColor={colors.primaryButton}
            onValueChange={() => toggleCheckBox2 ? setToggleCheckBox2(false) : check2()}
          />
          <Text style={styles.textCheck}>
            I am over 16. <Text style={styles.highlight} onPress={()=>{
              Alert.alert(
                "What to do",
                "You need to contact us by e-mail, at igem-ties@cri-paris.org to validate your account.",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "Open mail", onPress: () => Linking.openURL(`mailto:igem-ties@cri-paris.org`)}
                ],
                { cancelable: false }
              );
            }}>What to do if I'm not.</Text>
          </Text>
        </View>
        {props.keyboardOpen === true ? null : (
          <LittleButton
            toDo={validate}
            label={'Create account'}
            customStyle={{marginRight: -1}}
          />
        )}
      </Animated.View>
      <PrivacyAndTerms mb={{marginBottom: screen.h / 6}} keyboard={props.keyboardOpen} />
      {props.keyboardOpen === true ? (
        <View style={styles.validationBand}>
          <LittleButton
            toDo={validate}
            label={'Create account'}
            customStyle={{marginRight: -1}}
          />
        </View>
      ) : null}
      {
        !props.keyboardOpen && <Image
        source={require('./../../assets/images/signupIllu.png')}
        resizeMode={'cover'}
        style={styles.bottomIllu}
      />
      }
    </>
  );
};

export default PassWordStep;

const styles = StyleSheet.create({
  content: {
    // marginBottom: screen.h / 4.5,
    width: screen.w,
    paddingHorizontal: 50,
  },
  avoiding: {
    width: screen.w,
  },
  titleRegistration: {
    fontSize: 25,
    textAlign: 'left',
    width: screen.w - 100,
    color: colors.titleBlack,
    marginBottom: 20,
  },
  subtitle: {
    fontFamily: 'Montserrat-Regular',
    width: screen.w - 100,
    lineHeight: 30,
    fontSize: 17,
    color: colors.lightGrey,
    marginBottom: 40,
  },
  inputStyle: {
    marginBottom: 0,
  },
  error: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.red,
    marginTop: 5,
    marginLeft: 16,
  },
  indication: {
    color: colors.primaryButton,
  },
  validationBand: {
    backgroundColor: colors.white,
    width: screen.w,
    height: 60,
    justifyContent: 'center',
    paddingRight: 10,
    ...shadowTopStyles,
  },
  checkBoxWrapperFirst:{
    width: screen.w-75,
    flexDirection:'row',
    alignItems: 'flex-start',
    marginTop: 40,
    padding: 10
  },
  checkBoxWrapper:{
    flexDirection:'row',
    alignItems: 'flex-start',
    marginBottom: 20,
    marginTop: 0,
    padding: 10
  },
  checkBox:{
    transform: [{ scale: .8 }, {translateY: -1}]
  },
  textCheck:{
    fontFamily: 'Montserrat-Medium',
    fontSize: 13,
    color: colors.grey,
  },
  highlight:{
    color: colors.primaryButton,
  },
  bottomIllu: {
    width: screen.w,
    height: screen.h / 4,
    position: 'absolute',
    bottom: isIphoneXorAbove() ? -screen.h / 8 : -screen.h / 8,
  },
});
