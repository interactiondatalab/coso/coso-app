import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, Text, Keyboard, Animated, Image, Alert, Linking} from 'react-native';
import colors, {
  defaultScreenStyles,
  screen,
  shadowTopStyles,
} from './../../global/constants';
import PrimaryButton from './../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../components/Global/TextInput/CustomInput';
import LittleButton from '../../components/Global/Buttons/LittleButton';
import PrivacyAndTerms from './ClickableLinks/PrivacyAndTerms';
import {isIphoneXorAbove} from '../../global/functions';

import axios from 'axios';
import { getTeamByName } from '../../services/api/signup/functions';

const TeamNameStep = (props) => {
  const [teamName, setTeamName] = useState('');
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false)

  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 600,
      useNativeDriver: false
    }).start();
  });

  let updateState = (stateLabel, value) => {
    setTeamName(value);
  };

  let validate = async() => {
    setLoading(true)
    if (teamName !== '') {
      let result = await getTeamByName(teamName)

      if (result.status === 200) {
        props.register("team_id", result.data.team_id)
        props.manageStep(1)
      }else{
        setLoading(false)
        setError(true)
      }
    } else{
      setLoading(false)
      Alert.alert("Team name", "The field is empty.")
    }
    Keyboard.dismiss();
    setLoading(false)
  };
  return (
    <>
      <Animated.View
        style={[
          styles.content,
          {marginBottom: props.marginContent, opacity: fadeAnim},
        ]}>
        <Text style={styles.titleRegistration}>
          What’s your Team name?
        </Text>
        <CustomInput
          error={error}
          placeholder={'Team Name'}
          label={'Team Name'}
          update={updateState}
          customStyle={styles.inputStyle}
        />

        <Text style={[styles.error, props.keyboardOpen && {marginBottom: 0}]}>
          {error === true ? 'Oups! We cannot find your Team Name.' : null}
        </Text>
        <Text onPress={()=>Linking.openURL("https://igem.org/Team_List?year=2020")} style={styles.howTo}>How to retrieve my Team Name?</Text>
        {props.keyboardOpen === true ? null : (
          <LittleButton
            loading={loading}
            toDo={validate}
            label={'Next'}
            customStyle={{marginRight: -1}}
          />
        )}
      </Animated.View>
      <PrivacyAndTerms keyboard={props.keyboardOpen} />
      {props.keyboardOpen === true ? (
        <View style={styles.validationBand}>
          <LittleButton
            loading={loading}
            toDo={validate}
            label={'Next'}
            customStyle={{marginRight: -1}}
          />
        </View>
      ) : null}
      {
        !props.keyboardOpen && <Image
        source={require('./../../assets/images/signupIllu.png')}
        resizeMode={'cover'}
        style={styles.bottomIllu}
      />
      }
    </>
  );
};

export default TeamNameStep;

const styles = StyleSheet.create({
  content: {
    // marginBottom: screen.h / 4.5,
    width: screen.w,
    paddingHorizontal: 50,
  },
  avoiding: {
    width: screen.w,
  },
  titleRegistration: {
    fontSize: 25,
    textAlign: 'left',
    width: screen.w - 100,
    color: colors.titleBlack,
    marginBottom: 40,
  },
  inputStyle: {
    marginBottom: 0,
  },
  error: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.red,
    marginTop: 5,
    marginLeft: 16,
  },
  howTo: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.primaryButton,
    marginTop: 10,
    marginBottom: 40,
  },
  validationBand: {
    backgroundColor: colors.white,
    width: screen.w,
    height: 60,
    justifyContent: 'center',
    paddingRight: 10,
    ...shadowTopStyles,
  },
  bottomIllu: {
    width: screen.w,
    height: screen.h / 4,
    position: 'absolute',
    bottom: isIphoneXorAbove() ? -screen.h / 15 : -screen.h / 15,
  },
});
