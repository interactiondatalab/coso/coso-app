import React, {useState, useEffect, useRef} from 'react';
import {View, StyleSheet, Text, Keyboard, Animated, Image, Alert} from 'react-native';
import colors, {
  defaultScreenStyles,
  screen,
  shadowTopStyles,
} from './../../global/constants';
import PrimaryButton from './../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../components/Global/TextInput/CustomInput';
import LittleButton from '../../components/Global/Buttons/LittleButton';
import PrivacyAndTerms from './ClickableLinks/PrivacyAndTerms';
import {isIphoneXorAbove} from '../../global/functions';
import { existUser } from '../../services/api/usersAndTeams/functions';

const UsernameStep = (props) => {
  const [username, setUsername] = useState('');
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false)

  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 600,
      useNativeDriver: false
    }).start();
  });

  let updateState = (stateLabel, value) => {
    setUsername(value);
  };

  let validate = async() => {
    setLoading(true)

    if (username !== '') {
      if (username.length < 17 && username !== '') {
        await existUser(username).then(
          (res)=>{
            if (res.status === 200) {
              Alert.alert("Username", "Username already taken")

            }else{
              props.register("username", username)
              props.manageStep(1);
            }
          }
        )
      } else{
        setError(true)
        setLoading(false)

      }
    } else{
      Alert.alert("iGEM username", "The field is empty.")
      setLoading(false)
    }
    Keyboard.dismiss();
    setLoading(false)
  };
  return (
    <>
      <Animated.View
        style={[
          styles.content,
          {marginBottom: props.marginContent, opacity: fadeAnim},
        ]}>
        <Text style={styles.titleRegistration}>
          No iGEM username? {'\n'}Then let’s identify you!
        </Text>
        <Text style={styles.subtitle}>What’s your username?</Text>
        <CustomInput
          error={error}
          placeholder={'Username'}
          label={'Username'}
          update={updateState}
          customStyle={styles.inputStyle}
        />
        <Text style={[styles.error, props.keyboardOpen && {marginBottom: 0}]}>
          {error === true ? 'Oups! Your username is too long.' : null}
        </Text>
        {props.keyboardOpen === true ? null : (
          <LittleButton
            loading={loading}
            toDo={validate}
            label={'Next'}
            customStyle={{marginRight: -1}}
          />
        )}
      </Animated.View>
      <PrivacyAndTerms keyboard={props.keyboardOpen} />
      {props.keyboardOpen === true ? (
        <View style={styles.validationBand}>
          <LittleButton
            loading={loading}
            toDo={validate}
            label={'Next'}
            customStyle={{marginRight: -1}}
          />
        </View>
      ) : null}
      {
        !props.keyboardOpen && <Image
        source={require('./../../assets/images/signupIllu.png')}
        resizeMode={'cover'}
        style={styles.bottomIllu}
      />
      }
    </>
  );
};

export default UsernameStep;

const styles = StyleSheet.create({
  content: {
    // marginBottom: screen.h / 4.5,
    width: screen.w,
    paddingHorizontal: 50,
  },
  avoiding: {
    width: screen.w,
  },
  titleRegistration: {
    fontSize: 25,
    textAlign: 'left',
    width: screen.w - 100,
    color: colors.titleBlack,
    marginBottom: 20,
  },
  subtitle: {
    fontFamily: 'Montserrat-Regular',
    width: screen.w - 100,
    lineHeight: 30,
    fontSize: 17,
    color: colors.lightGrey,
    marginBottom: 40,
  },
  inputStyle: {
    marginBottom: 0,
  },
  error: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.red,
    marginTop: 5,
    marginLeft: 16,
    marginBottom: 65
  },
  howTo: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.primaryButton,
    marginTop: 10,
    marginBottom: 40,
  },
  validationBand: {
    backgroundColor: colors.white,
    width: screen.w,
    height: 60,
    justifyContent: 'center',
    paddingRight: 10,
    ...shadowTopStyles,
  },
  bottomIllu: {
    width: screen.w,
    height: screen.h / 4,
    position: 'absolute',
    bottom: isIphoneXorAbove() ? -screen.h / 15 : -screen.h / 15,
  },
});
