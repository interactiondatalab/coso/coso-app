import React, { useRef, useEffect } from 'react';
import {TouchableOpacity, StyleSheet, Text, View, Animated, Image} from 'react-native';
import colors, {screen, bigShadowStyles, shadowStyles} from '../../global/constants';

const ItemSettings = (props) => {

  return (
    <View style={[styles.container]}>
        {props.noLabel ? null:<Text style={styles.labelText}>{props.label}</Text>}
        <View style={[styles.labelWrapper, props.noLabel ? {paddingBottom: 25, paddingTop: 5}:null]}>
            <Text style={[styles.content, props.noLabel ? {marginTop: 0, color: colors.titleBlackLow}:null]}>{props.content}</Text>
            <TouchableOpacity style={styles.button} onPress={props.change}>
                <Text style={[styles.textButton, props.noLabel ? {marginBottom: 0}:null]}>{props.editable ? props.edit:''}</Text>
            </TouchableOpacity>
        </View>
        
  </View>
  );
};

const styles = StyleSheet.create({
    container:{
        width: screen.w,
        paddingHorizontal: 20,
        marginVertical: 10,
        justifyContent: 'space-between'
    },
    labelWrapper:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: colors.inactiveButton
    },
    button:{
        marginRight: 10
    },
    textButton:{
        fontSize: 15,
        fontWeight: '500',
        color: colors.primaryButton,
        marginBottom: 10
    },
    labelText:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        color: colors.lightGrey
    },
    content:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 16,
        color: colors.titleBlackLowLow,
        marginTop: 5
    },
    iconLabel:{
        width: 23,
        height:23
    },
    chevron:{
        width: 25,
        height: 20
    }
});

export default ItemSettings;
