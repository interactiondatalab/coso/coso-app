import React, { useRef, useEffect } from 'react';
import {TouchableOpacity, StyleSheet, Text, View, Animated, Image} from 'react-native';
import colors, {screen, bigShadowStyles, shadowStyles} from '../../global/constants';

const ItemSettings = (props) => {

  return (
    <TouchableOpacity onPress={props.press} style={styles.container}>
        <View style={styles.labelWrapper}>
            <Image
                source={props.icon}
                resizeMode={'contain'}
                style={styles.iconLabel}
            />
            <Text style={styles.labelText}>{props.label}</Text>
        </View>
        {
            props.without ? null:<Image
            source={require('./../../assets/icons/chevronR.png')}
            resizeMode={'contain'}
            style={styles.chevron}
        />
        }
        
  </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
    container:{
        width: screen.w,
        paddingHorizontal: 20,
        marginVertical: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    labelWrapper:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    labelText:{
        fontFamily: 'Montserrat-Regular',
        fontSize: 16,
        marginLeft: 15,
        color: colors.black
    },
    iconLabel:{
        width: 23,
        height:23
    },
    chevron:{
        width: 25,
        height: 20
    }
});

export default ItemSettings;
