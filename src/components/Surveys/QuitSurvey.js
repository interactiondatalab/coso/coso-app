import React, { useRef, useEffect } from 'react';
import styled from 'styled-components/native';
import {TouchableOpacity, StyleSheet, Text, View, Animated} from 'react-native';
import colors, {screen, bigShadowStyles} from '../../global/constants';
import PrimaryButton from '../Global/Buttons/PrimaryButton';
import SecondaryButton from '../Global/Buttons/SecondaryButton';

const QuitSurvey = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
   /*  Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 600,
      useNativeDriver: true
    }).start(); */
  });
  later = ()=>{
    props.setModal()
  }
  open = ()=>{
    props.goBack()
  }
  return (
    <View
        style={[
          styles.wrapper,

        ]}>
      <Text style={styles.title}>Exit survey?</Text>
      <Text style={styles.indication}>
        Are you sure you want to leave this survey?{'\n'}
        Your answers will not be saved.
      </Text>
      <View style={styles.buttons}>
        <SecondaryButton customStyle={styles.button} toDo={later} label={'Cancel'} />
        <PrimaryButton customStyle={[styles.button, {marginLeft: 10}]} toDo={open} label={'Yes leave'} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.white,
    width: screen.w-100,
    top: screen.h/2-80,
    zIndex: 1000,
    paddingVertical: 16,
    paddingHorizontal: 30,
    alignSelf: 'center',
    borderRadius: 9,
    alignSelf: 'center',
    position: 'absolute',
    ...bigShadowStyles,

  },
  title: {
    fontFamily: 'Montserrat-Medium',
    color: colors.greenGrey,
    fontSize: 21,
    letterSpacing: .25,
    marginBottom: 15,
    marginTop: 10,
  },
  indication: {
    fontFamily: 'Montserrat-Medium',
    color: colors.titleBlackLow,
    fontSize: 15,
    letterSpacing: .22,
    marginBottom: 35
  },
  buttons:{
    width: screen.w-100,
    flexDirection: 'row'
  },
  button:{
    width: (screen.w-170)/2,
    height: 40,
    paddingVertical: 0,
    justifyContent: 'center',
    marginBottom: 0
  }
});

export default QuitSurvey;
