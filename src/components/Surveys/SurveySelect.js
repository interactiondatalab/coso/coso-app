import React, {useEffect, useState} from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import colors, { screen } from '../../global/constants';
import { getIsDone } from '../../services/api/surveys/functions';

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const SurveySelect = (props) =>{
    const [done, setDone] = useState(false)
    const [minutes, setMinutes] = useState("1 minutes")

    useEffect(
        ()=>{
            getIsDone(props.survey.id).then(res=>{
                console.log(res.status, res.data)
                if (res.status === 200 && res.data === 0) {
                  //done = true
                  setDone(false)
                } else{
                  //done = false
                  setDone(true)
                }
            })
        }
    )


    let getIcon = ()=>{
        return(
            done ?
                <Image
                    source={require('./../../assets/icons/doneSurvey.png')}
                    resizeMode={'contain'}
                    style={styles.iconLabel}
                />:
                <Image
                    source={require('./../../assets/icons/newSurvey.png')}
                    resizeMode={'contain'}
                    style={styles.iconLabel}
                />
        )
    }

    let getDone = ()=>{
        return(
            done ?
                <Image
                    source={require('./../../assets/icons/check.png')}
                    resizeMode={'contain'}
                    style={styles.iconAction}
                />:
                <Image
            source={require('./../../assets/icons/chevronR.png')}
            resizeMode={'contain'}
            style={styles.iconAction}
        />)
    }

    let getDate = () =>{
        switch (date.getDate()) {
            case 1:
                return `${date.getDate()}st`
                break;
            case 2:
                return `${date.getDate()}nd`
                break;
            case 3:
                return `${date.getDate()}rd`
                break;

            default:
                return `${date.getDate()}th`
                break;
        }
    }

    next = () =>{
        props.next({title:props.survey.name, data: props.survey, minutes: props.survey.minutes})
    }

    return (
        <>
        {
            done ? <View onPress={next} style={[styles.wrapper, props.new ? {backgroundColor: colors.blueLight}:null]}>
            {getIcon()}
            <View style={styles.infos}>
                <Text style={styles.title}>{props.title}</Text>
                <Text style={styles.details}>{props.survey.minutes}</Text>
            </View>
            {getDone()}
          </View>
          :<TouchableOpacity onPress={next} style={[styles.wrapper, props.new ? {backgroundColor: colors.blueLight}:null]}>
            {getIcon()}
            <View style={styles.infos}>
                <Text style={styles.title}>{props.title}</Text>
                <Text style={styles.details}>{props.survey.minutes}</Text>
            </View>
            {getDone()}
        </TouchableOpacity>

        }
        </>
    );
}

export default SurveySelect

const styles= StyleSheet.create({
    wrapper:{
        width: screen.w,
        paddingHorizontal: 20,
        paddingVertical: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 5
    },
    iconLabel:{
        width: 25,
        height: 25,
        marginBottom: 17
    },
    iconAction:{
        width: 18,
        height: 18,
        marginTop: 10,
    },
    infos:{
        marginLeft: 20,
        width: '80%'
    },
    title:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        letterSpacing: .22,
        color: colors.titleBlack,
        marginBottom: 5,
    },
    details:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        letterSpacing: .4,
        color: colors.lightGrey
    }
})
