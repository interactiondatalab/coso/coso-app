import React, {Component, useState, useEffect, useRef} from 'react';
import {
  Image,
  Text,
  StyleSheet,
  Animated,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import colors, {
  screen,
  tasks,
  shadowStyles
} from '../../global/constants';

import { isIphoneXorAbove } from '../../global/functions';
import AddPeopleToTask from './AddPeopleSupplies/AddPeopleToTask';
import AddIgemTeamCollaborator from './AddPeopleSupplies/AddIgemTeamCollaborator';
import SelectPeople from './AddPeopleSupplies/SelectPeople';
import { getUserTeam, getRelationCategories } from '../../services/api/usersAndTeams/functions';

class AddPeople extends Component {
    constructor(props){
        super(props)
        this.state={
            fadeAnim: new Animated.Value(0),
            taskSelected: '',
            step: 0,
            recordsTasks: {},
            myTeam:[],
            relations: [],
            taskName: '',
            taskId: 0,
            igem_users:[],
            firstTab: true
        }
    }

    componentDidMount = async()=>{
        Animated.timing(this.state.fadeAnim, {
            toValue: 1,
            duration: 600,
            useNativeDriver: true
          }).start();
        this.props.workList.forEach(element => {
            let igem_users = this.addUsers(element.igem_users)
            let relations = this.addRelations(element.relation_categories)
            this.addTasksAndPeople(element.id, igem_users, relations, element.task_done_at, element.task_finished_at)
        });

        this.refreshMyTeam();
        this.refreshCollaborators();
    }

    refreshMyTeam = async() => {
      let myTeam = await getUserTeam().then(
          res=>{
              if (res.status === 200) {
                  this.setState({
                      myTeam: res.data.igem_users
                  })
              } else{
                  this.setState({
                      myTeam: ['Sorry, an error occured retrieving your team members.']
                  })
              }
          }
      )
    }

    addUsers = (users = []) => {
        let usersToReturn = []
        /* users.forEach(user=>{
            console.log(user.id)
        }) */
        if (users.length !== 0) {
            Object.keys(users).map(async(element)=>{
                //console.log(element, users[element])
                users[element].forEach(user=>{
                    usersToReturn.push(user.id)
                })
            })
            return usersToReturn
        } else{
            return usersToReturn
        }
    }

    addRelations = (relations = []) =>{
        let relationsToReturn = []


        if (relations.length !== 0) {
            relations.forEach(relation=>{
                relationsToReturn.push(relation.id)
            })
            return relationsToReturn
        } else{
            return relationsToReturn
        }
    }

    settingTaskName = (name, id)=>{
        this.setState({
            taskName: name,
            taskId: id,
        })
    }

    updateTaskSelected = (task)=>{
        this.setState({
            taskSelected:task
        })
    }

    setStep = (stepValue) =>{
        this.setState({
            step: stepValue
        })
    }

    addTasksAndPeople = (key, people, relationsCat, startDate, endDate) =>{
        this.setState(prevState => ({
            recordsTasks: {                   // object that we want to update
                ...prevState.recordsTasks,    // keep all other key-value pairs
                [key]: {
                    "igem_users": people,
                    "task_done_at": startDate,
                    "task_finished_at": endDate,
                    "relation_categories": relationsCat
                }     // update the value of specific key
            }
        }))
    }

    refreshCollaborators = () => {
      getRelationCategories().then(
          (res)=>{
              this.setState({
                  relations: res.data
              })
          }
      )
    }

    getStep = () =>{
        switch (this.state.step) {
            case 0:
                return <AddPeopleToTask
                            settingTaskName={this.settingTaskName}
                            modifying={this.props.modifying}
                            noPrevious={this.props.noPrevious}
                            registerTask={this.props.registerTask}
                            peopleTasks={this.state.recordsTasks}
                            back={this.props.back}
                            update={this.updateTaskSelected}
                            setStep={this.setStep}
                            list={this.props.workList}/>
                break;
            case 1:
                return <SelectPeople
                            peopleTasks={this.state.recordsTasks}
                            igem_users={this.state.igem_users}
                            taskId={this.state.taskId}
                            taskName={this.state.taskName}
                            modifying={this.props.modifying}
                            relations={this.state.relations}
                            myTeam={this.state.myTeam}
                            refreshMyTeam={this.refreshMyTeam}
                            task={this.state.taskSelected}
                            firstTab={this.state.firstTab}
                            modifyTaskAndPeople={this.addTasksAndPeople}
                            setStep={this.setStep}/>
              break;
            case 2:
              return <AddIgemTeamCollaborator refreshCollaborators={this.refreshCollaborators} setFirstTab={(value) => {this.setState({firstTab: value})}} setStep={this.setStep}/>
              break;
            default:
                break;
        }
    }

    validate = () =>{

    }

    render(){
        let {fadeAnim} =this.state
        return (
            <>
                <Animated.View
                    style={[
                    styles.content,
                    {opacity: fadeAnim},
                ]}>
                    {this.getStep()}
                </Animated.View>
            </>
        );
    }
};

export default AddPeople;

const styles = StyleSheet.create({
    content: {
        // marginBottom: screen.h / 4.5,
        width: screen.w,
        paddingHorizontal: 25,
        height: '100%',
    },
    title:{
        color: colors.black,
        fontSize: 25,
        marginBottom: 13
    },
    subTitle:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        color: colors.primaryButton,
        marginBottom: 30
    },
    scrollview:{
        height: screen.h*.65,
        width: screen.w-10,
        marginLeft: -20
    },
    container:{
        paddingHorizontal: 20
    },
});
