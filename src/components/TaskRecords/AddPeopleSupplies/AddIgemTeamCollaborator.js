import React, {Component, useState, useEffect, useRef} from 'react';
import {
  Image,
  Text,
  StyleSheet,
  Animated,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import colors, {
  screen,
  tasks,
  shadowStyles
} from '../../../global/constants';
import { getTeams } from "../../../services/api/usersAndTeams/functions"
import { createRelationCategory } from "../../../services/api/relationCategory/functions"
import CustomInput from './../../../components/Global/TextInput/CustomInput';
import Dropdown from './../../../components/Global/Dropdown/Dropdown';

import TaskItem from '../TaskItem'
import { isIphoneXorAbove } from '../../../global/functions';

class AddIgemTeamCollaborator extends Component {
    constructor(props){
        super(props)
        this.state={
            fadeAnim: new Animated.Value(0),
            name: "",
            teams: [],
            nameError: false,
        }
    }

    componentDidMount(){
        Animated.timing(this.state.fadeAnim, {
            toValue: 1,
            duration: 600,
            useNativeDriver: true
          }).start();

        getTeams()
        .then(res => {
          const teams = res.data.map((item, i) => (
            item[1]
          ))
          this.setState({teams: teams})
        })
    }

    saveIgemTeam = () => {
      var error = false;
      console.log(this.state.name)
      if (this.state.name === "") {
        this.setState({nameError: true})
        error = true
      } else {
        this.setState({nameError: false})
      }
      if (!error) {
        createRelationCategory(this.state.name)
        .then(res => {
          this.props.refreshCollaborators()
          this.props.setFirstTab(false)
          this.props.setStep(1)
        })
      }
    }

    render(){
        let {name, category, description, categoryList, fadeAnim} = this.state;

        return (
            <>
                <Animated.View
                    style={[
                    styles.content,
                    {opacity: fadeAnim},
                ]}>
                    <Text style={styles.title}>Add an iGEM team collaborator</Text>
                    <Text style={styles.subTitle}>Please add an external iGEM team collaborator you've worked with for this task.</Text>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollview} nestedScrollEnabled={true} contentContainerStyle={styles.container}>
                      <Dropdown
                        error={this.state.nameError}
                        value={this.state.name}
                        setValue={(value) => {this.setState({name: value})}}
                        choices={this.state.teams}
                        prompt="Please choose a collaborator"
                        defaultValue="Choose an IgemTeam"
                      />

                    </ScrollView>
                    <TouchableOpacity onPress={() => {this.props.setStep(1)}} style={[styles.iconBack, {bottom: isIphoneXorAbove() ? 150:135,}]}>
                        <Image
                            source={require('../../../assets/icons/arrowL.png')}
                            resizeMode={'contain'}
                            style={styles.bluetoothIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.saveIgemTeam} style={styles.icon}>
                        <Image
                            source={require('../../../assets/icons/arrowR.png')}
                            resizeMode={'contain'}
                            style={styles.bluetoothIcon}
                        />
                    </TouchableOpacity>
                </Animated.View>
            </>
        );
    }
};

export default AddIgemTeamCollaborator;

const styles = StyleSheet.create({
    content: {
        // marginBottom: screen.h / 4.5,
        width: screen.w,
        paddingHorizontal: 25,
        height: '100%'
    },
    title:{
        color: colors.black,
        fontSize: 25,
        marginBottom: 13
    },
    subTitle:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        color: colors.primaryButton,
        marginBottom: 30
    },
    scrollview:{
        height: screen.h*.65,
    },
    container:{
        paddingBottom: screen.h*.2,
        marginTop: 5,
        paddingTop: 10
    },
    opacity:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 15
    },
    label:{
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.lightBlack,
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 10,
        marginRight: 8,
        marginBottom: 7
    },
    textLabel:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 15,
        color: colors.grey
    },
    inputStyle: {
      marginBottom: 10,
    },
    icon:{
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.primaryButton,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: isIphoneXorAbove() ? 100:130,
        right: 35,
        ...shadowStyles
    },
    bluetoothIcon:{
        width: 19,
        height: 25
    }
});
