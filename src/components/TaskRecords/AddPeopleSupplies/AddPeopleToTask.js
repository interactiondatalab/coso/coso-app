import React, {Component, useState, useEffect, useRef} from 'react';
import { SwipeListView } from 'react-native-swipe-list-view';

import {
  Image,
  View,
  Text,
  StyleSheet,
  Animated,
  ScrollView,
  TouchableOpacity,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';
import colors, {
  screen,
  tasks,
  shadowStyles,
  consistentShadow
} from '../../../global/constants';

import { isIphoneXorAbove } from '../../../global/functions';
import TaskToAdd from '../TaskToAdd';
import Consumer from '../../Global/Contexts/ConfigContext';

import {deleteLog} from './../../../services/api/tasks/functions'

class AddPeopleToTask extends Component{

    constructor(props){
        super(props)
        this.state={
            list: props.list,
        }
    }


    validate =(ctx)=>{
        if (Object.keys(this.props.peopleTasks).length === 0 ){
            alert('vide')
        }else{
            if (this.props.modifying) {
                this.props.registerTask(this.props.peopleTasks, 'modif')
            }else{
                this.props.registerTask(this.props.peopleTasks)
                setTimeout(
                    ()=>ctx.toggleOpening(), 1000
                )
            }

        }

    }

    supprimer = (id, selected) =>{
        let newList
        for( var i = 0; i < this.state.list.length; i++){
            if ( this.state.list[i] === selected) {
                this.state.list.splice(i, 1);
                newList = this.state.list
                i--;
            }
        }
        Alert.alert(
            "Delete a task",
            `Are you sure you want to delete this ?`,
            [
              {
                text: "No",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Delete", onPress: () =>{
                deleteLog(id).then(
                    (res)=>{
                        if (res.status === 200 || res.status === 202 || res.status === 204) {
                            this.setState({
                                list: newList
                            })
                        } else{
                            Alert.alert("Error", "Something wrong occured, please retry")
                        }
                    }
                )
              }}
            ],
            { cancelable: false }
        );
    }

    render(){

        return (
            <>
                <Text style={styles.title}>Add people you worked with.</Text>
                <Text style={styles.subTitle}>
                    For each activity add people you worked with.{'\n'}
                </Text>
                {this.props.modifying? <Text style={{fontWeight: 'bold', marginBottom: 20}}>Swipe right to delete an activity.</Text> : <Text></Text>}
                <TouchableWithoutFeedback>
                    <>
                    {

                        <SwipeListView
                            disableLeftSwipe
                            style={styles.scrollview}
                            contentContainerStyle={styles.container}
                            data={this.state.list}
                            refreshing={true}
                            renderItem={ (data) =>
                                (
                                    <TaskToAdd setTaskName={this.props.modifying? this.props.settingTaskName:false} modif={this.props.modifying? true:false} peopleTasks={this.props.peopleTasks} update={this.props.update} setStep={this.props.setStep} task={data.item} label={data.item.name}/>
                                )
                            }
                            renderHiddenItem={ (data, rowMap) => (
                                this.props.modifying ?
                                <TouchableOpacity onPress={()=>this.supprimer(data.item.id, data.item)} style={styles.delete}>
                                    <Image
                                        source={require('./../../../assets/icons/can.png')}
                                        resizeMode={'contain'}
                                        style={styles.deleteIcon}
                                    />
                                </TouchableOpacity>
                                :null
                            )}
                            leftOpenValue={screen.w/5}
                        />
                    }

                    </>
                </TouchableWithoutFeedback>
                {
                    this.props.noPrevious ? null:
                    <TouchableOpacity onPress={this.props.back} style={styles.iconBack}>
                        <Image
                            source={require('./../../../assets/icons/arrowL.png')}
                            resizeMode={'contain'}
                            style={styles.bluetoothIcon}
                        />
                    </TouchableOpacity>
                }

                <Consumer>
                    {
                        ctx=>{
                            return(
                                <TouchableOpacity onPress={()=>this.validate(ctx)} style={[styles.icon, this.props.modifying ? {bottom: isIphoneXorAbove() ? 150:135,}:null]}>
                                    <Image
                                        source={require('./../../../assets/icons/checkW.png')}
                                        resizeMode={'contain'}
                                        style={styles.bluetoothIcon}
                                    />
                                </TouchableOpacity>
                            )
                        }
                    }
                </Consumer>

            </>
        );

    }


};

export default AddPeopleToTask;

const styles = StyleSheet.create({
    content: {
        // marginBottom: screen.h / 4.5,
        width: screen.w,
        paddingHorizontal: 25,
        height: '100%'
    },
    title:{
        color: colors.black,
        fontSize: 25,
        marginBottom: 13
    },
    subTitle:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        color: colors.primaryButton,
        marginBottom: 30
    },
    scrollview:{
        maxHeight: screen.h*.65,
        width: screen.w,
        marginLeft: -25,
    },
    container:{
        paddingHorizontal: 20,
        paddingBottom: screen.h*.2,
    },
    icon:{
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.primaryButton,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: isIphoneXorAbove() ? 100:130,
        right: 35,
        ...shadowStyles
    },
    iconBack:{
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: isIphoneXorAbove() ? 100:130,
        right: 85,
        ...shadowStyles
    },
    bluetoothIcon:{
        width: 19,
        height: 25
    },
    delete:{
        backgroundColor: colors.red,
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        borderRadius: 30,
        ...consistentShadow
    },
    deleteIcon:{
        width: 22,
        height: 22
    }
});
