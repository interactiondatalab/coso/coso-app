import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  StyleSheet,
  Animated,
  Image,
  TouchableOpacity,
} from 'react-native';
import colors, {
  screen,
  tasks
} from './../../../global/constants';

const PeopleItem = (props) => {

    const [selected, setSelected] = useState(false)

    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 600,
        useNativeDriver: true
        }).start();

        props.selectionsArray.includes(props.element.id) ? setSelected(true): setSelected(false)
    });


    select = ()=>{
        props.add(props.element.id)
    }



    return (
        props.notClickable ?
        <View style={[styles.label, styles.selected]}>
            <Text style={[selected? styles.selectedTexte : styles.textLabel]}>{props.label}</Text>
        </View>
        :
        <TouchableOpacity onPress={select} style={[styles.label, selected? styles.selected : null]}>
            <Text style={[selected? styles.selectedTexte : styles.textLabel]}>{props.label}</Text>
            <Image
                source={selected ?require('./../../../assets/icons/cancel.png') :require('./../../../assets/icons/addPerson.png')}
                style={styles.check}
                resizeMode={'contain'}
            />
        </TouchableOpacity>

    );
 };

export default PeopleItem;

const styles = StyleSheet.create({
    selected:{
        borderColor: colors.primaryButton,
        backgroundColor: colors.lightPrimary
    },
    label:{
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.lightBlack,
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 10,
        marginRight: 8,
        marginBottom: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignSelf: 'flex-start'
    },
    textLabel:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 15,
        color: colors.grey
    },
    selectedTexte:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 15,
        color: colors.grey,
        opacity: 0.86
    },
    check:{
        width: 14,
        height: 16,
        marginLeft: 10,
    }
});
