import React, {Component, useState, useEffect, useRef, useContext} from 'react';
import {
  Image,
  Text,
  StyleSheet,
  Animated,
  ScrollView,
  TouchableOpacity,
  Alert,
  View
} from 'react-native';
import CustomInput from '../../../components/Global/TextInput/CustomInput';
import colors, {
  screen,
  tasks,
  shadowStyles
} from '../../../global/constants';

import TaskItem from '../TaskItem'
import { isIphoneXorAbove } from '../../../global/functions';
import PeopleItem from './PeopleItem';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createIgemUser } from "../../../services/api/usersAndTeams/functions"
import {ConfigContext} from '../../Global/Contexts/ConfigContext';

import DatePicker from 'react-native-datepicker'

const SelectPeople = (props)=>{

    let ctx = useContext(ConfigContext)


    const [myTeam, setMyTeam] = useState(props.myTeam)
    const [firstTab, setFirstTab] = useState(props.firstTab)
    const [selectionUser, setSelectionsUser] = useState(props.igem_users)
    const [selectionRelation, setSelectionRelation] = useState([])
    const [peopleInput, setPeopleInput] = useState("")
    const [loading, setLoading] = useState(false)
    const [endDate, setEndDate] = useState("")

    useEffect(
        ()=>{
            if (props.modifying) {
                setSelectionsUser(props.peopleTasks[props.taskId].igem_users)
                setSelectionRelation(props.peopleTasks[props.taskId].relation_categories)
                setEndDate(props.peopleTasks[props.taskId].task_finished_at)
            } else {
              setSelectionsUser(props.peopleTasks[props.task.id].igem_users)
              setSelectionRelation(props.peopleTasks[props.task.id].relation_categories)
              setEndDate(props.peopleTasks[props.task.id].task_finished_at)
            }
        },[]
    )

    useEffect(()=>{
      setMyTeam(props.myTeam)
    }, [props.myTeam])

    let setTab = (value)=>{
        setFirstTab(value)
    }

    let validateList = ()=>{
        if (props.modifying) {
            props.modifyTaskAndPeople(props.taskId, selectionUser, selectionRelation, props.peopleTasks[props.taskId].task_done_at, endDate)
            props.setStep(0)
        }else{
            props.modifyTaskAndPeople(props.task.id, selectionUser, selectionRelation, ctx.date, endDate)
            props.setStep(0)
        }
    }

    let selectUser = (person) =>{
        if (selectionUser.includes(person)) {

            let list = [...selectionUser];

            for( var i = 0; i < list.length; i++){
                if ( list[i] === person) {
                    list.splice(i, 1);
                    setSelectionsUser(list);
                }
            }
        } else{
            setSelectionsUser([...selectionUser, person]);
        }
    }

    let selectRelation = (relation) =>{
        if (selectionRelation.includes(relation)) {

            let list = [...selectionRelation];

            for( var i = 0; i < list.length; i++){
                if ( list[i] === relation) {
                    list.splice(i, 1);
                    setSelectionRelation(list);
                }
            }
        } else{
            setSelectionRelation([...selectionRelation, relation]);
        }
    }

    let addIgemUser = () => {
      if (!loading) {
        setLoading(true)
        createIgemUser(peopleInput).then(
          res=>{
              setLoading(false)
              if (res.status === 201) {
                  props.refreshMyTeam()
                  console.log(res.data)
                  setMyTeam(myTeam.concat(res.data))
              } else{
                  console.log(res.data)
              }
          }
        )
      }
    }

    let teamList = myTeam
      .filter(element => element.username.toLowerCase().includes(peopleInput.toLowerCase()))
      .map(element=>{
        return(
            <PeopleItem selectionsArray={selectionUser} element={element} label={element.username} add={selectUser}/>
        )
    })

    let others = props.relations.map(element=>{
        if (element.name === "Another iGEM team") {
            return (
              <PeopleItem selectionsArray={selectionRelation} element={element} label={element.name} add={() => {props.setStep(2)}}/>
            )
        } else {
          return(
            <PeopleItem selectionsArray={selectionRelation} element={element} label={element.name} add={selectRelation}/>
          )
        }
    })

    return (
        <>
            <Text style={styles.title}>How long and with whom did you worked with on</Text>
            <TaskItem selectionsArray={props.modifying ? [props.taskName]:[props.task.name]} label={ props.modifying ? props.taskName :props.task.name} notClickable={true}/>
            <Text style={styles.subTitle}>
                Select the end date for this activity.
            </Text>
            <DatePicker
              date={endDate}
              mode="date"
              placeholder="select end date"
              format="YYYY-MM-DD"
              minDate={ctx.date}
              style={{marginBottom:10 }}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              iconSource={require('./../../../assets/images/calendar.png')}
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 0,
                  top: 4,
                  marginLeft: 0,
                  width:25,
                  height:25,
                  marginTop:2,
                },
                dateInput: {
                  paddingRight: 36,
                  borderWidth: 1,
                  borderColor: colors.borderColor,
                  /* borderColor: colors.lightGrey, */
                  borderRadius: 4,
                }
                // ... You can check the source to find the other keys.
              }}
              onDateChange={(date) => {setEndDate(date)}}
            />
            <Text style={styles.subTitle}>
                Select people you worked with.
            </Text>
            <View style={styles.selections}>
                <TouchableOpacity onPress={()=>setTab(true)} style={styles.wrapperLabel}>
                    <Text style={[styles.textLabel, firstTab && styles.selectedTexte]}>Your Team</Text>
                    {firstTab && <View style={styles.ariane}/>}
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>setTab(false)} style={styles.wrapperLabel}>
                    <Text style={[styles.textLabel, !firstTab && styles.selectedTexte]}>External collaborator</Text>
                    {!firstTab && <View style={styles.ariane}/>}
                </TouchableOpacity>
            </View>
            <ScrollView nestedScrollEnabled={true} style={styles.scrollview} contentContainerStyle={styles.container}>
                {firstTab &&
                  <View style={styles.searchSelection}>
                    <CustomInput
                      placeholder={'Add People'}
                      label={'Add People'}
                      update={(stateName, value) => {setPeopleInput(value)}}
                      customStyle={styles.inputStyle}
                      helpText="Type and press enter or click on + to add a missing member"
                    />
                    <Icon.Button
                      style={styles.addIcon}
                      name={loading ? "spinner" : "plus"}
                      size={20}
                      backgroundColor="transparent"
                      color={colors.primaryButton}
                      onPress={addIgemUser}
                      underlayColor="transparent"
                    />
                    </View>
                }
                <TouchableOpacity style={styles.opacity} activeOpacity={1}>
                    {firstTab ? myTeam && teamList: props.relations && others}
                </TouchableOpacity>
            </ScrollView>

            <TouchableOpacity onPress={()=>props.setStep(0)} style={[styles.iconBack, props.modifying ? {bottom: isIphoneXorAbove() ? 150:135,}:null]}>
                <Image
                    source={require('./../../../assets/icons/arrowL.png')}
                    resizeMode={'contain'}
                    style={styles.bluetoothIcon}
                />
            </TouchableOpacity>
            <TouchableOpacity onPress={validateList} style={[styles.icon, props.modifying ? {bottom: isIphoneXorAbove() ? 150:135,}:null]}>
                <Image
                    source={require('./../../../assets/icons/arrowR.png')}
                    resizeMode={'contain'}
                    style={styles.bluetoothIcon}
                />
            </TouchableOpacity>
        </>
    );

};

export default SelectPeople;

const styles = StyleSheet.create({
    content: {
        // marginBottom: screen.h / 4.5,
        width: screen.w,
        paddingHorizontal: 25,
        height: '100%'
    },
    title:{
        color: colors.black,
        fontSize: 25,
        marginBottom: 13
    },
    subTitle:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        color: colors.primaryButton,
        marginBottom: 5
    },
    scrollview:{
        height: screen.h*.65,
        width: screen.w-10,
        marginLeft: -20
    },
    container:{
        paddingHorizontal: 20,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingTop: 20,
        paddingBottom: 200,
    },
    opacity:{
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    selections:{
        width: screen.w,
        borderBottomWidth: 1,
        borderColor: colors.lightBlack,
        marginTop:30,
        marginLeft: -25,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    textLabel:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 12,
        color: colors.inactiveText,
        paddingBottom: 10
    },
    selectedTexte:{
        color: colors.primaryButton
    },
    wrapperLabel:{
        alignSelf: 'flex-start',
    },
    ariane:{
        height: 3,
        backgroundColor: colors.primaryButton,
        borderTopStartRadius: 3,
        borderTopEndRadius: 3,
    },
    icon:{
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.primaryButton,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: isIphoneXorAbove() ? 100:130,
        right: 35,
        ...shadowStyles
    },
    iconBack:{
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: isIphoneXorAbove() ? 100:130,
        right: 85,
        ...shadowStyles
    },
    bluetoothIcon:{
        width: 19,
        height: 25
    },
    searchSelection: {
      flexDirection: 'row',
    },
    addIcon: {
      paddingVertical: 20,
      paddingHorizontal: 5,
    }
});
