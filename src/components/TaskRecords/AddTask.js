import React, {Component, useState, useEffect, useRef} from 'react';
import {
  Image,
  Text,
  StyleSheet,
  Animated,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import colors, {
  screen,
  tasks,
  shadowStyles
} from '../../global/constants';
import { createTask } from "../../services/api/tasks/functions"
import CustomInput from './../../components/Global/TextInput/CustomInput';
import Dropdown from './../../components/Global/Dropdown/Dropdown';

import TaskItem from './TaskItem'
import { isIphoneXorAbove } from '../../global/functions';

class AddTask extends Component {
    constructor(props){
      let tmp = props.tasksList.map(element => {
          return element.category;
      })
      let categoryList = [...new Set(tmp)];
        super(props)
        this.state={
            fadeAnim: new Animated.Value(0),
            name: "",
            category: "",
            description: "",
            categoryList: categoryList,
            nameError: false,
            categoryError: false,
        }
    }

    componentDidMount(){
        Animated.timing(this.state.fadeAnim, {
            toValue: 1,
            duration: 600,
            useNativeDriver: true
          }).start();
    }

    saveTask = () => {
      var error = false;
      if (this.state.name === "") {
        this.setState({nameError: true})
        error = true
      } else {
        this.setState({nameError: false})
      }
      if (this.state.category === "" || this.state.category === "Choose a category") {
        this.setState({categoryError: true})
        error = true
      } else  {
        this.setState({categoryError: false})
      }
      if (!error) {
        createTask(this.state.name, this.state.category, this.state.description)
        .then(res => {
          this.props.refreshTasks()
          this.props.setStep(1)
        })
      }
    }

    render(){
        let {name, category, description, categoryList, fadeAnim} = this.state;


        return (
            <>
                <Animated.View
                    style={[
                    styles.content,
                    {opacity: fadeAnim},
                ]}>
                    <Text style={styles.title}>Add a custom task</Text>
                    <Text style={styles.subTitle}>If none of the default tasks suits your needs, you can add a custom one here.</Text>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollview} nestedScrollEnabled={true} contentContainerStyle={styles.container}>
                      <CustomInput
                        error={this.state.nameError}
                        placeholder={'Task name'}
                        label={'Task Name'}
                        update={(stateName, value) => {this.setState({name: value})}}
                        customStyle={styles.inputStyle}
                      />
                      <CustomInput
                        error={false}
                        placeholder={'Description'}
                        label={'Description'}
                        update={(stateName, value) => {this.setState({description: value})}}
                        customStyle={styles.inputStyle}
                      />
                      <Dropdown
                        error={this.state.categoryError}
                        value={this.state.category}
                        setValue={(value) => {this.setState({category: value})}}
                        choices={categoryList}
                        prompt="Please choose a category"
                        defaultValue="Choose a category"
                      />

                    </ScrollView>
                    <TouchableOpacity onPress={() => {this.props.setStep(1)}} style={[styles.iconBack, {bottom: isIphoneXorAbove() ? 150:135,}]}>
                        <Image
                            source={require('./../../assets/icons/arrowL.png')}
                            resizeMode={'contain'}
                            style={styles.bluetoothIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.saveTask} style={styles.icon}>
                        <Image
                            source={require('./../../assets/icons/arrowR.png')}
                            resizeMode={'contain'}
                            style={styles.bluetoothIcon}
                        />
                    </TouchableOpacity>
                </Animated.View>
            </>
        );
    }
};

export default AddTask;

const styles = StyleSheet.create({
    content: {
        // marginBottom: screen.h / 4.5,
        width: screen.w,
        paddingHorizontal: 25,
        height: '100%'
    },
    title:{
        color: colors.black,
        fontSize: 25,
        marginBottom: 13
    },
    subTitle:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        color: colors.primaryButton,
        marginBottom: 30
    },
    scrollview:{
        height: screen.h*.65,
    },
    container:{
        paddingBottom: screen.h*.2,
        marginTop: 5,
        paddingTop: 10
    },
    opacity:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 15
    },
    label:{
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.lightBlack,
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 10,
        marginRight: 8,
        marginBottom: 7
    },
    textLabel:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 15,
        color: colors.grey
    },
    inputStyle: {
      marginBottom: 10,
    },
    icon:{
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.primaryButton,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: isIphoneXorAbove() ? 100:130,
        right: 35,
        ...shadowStyles
    },
    bluetoothIcon:{
        width: 19,
        height: 25
    }
});
