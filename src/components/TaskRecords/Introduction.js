import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  Image,
  View,
  StyleSheet,
  Animated,
  TouchableOpacity
} from 'react-native';
import colors, {
  screen, shadowStyles,
} from '../../global/constants';
import { isIphoneXorAbove } from '../../global/functions';
import { AsyncStorage } from 'react-native';

const Introduction = (props) => {

  const [ skip, setSkip ] = useState(null);

  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 600,
      useNativeDriver: true
    }).start();
  });

  // saveSkip = async () => {
  //   try {
  //     await AsyncStorage.setItem(
  //       '@settings:skipIntro',
  //       skip
  //     );
  //   } catch (error) {
  //    console.log(error);
  //   }
  // };

  const storeData = async () => {
    try {
      console.log(JSON.stringify(skip))
      await AsyncStorage.setItem('@skipIntro', JSON.stringify(skip))
    } catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    console.log(skip)
    if (skip!==null)
    {
      storeData();
    }
  }, [skip])

  let updateState = (stateLabel, value) => {
    setEmail(value);
  };


  return (
    <>
        <Animated.View
            style={[
            styles.content,
            {opacity: fadeAnim},
        ]}>
            <Text style={styles.title}>Hey there,</Text>
              <Image
                  source={require('./../../assets/images/robot.png')}
                  resizeMode={'contain'}
                  style={styles.imageIllu}
                />
                <Text style={styles.questions}>Are you ready to add activities to your logbook?</Text>
                <Text style={styles.answers}>It’s super easy and it won’t take you more than 5 min.</Text>
                <Text style={styles.questions}>You forgot something last time?</Text>
                <Text style={styles.answers}>No worries, feel free to come back later and modify existing activities or record new ones!</Text>
                <TouchableOpacity onPress={() =>{if (skip) {setSkip(!skip)} else {setSkip(true)}}}>
                  <View style={{flexDirection:"row"}}>
                  <Image
                      source={require('./../../assets/icons/check.png')}
                      resizeMode={'contain'}
                      style={styles.iconSkip}
                  />
                  <Text style={skip?styles.skip:styles.unskip}>Don't show me this screen again.</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={props.next} style={styles.icon}>
                  <Image
                      source={require('./../../assets/icons/arrowR.png')}
                      resizeMode={'contain'}
                      style={styles.bluetoothIcon}
                  />
                </TouchableOpacity>
        </Animated.View>
    </>
  );
};

export default Introduction;

const styles = StyleSheet.create({
  content: {
    // marginBottom: screen.h / 4.5,
    width: screen.w,
    paddingHorizontal: 25,
    height: '100%'
  },
  title: {
    color: colors.black,
    fontSize: 25,
    textAlign: 'center'
  },
  imageIllu: {
    height: 150,
    marginTop: 28,
    width: screen.w - 80,
    marginBottom: 40,
    alignSelf: 'center',
  },
  questions:{
    width: screen.w-100,
    alignSelf: 'center',
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    color: colors.grey,
    textAlign: 'center',
    lineHeight: 20,
    marginBottom: 10
  },
  answers:{
    width: screen.w-100,
    alignSelf: 'center',
    fontFamily: 'Montserrat-Regular',
    fontSize: 17,
    color: colors.lightGrey,
    textAlign: 'center',
    lineHeight: 27,
    marginBottom: 30
  },
  icon:{
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: colors.primaryButton,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: isIphoneXorAbove() ? 100:130,
    right: 35,
    ...shadowStyles
  },
  bluetoothIcon:{
    width: 19,
    height: 25
  },
  iconSkip:{
    width: 19,
    height: 25,
    marginRight:15
  },
  skip:{
    color: colors.primaryButton,
    fontWeight:"bold"
  },
  unskip:{
    color: colors.lightGrey
  }
});
