import React, {useState, useEffect, useRef} from 'react';
import {
  Text,
  StyleSheet,
  Animated,
  Image,
  TouchableOpacity,
  View
} from 'react-native';
import colors, {
  screen,
  tasks
} from './../../global/constants';

const TaskItem = (props) => {

    const [selected, setSelected] = useState(false)

    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 600,
        useNativeDriver:false
        }).start();

        props.selectionsArray.includes(props.task) ? setSelected(true): setSelected(false)
    });


    select = (task)=>{
        props.addSelection(task)
    }



    return (
        props.notClickable ?
        <View style={[styles.label, styles.selected]}>
            <Image
                source={require('./../../assets/icons/check.png')}
                style={styles.check}
                resizeMode={'contain'}
            />
            <Text style={[selected? styles.selectedTexte : styles.textLabel]}>{props.label}</Text>
        </View>
        : props.isNoWork ?
        <TouchableOpacity onPress={()=>select(props.task)} style={[styles.label_nowork, selected? styles.selected : null]}>
            {selected && <Image
                source={require('./../../assets/icons/check.png')}
                style={styles.check}
                resizeMode={'contain'}
            />}
            <Text style={[selected? styles.selectedTexte : styles.textLabel]}>{props.label}</Text>
        </TouchableOpacity>
        :
        <TouchableOpacity onPress={()=>select(props.task)} style={[styles.label, selected? styles.selected : null]}>
            {selected && <Image
                source={require('./../../assets/icons/check.png')}
                style={styles.check}
                resizeMode={'contain'}
            />}
            <Text style={[selected? styles.selectedTexte : styles.textLabel]}>{props.label}</Text>
        </TouchableOpacity>

    );
 };

export default TaskItem;

const styles = StyleSheet.create({
    selected:{
        borderColor: colors.primaryButton,
        backgroundColor: colors.lightPrimary
    },
    additional: {
        alignSelf: 'flex-start',
        marginTop: -5,
        marginBottom: 10
    },
    label_nowork:{
        backgroundColor: 'rgba(224, 231, 234, 0.80)',
        borderWidth: 1,
        borderColor: colors.lightBlack,
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 10,
        marginRight: 8,
        marginBottom: 7,
        flexDirection: 'row',
        alignItems: 'center'
    },
    label:{
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.lightBlack,
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 10,
        marginRight: 8,
        marginBottom: 7,
        flexDirection: 'row',
        alignItems: 'center'
    },
    textLabel:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 15,
        color: colors.grey
    },
    selectedTexte:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 15,
        color: colors.grey,
        opacity: 0.86
    },
    check:{
        width: 14,
        height: 16,
        marginRight: 10
    }
});
