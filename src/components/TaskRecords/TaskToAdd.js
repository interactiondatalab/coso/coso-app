import React, {useState, useEffect, useRef, useContext} from 'react';
import {
    View,
  Text,
  StyleSheet,
  Animated,
  Image,
  TouchableOpacity,
  AsyncStorage,
  TouchableWithoutFeedback,
} from 'react-native';
import colors, {
  screen,
  tasks,
  shadowStyles,
  consistentShadow
} from './../../global/constants';
import { getTask } from '../../services/api/tasks/functions';
import Icon from 'react-native-vector-icons/FontAwesome';
import {ConfigContext} from '../Global/Contexts/ConfigContext';
import moment from 'moment'

const TaskToAdd = (props) => {

    const [label, setLabel]= useState('')
    const [cats, setCat]= useState(0)
    const [users, setUsers]= useState(0)
    const [days, setDays]= useState(1)

    let ctx = useContext(ConfigContext)
    console.log()

    useEffect(()=>{
        setTimeout(
            ()=>{
                if (props.label) {
                    setLabel(props.label)
                    if(props.peopleTasks[`${props.task.id}`]){
                        if (props.peopleTasks[`${props.task.id}`].relation_categories) {
                            setCat(props.peopleTasks[`${props.task.id}`].relation_categories.length)
                        }
                        if (props.peopleTasks[`${props.task.id}`].igem_users) {
                            setUsers(props.peopleTasks[`${props.task.id}`].igem_users.length)
                        }
                        if (props.peopleTasks[`${props.task.id}`].task_finished_at) {
                            setDays(moment(props.peopleTasks[`${props.task.id}`].task_finished_at).diff(moment(props.peopleTasks[`${props.task.id}`].task_done_at), 'days')+1)
                        }
                    }
                }else{
                    let result = getTask(props.task.task_id).then(
                        res=>{
                            //console.log(props.task.task_id, res.data)
                            setLabel(res.data.name)
                            if (res.data.task_finished_at) {
                                setDays(moment(res.data.task_finished_at).diff(moment(res.data.task_done_at), 'days')+1)
                            }
                        }
                    )
                    if (props.modif) {
                      console.log("modif")
                        if(props.peopleTasks[`${props.task.id}`]){
                          console.log(props.peopleTasks)
                            if (props.peopleTasks[`${props.task.id}`].relation_categories) {
                                setCat(props.peopleTasks[`${props.task.id}`].relation_categories.length)
                            }
                            if (props.peopleTasks[`${props.task.id}`].igem_users) {
                                setUsers(props.peopleTasks[`${props.task.id}`].igem_users.length)
                            }
                            if (props.peopleTasks[`${props.task.id}`].task_finished_at) {
                                setDays(moment(props.peopleTasks[`${props.task.id}`].task_finished_at).diff(moment(props.peopleTasks[`${props.task.id}`].task_done_at), 'days')+1)
                            }
                        }
                    }else{
                        if (props.task.igem_users[Object.keys(props.task.igem_users)[0]]) {
                            let usersTabs = props.task.igem_users[props.task.team_id].length
                            /*  props.task.igem_users[Object.keys(props.task.igem_users)[0]].forEach(element => {
                                console.log(element)
                            }); */
                            if (props.task.igem_users[props.task.team_id].length > 0) {
                                setUsers(props.task.igem_users[props.task.team_id].length)
                            }else{
                                setUsers(0)
                            }
                        }
                        setCat(props.task.relation_categories.length)
                        if (props.task.task_finished_at) {
                            setDays(moment(props.task.task_finished_at).diff(moment(props.task.task_done_at), 'days')+1)
                        }
                    }


                }
            }, props.index ? props.indx*150:150
        )

    })

    let open = ()=>{
        if (props.label) {
            props.update(props.task)
            props.setStep(1)
        }else{
            if (props.modif) {
                props.setTaskName(label, props.task.id)
                props.setStep(1)
            }else{
                if (props.show) {
                    props.setModal()
                }
            }

        }

    }

    let getLabels = () =>{
        if ( users !== 0 ||  cats !== 0 ){
            return (
                <View style={styles.label}>
                    {
                        users !== 0 ?
                        <View style={styles.infos}>
                            <Image
                                source={require('./../../assets/icons/group.png')}
                                style={styles.group}
                                resizeMode={'contain'}
                            />
                                <Text style={styles.number}>{users}{" "}{users > 1 ? 'People':'Person'}</Text>
                        </View>:null
                    }
                    {
                        cats !== 0 ?
                        <View style={styles.infos}>
                            <Image
                                source={require('./../../assets/icons/others.png')}
                                style={styles.other}
                                resizeMode={'contain'}
                            />
                            <Text style={styles.number}>{cats}{" "}{cats > 1 ? 'Externals':'External'}</Text>
                        </View>:null

                    }
                    {
                        props.show ? <Text></Text>:<Image
                            source={require('./../../assets/icons/addPeople.png')}
                            style={styles.addPeople}
                            resizeMode={'contain'}
                        />
                    }

                </View>
            )
        }
    }

    let testIcon =()=>{
        if((users == 0) &&  (cats == 0)){
            return (
                !props.show && <Image
                source={require('./../../assets/icons/addPeople.png')}
                style={styles.addPeople}
                resizeMode={'contain'}
            />
            )
        }

    }

    return (
        <TouchableOpacity onPress={open} style={[styles.wrapper]} activeOpacity={props.modif ? 1:.7}>
            <View style={[styles.label]}>
                <Text style={styles.textLabel}>{label}</Text>
                <TouchableOpacity onPress={open}>
                    {props.peopleTasks[`${props.task.id}`] && testIcon()}
                </TouchableOpacity>
            </View>

            {(props.peopleTasks[`${props.task.id}`] || users !== 0 || cats !== 0) && getLabels()}

            <View style={styles.label}>
                <View style={styles.infos}>
                  <Icon name="calendar" size={16} style={{marginRight:10}} color={colors.agendaGrey}/>
                  <Text style={styles.number}>{days}{" "}{days > 1 ? 'Days':'Day'}</Text>
                </View>
                {
                    props.show ? <Text></Text>:<Icon name="calendar-plus-o" size={25} style={{marginRight:10}} color={colors.white}/>
                }

            </View>


        </TouchableOpacity>
    );
 };

export default TaskToAdd;

const styles = StyleSheet.create({
    wrapper:{
        backgroundColor: colors.primaryButton,
        width: '100%',
        borderColor: colors.lightBlack,
        paddingLeft: 15,
        paddingRight: 10,
        borderRadius: 10,
        marginRight: 8,
        marginBottom: 18,
        paddingVertical: 10,
        minHeight: 70,
        justifyContent: 'center',
        ...consistentShadow
    },
    label:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textLabel:{
        fontSize: 16,
        color: colors.white,
    },
    addPeople:{
        width: 25,
        height: 25,
        marginRight: 10
    },
    number:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 14,
        color: colors.grey,
        maxWidth: screen.w/4
    },
    group:{
        width: 16,
        height: 16,
        marginRight: 10
    },
    other:{
        width: 17,
        height: 17,
        marginRight: 10
    },
    infos:{
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.lightPrimary,
        paddingHorizontal: 13,
        paddingVertical: 7,
        borderRadius: 8,
        marginTop: 10,
        marginBottom: 5
    }
});
