import React, {Component, useState, useEffect, useRef} from 'react';
import {
  Image,
  Text,
  StyleSheet,
  Animated,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import colors, {
  screen,
  tasks,
  shadowStyles
} from '../../global/constants';

import TaskItem from './TaskItem'
import { isIphoneXorAbove } from '../../global/functions';

class WorksSelection extends Component {
    constructor(props){
      console.log("Workselection PROPS! !!!")
      console.log(props.workList)
      console.log(props.initialList)
      console.log(props.tasksList)
      console.log(props.addTask)

      console.log("-------------------------")
      console.log("-------------------------")
        super(props)
        this.state={
            selected : props.initialList,
            selections : props.initialList,
            fadeAnim: new Animated.Value(0)
        }
    }

    componentDidMount(){
        Animated.timing(this.state.fadeAnim, {
            toValue: 1,
            duration: 600,
            useNativeDriver: true
          }).start();
    }

    select = (task)=>{
        if (this.state.selections.includes(task)) {
            let list = [...this.state.selections];

            for( var i = 0; i < list.length; i++){
                if ( list[i] === task) {
                    list.splice(i, 1);
                    this.setState({
                        selections: list
                    })
                }
            }
        } else{
            this.setState({
                selections: [...this.state.selections,task]
             })
        }
    }

    validate = () =>{
        if (this.state.selections.length !== 0) {
            this.props.updateWork(this.state.selections)
        }else{
            Alert.alert("Attention","You have not selected any tasks.")
        }
    }

    render(){
        let {selections, fadeAnim} =this.state;
        let isNoWork;
        let tasksList = this.props.tasksList.map(element => {
            isNoWork = false;

            if(element.id === 20) {
              isNoWork = true;
            }

            if(this.state.selected.length > 0 && this.state.selected.some(item => ( item.task_id === element.id ))) {
              return (
                  <TaskItem key={element.id} isNoWork={isNoWork} notClickable={true} selectionsArray={selections} task={element} label={element.name} addSelection={this.select}/>
              )
            } else {
              return (
                  <TaskItem key={element.id} isNoWork={isNoWork} selectionsArray={selections} task={element} label={element.name} addSelection={this.select}/>
              )
            }
        })

        return (
            <>
                <Animated.View
                    style={[
                    styles.content,
                    {opacity: fadeAnim},
                ]}>
                    <Text style={styles.title}>What did you do for your project today?</Text>
                    <Text style={styles.subTitle}>Select one or multiple activities from the list. If you didn’t work for iGEM, select No iGEM work.</Text>
                    <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollview} nestedScrollEnabled={true} contentContainerStyle={styles.container}>
                        <TouchableOpacity TouchableOpacity activeOpacity={1} style={styles.opacity}>
                            {tasksList}
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.props.setStep(3)}}>
                            <Text style={styles.textLabel}>Add a custom task</Text>
                        </TouchableOpacity>
                    </ScrollView>
                    <TouchableOpacity onPress={this.validate} style={styles.icon}>
                        <Image
                            source={require('./../../assets/icons/arrowR.png')}
                            resizeMode={'contain'}
                            style={styles.bluetoothIcon}
                        />
                    </TouchableOpacity>
                </Animated.View>
            </>
        );
    }
};

export default WorksSelection;

const styles = StyleSheet.create({
    content: {
        // marginBottom: screen.h / 4.5,
        width: screen.w,
        paddingHorizontal: 25,
        height: '100%'
    },
    title:{
        color: colors.black,
        fontSize: 25,
        marginBottom: 13
    },
    subTitle:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 13,
        color: colors.primaryButton,
        marginBottom: 30
    },
    scrollview:{
        height: screen.h*.65,
    },
    container:{
        paddingBottom: screen.h*.2,
        marginTop: 15
    },
    opacity:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 15
    },
    label:{
        backgroundColor: colors.white,
        borderWidth: 1,
        borderColor: colors.lightBlack,
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 10,
        marginRight: 8,
        marginBottom: 7
    },
    textLabel:{
        fontFamily: 'Montserrat-Medium',
        fontSize: 15,
        color: colors.grey
    },
    icon:{
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: colors.primaryButton,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: isIphoneXorAbove() ? 100:130,
        right: 35,
        ...shadowStyles
    },
    bluetoothIcon:{
        width: 19,
        height: 25
    }
});
