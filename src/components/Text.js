import React from 'react';
import styled from 'styled-components/native';

export const TextS = styled.Text`
  font-size: ${props => props.theme.sizes};
  color: ${props => props.theme.textcolor};
`;

export const TextM = styled.Text`
  font-size: ${props => props.theme.sizem};
  color: ${props => props.theme.textcolor};
`;

export const TextL = styled.Text`
  font-size: ${props => props.theme.sizel};
  font-weight: bold;
  color: ${props => props.theme.textcolor};
`;

export const TextXL = styled.Text`
  font-size: ${props => props.theme.sizexl};
  font-weight: bold;
  color: ${props => props.theme.textcolor};
`;

export const TextInfo = styled(TextS)`
  color: #aaa;
  color: ${props => props.theme.textinfocolor};
`;

export const TextError = styled(TextM)`
  color: red;
`;