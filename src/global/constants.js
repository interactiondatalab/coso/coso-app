import {Dimensions} from 'react-native';
import {isIphoneXorAbove} from './functions';

export const screen = {
  w: Dimensions.get('screen').width,
  h: Dimensions.get('screen').height,
};

export const config = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80
};

const colors = {
  white: 'rgb(255, 255, 255)',
  paleGrey: 'rgb(228, 245, 246)',
  black: 'rgba(0, 0, 0, 0.87)',
  lightBlack: 'rgba(0, 0, 0, 0.12)',
  titleBlack: 'rgb(40, 47, 46)',
  titleBlackLow: 'rgba(40, 47, 46, .6)',
  titleBlackLowLow: 'rgba(40, 47, 46, .87)',
  hardGrey: 'rgb(40, 47, 46)',
  grey: 'rgba(46, 54, 52, 0.87)',
  lightGrey: 'rgba(0, 0, 0, 0.6)',
  agendaGrey: 'rgba(60, 70, 68, 0.87)',
  headerHomeColor: 'rgb(255,248,236)',
  greenGrey: 'rgb(70, 79, 78)',
  shadow: 'rgb(73, 77, 111)',
  borderColor: 'rgba(0, 0, 0, 0.32)',
  inactiveIcon: 'rgba(0, 0, 0, 0.54)',
  dotColor: 'rgba(0, 0, 0, 0.38)',
  inactiveButton: 'rgba(0, 0, 0, 0.10)',
  inactiveText: 'rgba(0, 0, 0, 0.38)',
  activeDot: 'rgb(59, 140, 144)',
  primaryButton: 'rgb(59, 140, 144)',
  secondPrimary: 'rgb(75, 151, 155)',
  lightPrimary: 'rgb(172, 213, 211)',
  blueLight : 'rgba(0, 197, 176, .12)',
  red: 'rgb(208, 63, 59)',
};

export const defaultScreenStyles = {
  container: {
    flex: 1,
    backgroundColor: 'red',
    zIndex: 10,
  },
  statusBar: {
    flex: 0,
    backgroundColor: colors.headerHomeColor,
    width: '100%',
  },
  wrapperContent: {
    width: screen.w,
    height: '100%',
    alignItems: 'center',
    position: 'relative',
    zIndex: 10,
  },
};

export const backButtonStyles = {
  backPress: {
    position: 'absolute',
    zIndex: 10,
    top: isIphoneXorAbove() ? 50 : 20,
    left: 15,
    padding: 10,
    width: 30,
    height: 40,
  },
  backButton: {
    width: 11,
    height: 20,
  },
};

export const bigShadowStyles = {
  shadowColor: colors.black,
  shadowOffset: {
    width: 0,
    height: 3,
  },
  shadowOpacity: 0.3,
  shadowRadius: 10,
  elevation: 2,
};
export const shadowStyles = {
  shadowColor: colors.black,
  shadowOffset: {
    width: 0,
    height: 1.5,
  },
  shadowOpacity: 0.3,
  shadowRadius: 1,
  elevation: 2,
};
export const consistentShadow = {
  shadowColor: colors.black,
  shadowOffset: {
    width: 0,
    height: 3,
  },
  shadowOpacity: 0.3,
  shadowRadius: 4,
  elevation: 2,
};
export const consistentTopShadow = {
  shadowColor: colors.black,
  shadowOffset: {
    width: 0,
    height: Platform.OS==='android' ? 0:1,
  },
  shadowOpacity: 0.3,
  shadowRadius: Platform.OS==='android' ? 0:4,
  elevation: Platform.OS==='android' ? 0:2,
};
export const shadowTopStyles = {
  shadowColor: colors.lightGrey,
  shadowOffset: {
    width: 0,
    height: -1,
  },
  shadowOpacity: 0.1,
  shadowRadius: 5,
  elevation: 2,
};

export const tasks = [
  'Brainstorming','Planning tasks', 'Meetings', 'Developing protocols', 'Reading papers', 'Education event',
  'Meetups', 'Collaborating with teams', 'Lab maintenance', 'Running assays', 'Preparing samples', 'Analysing results',
  'Interpering results', 'Preparing docs to share outside', 'Public outreach event', 'Writing / preparing presentations',
  'Project administration', 'Software development', 'Hardware development'
]

export default colors;
