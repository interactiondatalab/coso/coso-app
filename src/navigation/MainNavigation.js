import React, {useEffect} from 'react';
import messaging from '@react-native-firebase/messaging';
import { AsyncStorage } from 'react-native';

import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {withTheme} from 'styled-components';
import {OnboardingScreen} from './../screens/onboarding/OnboardingScreen';
import {AuthenticationScreen} from './../screens/authentication/AuthenticationScreen';
import LoginScreen from './../screens/authentication/Login/LoginScreen';
import ResetPasswordScreen from './../screens/authentication/Login/ResetPasswordScreen';
import ConfirmResetScreen from './../screens/authentication/Login/ConfirmResetScreen';
import ChangePasswordScreen from './../screens/authentication/Login/ChangePasswordScreen';
import ConfirmChangePasswordScreen from './../screens/authentication/Login/ConfirmChangePasswordScreen';
import WelcomeScreen from './../screens/authentication/SignUp/WelcomeScreen';
import ValidateRegistrationScreen from './../screens/authentication/SignUp/ValidateRegistrationScreen';
import RegisterScreen from './../screens/authentication/SignUp/RegisterScreen';
import HomeScreen from './../screens/home/HomeScreen';
import SettingsScreen from './../screens/settings/SettingsScreen';
import MyAccountScreen from './../screens/settings/MyAccountScreen';
import BlankTab from '../screens/tasks/BlankTabScreen'
import TabBarIcon from './../components/Global/TabBar/TabBarIcon';
import AnalyticsMainScreen from '../screens/analytics/AnalyticsMainScreen';
import SurveyMainScreen from './../screens/surveys/SurveyMainScreen'
import colors, { screen } from '../global/constants';
import { OpenDrawer } from '../components/Global/Buttons/OpenDrawer';
import Consumer from '../components/Global/Contexts/ConfigContext';
import SurveyResumeScreen from '../screens/surveys/SurveyResumeScreen';
import SurveyWebView from '../screens/surveys/SurveyWebView';
import NewsMainScreen from '../screens/news/NewsMainScreen';
import NewsWebView from '../screens/news/NewsWebView';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const SettingsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Setting"
    >
      <Stack.Screen name="Setting" component={SettingsScreen} />
      <Stack.Screen name="MyAccount" component={MyAccountScreen}/>
      <Stack.Screen name="HowWorks" component={OnboardingScreen} />
    </Stack.Navigator>
  );
};

const SurveyStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="SurveyMain">
      <Stack.Screen name="SurveyMain" component={SurveyMainScreen} />
      <Stack.Screen name="ResumeScreen" component={SurveyResumeScreen} />
      <Stack.Screen options={{tabBarVisible: false}} name="Survey" component={SurveyWebView} />
    </Stack.Navigator>
  );
};

const NewsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="NewsMain">
      <Stack.Screen name="NewsMain" component={NewsMainScreen} />
      <Stack.Screen options={{tabBarVisible: false}} name="NewsWebView" component={NewsWebView} />
    </Stack.Navigator>
  );
};
// <Stack.Screen options={{tabBarVisible: false}} name="Survey" component={SurveyScreen} />

const HomeTab = () => {
  return (
    /* Adding the context on the homeTab to have it stored in the bottom tab navigator */
    <Consumer>
      {
        context=>{
          return(
            <Tab.Navigator
            lazy={false}
            screenOptions={({route}) => ({
              tabBarIcon: ({focused}) => {
                let iconName;
                let gap = false;
                let secondGap = false;
                switch (route.name) {
                  case 'Home':
                    iconName = 'home';
                    break;
                  case 'Charts':
                    iconName = 'charts';
                    gap=true;
                    break;
                  case 'Assignments':
                    secondGap=true;
                    iconName = 'assignments';
                    break;
                  case 'Settings':
                    iconName = 'settings';
                    break;

                  default:
                    break;
                }

                // You can return any component that you like here!
                return <TabBarIcon focused={focused} iconName={iconName} gap={gap} secondGap={secondGap}/>;
              },
            })}
            tabBarOptions={{
              style: {
                backgroundColor: colors.paleGrey,
                zIndex: 0,
                borderTopWidth:0,
                height: screen.h*.1, paddingTop: 14,paddingBottom: 20},
              activeTintColor: colors.primaryButton,
              inactiveTintColor: colors.inactiveIcon,
              showLabel: false
            }}>
            <Tab.Screen name="Home" component={HomeScreen} />
            <Tab.Screen name="Charts" component={AnalyticsMainScreen} />
            {/* Custom plus button on the app to add activities */}
            <Tab.Screen options={{

              tabBarButton: props => <OpenDrawer clickable={context.clickable} {...props}/>,
            }}
            listeners={{
              tabPress: props=>{
                //console.log(context.opened)
                context.toggleOpening()
              }
            }}
            name="Drawer" component={BlankTab} />
            <Tab.Screen name="Assignments" component={SurveyStack} />
            <Tab.Screen options={{unmountOnBlur: true}} name="Settings" component={SettingsStack} />
          </Tab.Navigator>
          )
        }
      }
    </Consumer>

  );
};

const LoginStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Login">
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Reset" component={ResetPasswordScreen} />
      <Stack.Screen name="ConfirmReset" component={ConfirmResetScreen} />
    </Stack.Navigator>
  );
};

const SignupStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Welcome">
      <Stack.Screen name="Welcome" component={WelcomeScreen} />
      <Stack.Screen name="Registration" component={RegisterScreen} />
      <Stack.Screen name="Complete" component={ValidateRegistrationScreen} />
      <Stack.Screen name="Login" component={LoginStack} />
      <Stack.Screen name="Signup" component={SignupStack} />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Choose">
      <Stack.Screen name="Choose" component={AuthenticationScreen} />
      <Stack.Screen name="Login" component={LoginStack} />
      <Stack.Screen name="Signup" component={SignupStack} />
      <Stack.Screen name="ChangePassword" component={ChangePasswordScreen}/>
      <Stack.Screen name="ConfirmReset" component={ConfirmChangePasswordScreen}/>
    </Stack.Navigator>
  );
};

const AppStack = () => {

  useEffect(() => {

    // Assume a message-notification contains a "type" property in the data payload of the screen to open

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      //navigation.navigate(remoteMessage.data.type);
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          //setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
        }
        //setLoading(false);
      });
  }, []);

  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="HomeTab">
      <Stack.Screen name="AuthLoading" component={AuthStack} />
      <Stack.Screen name="HomeTab" component={HomeTab} />
      {/* Need it to reshow the tuto from the settings screen */}
      <Stack.Screen name="Onboarding" component={OnboardingScreen} />
    </Stack.Navigator>
  );
};

/*
* Testing if the user has seen the tutorial and if has a login token.
* We'll test the validity of the token on the componentDidMount of the Homescreen
*/


const MainNavigation = () => {

  const [tuto, setTuto] = React.useState(false)
  const [user, setUser] = React.useState(false)
  const [loaded, setLoaded] = React.useState(false)



  AsyncStorage.getItem('@tuto_seen').then((value) => {
    if (value !== null) {
      setTuto(true)
    }
  }).then(
    ()=>{
      AsyncStorage.getItem('@user').then((value) => {
        if (value !== null) {
          setUser(true)
        }
      }).then(
        ()=>setLoaded(true)
      )
    }
  )

  return (
    loaded &&
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {user ? null : tuto ? (
        <Stack.Screen name="Authentication" component={AuthStack} />
      ) : (
        <Stack.Screen name="Onboarding" component={OnboardingScreen} />
      )}
      <Stack.Screen name="HomeStack" component={AppStack} />
      <Stack.Screen name="NewsStack" component={NewsStack} />
      <Stack.Screen name="AuthLoading" component={AuthStack} />
    </Stack.Navigator>
  );
};

export default MainNavigation;
