import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  Linking
} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../global/constants';
import { HeaderMain } from './../../components/Global/Headers/HeaderMain';
import TasksPlot from './../../components/Dataviz/TasksPlot';
import CollaboratorsPlot from './../../components/Dataviz/CollaboratorsPlot';
import StreakPlot from './../../components/Dataviz/StreakPlot';
import GlobalPlot from './../../components/Dataviz/GlobalPlot';
import PrimaryButton from './../../components/Global/Buttons/PrimaryButton';


class AnalyticsMainScreen extends React.Component {

  render() {
    if (false) {
      return (
        <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
          <SafeAreaView style={styles.wrapperContent}>
            <View style={styles.content}>
              <Image
                source={require('./../../assets/images/onboarding3.png')}
                resizeMode={'contain'}
                style={styles.imageIllu}
              />
              <Text style={styles.title}>Coming soon</Text>
              <Text style={styles.description}>
                We will highlight your team interactions and progress throughout
                the several steps of the competition.{' '}
              </Text>


            </View>
            <Image
              source={require('./../../assets/images/charts.png')}
              resizeMode={'contain'}
              style={styles.bottomIllu}
            />
          </SafeAreaView>
        </View>
      );
    }
    else {
      return (
        <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
          <SafeAreaView style={styles.wrapperContent}>

            <ScrollView style={styles.scroll} contentContainerStyle={{justifyContent: 'space-between', alignItems: 'center', minHeight: screen.h*.75}}>
              <View style={styles.content}>
                <Text style={styles.blockTitle}>
                  Your Insights
                </Text>
                <Text style={styles.description}>
                  Unlock your team analytics by continuously filling the tasks,
                  surveys, and activating the bluetooth tracing.{' '}
                </Text>
                <PrimaryButton toDo={() => Linking.openURL('https://coso-frontend.herokuapp.com/insights')} label={"Access your team analytics!"} />
                <StreakPlot />
                <TasksPlot />
                <CollaboratorsPlot />
                <Text style={styles.blockTitle}>
                  Global statistics
                </Text>
                <GlobalPlot />
              </View>
            </ScrollView>
          </SafeAreaView>
        </View>
      );
    }
  }
}

export default AnalyticsMainScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  content: {
    width: screen.w,
    marginTop: screen.h / 15,
  },
  imageIllu: {
    height: 260,
    width: screen.w - 80,
    marginBottom: 40,
    alignSelf: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  blockTitle: {
    fontSize: 28,
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: 20,
    marginTop: 10,
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  description: {
    fontFamily: 'Montserrat-Regular',
    paddingHorizontal: 30,
    marginTop: 20,
    marginBottom: 20,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17,
    color: colors.lightGrey,
  },
  bottomIllu: {
    position: 'absolute',
    bottom: -28,
    height: 180,
    left: 40,
    width: screen.w - 40,
  },
});
