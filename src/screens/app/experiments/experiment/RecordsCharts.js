import React, {Fragment} from "react";
import { Dimensions, Text, View, ScrollView } from "react-native";
import wording from "../../../../data/wording";
import { BarChart, Grid, XAxis, YAxis, LineChart } from 'react-native-svg-charts'
import _ from "lodash";
import moment from "moment";
import { Circle } from 'react-native-svg'
import * as scale from 'd3-scale'

const formatRecordsForBarChart = (records) => {
  const interactions = records.reduce((map, obj) => {
    if(map[obj[0]]){
      map[obj[0]] += 1;
    }else{
      map[obj[0]] = 1;
    }
    return map;
  }, {});
  for(let i = interactions.length; i < 8; i++){
    interactions[_.repeat(" "), i] = 0;
  }
  interactions[" "] = 0;
  interactions["  "] = 0;
  interactions["   "] = 0;
  interactions["    "] = 0;
  interactions["     "] = 0;
  return Object.keys(interactions).map((item, idx) => {
    return {
      value: interactions[item],
      name: item
    }
  })
}

const renderBarChart = (records) => {
  const bar = formatRecordsForBarChart(records);
  return(
    <View style={{flexDirection: "row"}}>  
      <YAxis
          data={bar}
          contentInset={{top: 30, bottom: 30}}
          svg={{
            fill: 'grey',
            fontSize: 10,
          }}
          numberOfTicks={5}
          formatLabel={(value) => `${value}`}
          min={0}
          max={bar.reduce((max, val) => val.value > max ? val.value : max, 0)}
        />
      <View style={{flex: 1}}>  
        <BarChart 
        style={{ height: 200 }} 
        data={bar} 
        svg={{ fill: 'rgba(141, 218, 171, 0.8)'}} 
        yMin={0}
        yAccessor={ ({ item }) => item.value }
        xAccessor={ ({ item }) => item.name }
        yMax={bar.reduce((max, val) => val.value > max ? val.value : max, 0)}
        contentInset={{ top: 30, bottom: 30 }}>
          <Grid/>
        </BarChart>
        <XAxis
          data={bar}
          scale={scale.scaleBand}
          formatLabel={(value, index) => bar[index].name}
          contentInset={{ top: 30, bottom: 30 }}
          svg={{ fontSize: 10, fill: 'black', fontWeight: "bold" }}
        />
      </View>
    </View>
  );
}

const formatRecordsForLineChart = (records) => {
  let data = {};
  
  if(records.length > 0) {
    let i, startTime, endTime;
    startTime = 0;
    endTime = 0;
    for(i=0; i<records.length; i++) {
      if(startTime == 0 || records[i][1] < startTime) {
        startTime = records[i][1];
      }
      if(records[i][2] > endTime) {
        endTime = records[i][2];
      }
    }    

    //const startTime = records[0][1];
    //const endTime = records[records.length - 1][2];

    const duration = endTime - startTime;
    const PERIOD_NUMBER = Math.floor(duration / 3600);

    const periods = _.fill(new Array(PERIOD_NUMBER), 0).map((val, idx) => {
      return startTime + (duration / PERIOD_NUMBER * idx);
    });

    data = periods.map((val, idx) => {
      return idx < periods.length - 1 ? 
        records.filter((record) => record[1] >= val && record[1] < periods[idx + 1]).length :
        records.filter((record) => record[1] >= periods[idx]).length;
    });
  }
  
  return Object.keys(data).map((item, idx) => {
    return {
      value: Number(data[idx]),
      date: new Date(periods[idx]*1000)
    }
  });
}

const renderLineChart = (records) => {
  const line = formatRecordsForLineChart(records);
  
  const Decorator = ({ x, y, data }) => {
    return line.map((value, index) => (
        <Circle
            key={ value.date }
            cx={ x(value.date) }
            cy={ y(value.value) }
            r={ 3 }
            stroke={ 'rgb(141, 218, 171)' }
            fill={ 'white' }
        />
    ))
  }
  return(
    <View style={{margin: 20, flexDirection: "row"}}>  
      <YAxis
        style={{marginBottom: 20}}
        data={line}
        contentInset={{top: 10, bottom: 10}}
        svg={{
          fill: 'grey',
          fontSize: 10,
        }}
        numberOfTicks={5}
        formatLabel={(value) => `${value}`}
        min={0}
        max={line.reduce((max, val) => val.value > max ? val.value : max, 0)}
      />
      <View style={{flex: 1}}>  
        <LineChart
        style={{ height: 200 }} 
        data={line} 
        svg={{ 
          stroke: 'rgba(141, 218, 171, 0.8)',
          strokeWidth: 2
         }} 
        yAccessor={ ({ item }) => item.value }
        xAccessor={ ({ item }) => item.date }
        yMin={0}
        contentInset={{ top: 10, bottom: 10 }}
        xScale={ scale.scaleTime }>
          <Grid />
          <Decorator />
        </LineChart>
        <XAxis
        style={{height: 20}}
        data={line}
        xAccessor={ ({ item }) => item.date }
        scale={ scale.scaleTime }
        numberOfTicks={ 12 }
        formatLabel={(value, index) => {
          return moment(value).format("HH[h]mm");
        }}
        style={{ marginHorizontal: -15, height: 20 }}
        contentInset={{ left: 10, right: 25 }}
        svg={{
          fill: 'black',
          fontSize: 8,
          fontWeight: 'bold',
          rotation: 20,
          originY: 30,
          y: 5,
        }}
        />
      </View>
    </View>
  );
} 

const RecordsCharts = (props) => {

  const interactions = props.records.reduce((map, obj) => {
    if(map[obj[0]]){
      map[obj[0]] += 1;
    }else{
      map[obj[0]] = 1;
    }
    return map;
  }, {});
  
  return(
    <View style={{alignItems: "center"}}>
      <Text style={{marginHorizontal: 20, marginVertical: 10}}>
        <Text>{"We have recorded "}</Text>
        <Text style={{fontWeight: "bold"}}>{props.records.length}</Text>
        <Text>{" interactions with "}</Text>
        <Text style={{fontWeight: "bold"}}>{Object.keys(interactions).length}</Text>
        <Text>{" people during the recording! Here is the breakdown :"}</Text>
      </Text>
      <Text>{wording.experiments.interactions_with_team_members}</Text>
      {renderBarChart(props.records)}
      
      
    </View>
  );

}

export default RecordsCharts;

// <Text style={{marginTop: 30}}>{wording.experiments.total_interactions}</Text>
      // {renderLineChart(props.records)}