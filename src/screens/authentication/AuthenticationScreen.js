import React, {useState} from 'react';
import {View, SafeAreaView, StyleSheet, Text, Image, AsyncStorage} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../global/constants';
import PrimaryButton from './../../components/Global/Buttons/PrimaryButton';
import SecondaryButton from './../../components/Global/Buttons/SecondaryButton';

export class AuthenticationScreen extends React.Component {
  componentDidMount = async()=>{
    await AsyncStorage.removeItem('@user').then(
      async ()=>{
        await AsyncStorage.removeItem('@myInfos');
        await AsyncStorage.removeItem('@logsList');
      }
    )
  }
  componentDidUpdate = async() =>{
    await AsyncStorage.removeItem('@myInfos');
    await AsyncStorage.removeItem('@logsList');
    await AsyncStorage.removeItem('@user');
  }
  login = () => {
    this.props.navigation.navigate('Login');
  };
  signup = () => {
    this.props.navigation.navigate('Signup');
  };
  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView
          style={[styles.wrapperContent, {justifyContent: 'flex-end'}]}>
          <View style={styles.welcome}>
            <Image
              source={require('./../../assets/images/appIcon.png')}
              resizeMode={'contain'}
              style={styles.appIcon}
            />
            <Text style={styles.title}>Welcome to CoSo</Text>
            <Text style={styles.subTitle}>
              Join the iGEM Network Experience
            </Text>
          </View>
          <View style={styles.buttons}>
            <PrimaryButton toDo={this.login} label={'Log in'} />
            <SecondaryButton toDo={this.signup} label={'Sign up'} />
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  welcome: {
    marginBottom: screen.h / 4.5,
  },
  appIcon: {
    height: 120,
    width: 120,
    marginBottom: 50,
    alignSelf: 'center',
  },
  title: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  subTitle: {
    fontFamily: 'Montserrat-Regular',
    paddingHorizontal: 50,
    marginTop: 25,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17,
    color: colors.lightGrey,
  },
  buttons: {
    marginBottom: 25,
  },
});
