import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Animated,
  KeyboardAvoidingView,
  TouchableOpacity,
  Platform,
  Alert
} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../../global/constants';
import PrimaryButton from './../../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../../components/Global/TextInput/CustomInput';
import {changePassword} from './../../../services/api/signup/functions'


class ChangePasswordScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mdp: '',
      confirmMdp: '',
      width: new Animated.Value(120),
      height: new Animated.Value(120),
      top: new Animated.Value(),
    };
  }
  updateState = (stateLabel, value) => {
    this.setState({
      [stateLabel]: value,
    });
  };

  confirmation = () => {
    if (this.state.mdp === this.state.confirmMdp && this.state.mdp!== '') {
      changePassword(this.props.route.params.reset_password_token, this.state.mdp, this.state.confirmMdp).then(
        (res)=>{
          console.log(res.status)
          if (res.status === 204) {
            this.props.navigation.navigate('ConfirmReset')
          }else{
            Alert.alert("Password", "An error occured, your password must be at least 8 characters. Please retry")
          }
          //
        }
      )
      .catch(
        ()=>{
          Alert.alert('Change password', 'An error occured, please retry')
        }
      )
    } else {
      Alert.alert('Change password', 'The two password fields didn\'t match');
    }
  };

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardWillHide,
      );
    }else{
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardWillShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardWillHide',
        this._keyboardWillHide,
      );
    }
  }

  _keyboardWillShow = () => {
    Animated.parallel([
      Animated.timing(this.state.width, {
        toValue: 60,
        duration: 250,
      }),
      Animated.timing(this.state.height, {
        toValue: 60,
        duration: 250,
      }),
    ]).start();
  };

  _keyboardWillHide = () => {
    Animated.parallel([
      Animated.timing(this.state.width, {
        toValue: 120,
        duration: 250,
      }),
      Animated.timing(this.state.height, {
        toValue: 120,
        duration: 250,
      }),
    ]).start();
  };

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillHideListener.remove();
  }

  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView
            style={[styles.wrapperContent, Platform.OS === 'ios' ? {justifyContent: 'flex-end'}: {paddingTop: screen.h/8}]}>
            <KeyboardAvoidingView
              behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
              <View style={styles.content}>
                <Animated.View
                  style={[
                    styles.appIconWrapper,
                    {
                      width: this.state.width,
                      height: this.state.height,
                    },
                  ]}>
                  <Image
                    source={require('./../../../assets/images/appIcon.png')}
                    resizeMode={'contain'}
                    style={styles.appIcon}
                  />
                </Animated.View>

                <Text style={styles.title}>Reset your password</Text>

                <CustomInput
                  placeholder={'New password'}
                  stateName={'mdp'}
                  update={this.updateState}
                  label={'New password'}
                  password={true}
                />
                <CustomInput
                  placeholder={'Confirm password'}
                  stateName={'confirmMdp'}
                  update={this.updateState}
                  label={'Confirm password'}
                  password={true}
                  customStyle={styles.lastInput}
                />
                <PrimaryButton toDo={this.confirmation} label={'SUBMIT'} />
              </View>
            </KeyboardAvoidingView>

            <View style={styles.buttons}></View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

export default ChangePasswordScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  content: {},
  appIconWrapper: {
    marginBottom: screen.h / 20,
    alignSelf: 'center',
  },
  appIcon: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
    marginBottom: screen.h / 15,
  },
  lastInput: {
    marginBottom: screen.h / 15,
  },
  buttons: {
    marginBottom: 25,
    marginTop: screen.h / 7.5,
  },
});
