import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Animated,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import colors, {
  defaultScreenStyles,
  screen,
  backButtonStyles,
} from './../../../global/constants';
import {isIphoneXorAbove} from './../../../global/functions';
import PrimaryButton from './../../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../../components/Global/TextInput/CustomInput';
import {validateEmail} from '../../../global/functions';
import { CommonActions } from '@react-navigation/native';

class ConfirmChangePasswordScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      top: new Animated.Value(),
      error: false,
      tested: false,
    };
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  goLogin = async() => {
    await this.props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Login' }
        ],
      })
    );
  };

  render() {
    let {error, width, height, tested} = this.state;
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView
            style={[styles.wrapperContent, {justifyContent: 'space-between'}]}>
            <TouchableOpacity onPress={this.goBack} style={styles.backPress}>
              <Image
                source={require('./../../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
            <KeyboardAvoidingView
              behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
              <View style={styles.content}>
                <Image
                  source={require('./../../../assets/images/appIcon.png')}
                  resizeMode={'contain'}
                  style={styles.appIcon}
                />

                <Text style={styles.title}>Reset your password</Text>
                <Text style={styles.subtitle}>
                  Your password has been successfully changed.
                </Text>
              </View>
            </KeyboardAvoidingView>
            <Image
              source={require('./../../../assets/images/pets.png')}
              resizeMode={'contain'}
              style={styles.illu}
            />
            <View style={styles.buttons}>
              <PrimaryButton toDo={this.goLogin} label={'BACK TO LOGIN'} />
            </View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

export default ConfirmChangePasswordScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  ...backButtonStyles,
  content: {
    marginTop: 15,
  },
  avoiding: {
    width: screen.w,
    borderWidth: 1,
  },
  appIcon: {
    height: 60,
    width: 60,
    alignSelf: 'center',
    marginBottom: screen.h / 25,
  },
  title: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
    marginBottom: screen.h / 25,
  },
  subtitle: {
    fontFamily: 'Montserrat-Medium',
    width: screen.w - 100,
    textAlign: 'center',
    lineHeight: 20,
    fontSize: 15,
    color: colors.grey,
    marginBottom: screen.h / 20 + 10,
  },
  inputStyle: {
    marginBottom: 0,
  },
  error: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.red,
    marginTop: 5,
    marginLeft: 16,
    marginBottom: 40,
  },
  account: {
    fontFamily: 'Montserrat-Regular',
    color: colors.lightGrey,
  },
  forgot: {
    alignSelf: 'center',
    marginBottom: 30,
  },
  clickable: {
    fontFamily: 'Montserrat-Regular',
    color: colors.primaryButton,
  },
  illu: {
    width: 200,
    height: 160,
  },
  buttons: {
    marginBottom: 25,
    marginTop: screen.h / 7.5,
  },
});
