import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Animated,
  KeyboardAvoidingView,
  TouchableOpacity,
  Alert,
  AsyncStorage,
} from 'react-native';
import { CommonActions } from '@react-navigation/native';
import colors, {defaultScreenStyles, screen, backButtonStyles} from './../../../global/constants';
import PrimaryButton from './../../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../../components/Global/TextInput/CustomInput';
import { loginUser } from '../../../services/api/signup/functions';


class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      mdp: '',
      width: new Animated.Value(120),
      height: new Animated.Value(120),
      top: new Animated.Value(0),
      loading: false
    };

  }
  updateState = (stateLabel, value) => {
    this.setState({
      [stateLabel]: value,
    });
  };

  login = async () => {
    this.setState({
      loading: true
    })
    if(this.state.login !== '' && this.state.mdp !==''){
      let result = await loginUser(this.state.login, this.state.mdp)
      //console.log('RESULT', result)

      if (result !== null && result.status == 200) {
        let token = result.headers.authorization
          console.log('token:',token)

          await AsyncStorage.setItem(
            '@user',
            token
          ).then(async()=>{
            await AsyncStorage.setItem(
              '@myInfos',
              JSON.stringify(result.data)
            ).then(async()=>{
              setTimeout(
                async ()=>{
                  this.setState({
                    loading: false
                  })

                  await this.props.navigation.dispatch(
                    CommonActions.reset({
                      index: 1,
                      routes: [
                        { name: 'HomeStack' }
                      ],
                    })
                  );
                }, 2000
              )

            })

          })
          .catch((error) =>{
            console.error(error)
            // Error saving data
            this.setState({
              loading: false,
              error: true,
              textError: result.data.error
            })
          })

      } else{
        //alert('jis')
        this.setState({
          loading: false,
          error: true,
          textError: result.data.error
        })
      }
    }else{
      this.setState({
        loading: false
      })
      Alert.alert("Identifiants", "Field(s) empty")
    }
  };


  componentDidMount() {
    if (Platform.OS === 'android') {
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardWillHide,
      );
    }else{
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardWillShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardWillHide',
        this._keyboardWillHide,
      );
    }
  }

  _keyboardWillShow = () => {
    Animated.parallel([
      Animated.timing(this.state.width, {
        toValue: 70,
        duration: 250,
      }),
      Animated.timing(this.state.height, {
        toValue: 70,
        duration: 250,
      }),
    ]).start();
  };

  _keyboardWillHide = () => {
    Animated.parallel([
      Animated.timing(this.state.width, {
        toValue: 120,
        duration: 250,
      }),
      Animated.timing(this.state.height, {
        toValue: 120,
        duration: 250,
      }),
    ]).start();
  };

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillHideListener.remove();
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  goRegister = () => {
    this.props.navigation.navigate('Signup');
  };

  resetPassword = () => {
    this.props.navigation.navigate('Reset');
  };

  render() {
    let {error, textError, width, height, loading} = this.state
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView style={[styles.wrapperContent, {justifyContent: 'space-between'}]}>
            <TouchableOpacity onPress={this.goBack} style={styles.backPress}>
              <Image
                source={require('./../../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
            <KeyboardAvoidingView
              behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
              <View style={styles.content}>
                <Animated.View
                  style={[
                    styles.appIconWrapper,
                    {
                      width: width,
                      height: height,
                    },
                  ]}>
                  <Image
                    source={require('./../../../assets/images/appIcon.png')}
                    resizeMode={'contain'}
                    style={styles.appIcon}
                  />
                </Animated.View>

                <Text style={styles.title}>Login</Text>

                <CustomInput
                  placeholder={'Email'}
                  stateName={'login'}
                  update={this.updateState}
                  label={'Email'}
                />
                <CustomInput
                  placeholder={'Password'}
                  stateName={'mdp'}
                  update={this.updateState}
                  customStyle={{marginBottom: 0}}
                  label={'Password'}
                  password={true}
                />
                <Text style={styles.error}>
                  {error === true ? textError : null}
                </Text>
                <TouchableOpacity
                  style={styles.forgot}
                  onPress={this.resetPassword}>
                  <Text style={styles.clickable}>Forgot password?</Text>
                </TouchableOpacity>
                  <TouchableOpacity
                  style={styles.forgot}
                  onPress={this.resetPassword}>
                  <Text style={styles.clickable}></Text>
                </TouchableOpacity>
                <PrimaryButton loading={loading} toDo={this.login} label={'LOG IN'} />
              </View>
            </KeyboardAvoidingView>

            <View style={styles.buttons}>
              <Text style={styles.account}>
                Don't have an account?{' '}
                <Text onPress={this.goRegister} style={styles.clickable}>
                  Sign up
                </Text>
              </Text>
            </View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

export default LoginScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  ...backButtonStyles,
  content: {
    alignItems: 'center'
  },
  avoiding: {
    width: screen.w,
    borderWidth: 1,
  },
  appIconWrapper: {
    marginBottom: screen.h / 20,
    alignSelf: 'center',
    marginTop: 20
  },
  appIcon: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
    marginBottom: screen.h / 20,
  },
  error: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.red,
    marginTop: 5,
    marginLeft: 16,
    marginBottom: 40,
    width: screen.w-100
  },
  account: {
    fontFamily: 'Montserrat-Regular',
    color: colors.lightGrey,
  },
  forgot: {
    alignSelf: 'center',
    marginBottom: 30,
  },
  clickable: {
    fontFamily: 'Montserrat-Regular',
    color: colors.primaryButton,
  },
  buttons: {
    marginBottom: 25,

  },
});
