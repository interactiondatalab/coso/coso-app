import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Animated,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import colors, {
  defaultScreenStyles,
  screen,
  backButtonStyles,
} from '../../../global/constants';
import {isIphoneXorAbove} from './../../../global/functions';
import PrimaryButton from '../../../components/Global/Buttons/PrimaryButton';
import CustomInput from '../../../components/Global/TextInput/CustomInput';
import {validateEmail} from '../../../global/functions';
import {recoverPassword} from './../../../services/api/signup/functions'

class ResetPasswordScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      width: new Animated.Value(120),
      height: new Animated.Value(120),
      top: new Animated.Value(),
      error: false,
      tested: false,
    };
  }
  updateState = (stateLabel, value) => {
    this.setState({
      [stateLabel]: value,
    });
  };

  reset = () => {
    //this.props.navigation.navigate('HomeStack');

    if (validateEmail(this.state.email)) {
      this.setState({
        tested: true,
      });
      // FIND EMAIL AND IF HAS EMAIL SEND EMAIL
        recoverPassword(this.state.email).then(
          ()=>{
            Keyboard.dismiss();
            this.props.navigation.navigate('ConfirmReset');
          }
        ).catch(
          (e)=>{
            this.setState({
              error: true,
              tested: false,
            });
          }
        )
    } else {
      this.setState({
        error: true,
      });
    }
  };

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardWillHide,
      );
    }else{
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardWillShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardWillHide',
        this._keyboardWillHide,
      );
    }
  }

  _keyboardWillShow = () => {
    Animated.parallel([
      Animated.timing(this.state.width, {
        toValue: 60,
        duration: 250,
      }),
      Animated.timing(this.state.height, {
        toValue: 60,
        duration: 250,
      }),
    ]).start();
  };

  _keyboardWillHide = () => {
    Animated.parallel([
      Animated.timing(this.state.width, {
        toValue: 120,
        duration: 250,
      }),
      Animated.timing(this.state.height, {
        toValue: 120,
        duration: 250,
      }),
    ]).start();
  };

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillHideListener.remove();
  }

  testEmailFormat = () => {
    this.setState({
      error: !validateEmail(this.state.email),
    });
  };

  resetError = () => {
    this.setState({
      error: false,
      tested: false,
    });
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    let {error, width, height, tested} = this.state;
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView
            style={[styles.wrapperContent, Platform.OS === 'ios' ? {justifyContent: 'flex-end'}: {paddingTop: screen.h/8}]}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={styles.backPress}>
              <Image
                source={require('./../../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
            <KeyboardAvoidingView
              behavior={Platform.OS == 'ios' ? 'padding' : 'height'}>
              <Animated.View
                style={[
                  styles.appIconWrapper,
                  {
                    width: width,
                    height: height,
                  },
                ]}>
                <Image
                  source={require('./../../../assets/images/appIcon.png')}
                  resizeMode={'contain'}
                  style={styles.appIcon}
                />
              </Animated.View>
              <View style={styles.content}>
                <Text style={styles.title}>Reset your password</Text>
                <Text style={styles.subtitle}>
                  Submit your email address and we’ll send you a link to reset
                  your password.
                </Text>

                <CustomInput
                  error={error}
                  placeholder={'Email'}
                  label={'Email'}
                  stateName={'email'}
                  update={this.updateState}
                  customStyle={styles.inputStyle}
                  blurFunction={this.testEmailFormat}
                  focusFunction={this.resetError}
                  onKeyFunction={this.resetError}
                />
                <Text style={styles.error}>
                  {error === true
                    ? tested === true
                      ? 'Sorry we could’t find this email.'
                      : 'Please enter a valid email address.'
                    : null}
                </Text>
                <PrimaryButton
                  inactive={tested}
                  toDo={this.reset}
                  label={'SUBMIT'}
                />
              </View>
            </KeyboardAvoidingView>
            <View style={styles.buttons} />
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

export default ResetPasswordScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  ...backButtonStyles,
  content: {width: screen.w - 100},
  appIconWrapper: {
    marginBottom: screen.h / 20,
    alignSelf: 'center',
  },
  appIcon: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
  },
  title: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
    marginBottom: screen.h / 20,
  },
  subtitle: {
    fontFamily: 'Montserrat-Medium',
    width: screen.w - 100,
    textAlign: 'center',
    lineHeight: 20,
    fontSize: 15,
    color: colors.grey,
    marginBottom: screen.h / 20 + 10,
  },
  inputStyle: {
    marginBottom: 0,
  },
  error: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    color: colors.red,
    marginTop: 5,
    marginLeft: 16,
    marginBottom: 40,
  },
  account: {
    fontFamily: 'Montserrat-Regular',
    color: colors.lightGrey,
  },
  forgot: {
    alignSelf: 'center',
    marginBottom: 30,
  },
  clickable: {
    fontFamily: 'Montserrat-Regular',
    color: colors.primaryButton,
  },
  buttons: {
    marginBottom: 25,
    marginTop: screen.h / 7.5,
  },
});
