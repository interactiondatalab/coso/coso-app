import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  Animated,
  Alert,
  AsyncStorage
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import colors, {
  defaultScreenStyles,
  screen,
  backButtonStyles,
} from './../../../global/constants';
import IgemIdStep from '../../../components/Registration/IgemIdStep';
import TeamNameStep from '../../../components/Registration/TeamNameStep';
import UsernameStep from '../../../components/Registration/UsernameStep';
import EmailStep from '../../../components/Registration/EmailStep';
import PassWordStep from '../../../components/Registration/PassWordStep';
import { signUpUSer, loginUser } from '../../../services/api/signup/functions';

class RegisterScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      step: 0,
      open: false,
      spaceBottom: new Animated.Value(screen.h / 10),
      userData: {},
      jump: false
    };
  }

  componentDidMount() {
    
    if (Platform.OS === 'android') {
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardDidShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardDidHide',
        this._keyboardWillHide,
      );
    }else{
      this.keyboardWillShowListener = Keyboard.addListener(
        'keyboardWillShow',
        this._keyboardWillShow,
      );
      this.keyboardWillHideListener = Keyboard.addListener(
        'keyboardWillHide',
        this._keyboardWillHide,
      );
    }
  }

  _keyboardWillShow = () => {
    Animated.timing(this.state.spaceBottom, {
      toValue: 10,
      duration: 0 /* TO TEST */,
    }).start();
    this.setState({
      open: true,
    });
  };

  _keyboardWillHide = () => {
    Animated.timing(this.state.spaceBottom, {
      toValue: screen.h / 10,
      duration: 0,
    }).start();
    this.setState({
      open: false,
    });
  };

  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillHideListener.remove();
  }

  updateUserInfos = (key,value, end) =>{
      this.setState(prevState => ({
        userData: {                   // object that we want to update
            ...prevState.userData,    // keep all other key-value pairs
            [key]: value       // update the value of specific key
        }
      }),()=>{
        if (end) {
          this.completeRegistration()
        }
      })
  }

  updateState = (stateLabel, value) => {
    this.setState({
      [stateLabel]: value,
    });
  };

  manageStep = (value) => {
    if (this.state.step === 0 && value === -1) {
      this.props.navigation.goBack();
    } else {
      this.setState((prevState) => ({
        step: prevState.step + value,
      }));
      if (value === 2) {
        this.setState({
          jump:true
        })
      }else{
        this.setState({
          jump:false
        })
      }
    }
  };

  completeRegistration = async() => {
    let _this = this;
    let result = await signUpUSer(this.state.userData)
    console.log('RESULT', result.data)
    if (result.status == '201') {
      try {

        let res = await loginUser(this.state.userData.email, this.state.userData.password)
        if (res.status == 200) {
          let token = res.headers.authorization
          console.log('tokenl',token)
      
            await AsyncStorage.setItem(
              '@user',
              token
            ).then(async()=>{
              await AsyncStorage.setItem(
                '@myInfos',
                JSON.stringify(res.data)
              ).then(()=>{
                setTimeout(
                  ()=> _this.props.navigation.navigate('Complete', {username: this.state.userData.username})
                )
              })
              
            })
            .catch((error) =>{
              console.error(error)
              // Error saving data
              this.setState({
                loading: false
              })
            })
        }
      } catch (error) {
        // Error saving data
      }
    } else{
      if (result.status == '400') {
        Alert.alert("Registration", result.data.error)
      } else{
        Alert.alert(
          "Registration",
          `An error occured, ${result.data.error}`,
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "Open mail", onPress: () => Linking.openURL(`mailto:igem-ties@cri-paris.org`)}
          ],
          { cancelable: false }
        );
      }
    }
  };

  displayForms = () => {
    switch (this.state.step) {
      case 0:
        return (
          <EmailStep
            keyboardOpen={this.state.open}
            marginContent={this.state.spaceBottom}
            manageStep={this.manageStep}
            register={this.updateUserInfos}
          />
        );
        break;
      case 1:
        return (
          <IgemIdStep
            keyboardOpen={this.state.open}
            marginContent={this.state.spaceBottom}
            manageStep={this.manageStep}
            register={this.updateUserInfos}
          />
        );
        break;
      case 2:
          return (
            <UsernameStep
              keyboardOpen={this.state.open}
              marginContent={this.state.spaceBottom}
              manageStep={this.manageStep}
              register={this.updateUserInfos}
            />
          );
          break;
      case 3:
        return (
          <TeamNameStep
            keyboardOpen={this.state.open}
            marginContent={this.state.spaceBottom}
            manageStep={this.manageStep}
            register={this.updateUserInfos}
          />
        );
        break;
     
      case 4:
        return (
          <PassWordStep
            keyboardOpen={this.state.open}
            marginContent={this.state.spaceBottom}
            complete={this.completeRegistration}
            register={this.updateUserInfos}
          />
        );
        break;

      default:
        break;
    }
  };

  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <SafeAreaView
            style={[styles.wrapperContent, {justifyContent: 'flex-end'}]}>
            <TouchableOpacity
              onPress={() => {
                this.state.jump ? 
                this.manageStep(-2)
                :this.manageStep(-1)
                
              }}
              style={styles.backPress}>
              <Image
                source={require('./../../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
            <KeyboardAvoidingView
              behavior={Platform.OS == 'ios' ? 'padding' : 'padding'}>
              {this.displayForms()}
            </KeyboardAvoidingView>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

export default RegisterScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  ...backButtonStyles,
});
