import React, {useState} from 'react';
import {View, SafeAreaView, StyleSheet, Text, Image} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../../global/constants';
import PrimaryButton from './../../../components/Global/Buttons/PrimaryButton';
import {CommonActions} from '@react-navigation/native';
import {isIphoneXorAbove} from '../../../global/functions';

class ValidateRegistrationScreen extends React.Component {

  gotoMain = async() => {
    await this.props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'HomeStack' }
        ],
      })
    );
  };

  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView
          style={[styles.wrapperContent, {justifyContent: 'flex-end'}]}>
          <Image
            source={require('./../../../assets/images/fullIllu.png')}
            resizeMode={'cover'}
            style={styles.imageIllu}
          />
          <View style={styles.content}>
            <Text style={styles.title}>Hello {this.props.route.params.username}{'\n'}Welcome aboard!</Text>
          </View>
          <View style={styles.buttons}>
            <PrimaryButton toDo={this.gotoMain} label={"Let's start"} />
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

export default ValidateRegistrationScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  content: {
    //marginBottom: screen.h / 6,
  },
  title: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
    marginBottom: 35,
  },
  imageIllu: {
    position: 'absolute',
    bottom: 0,
    height: screen.h*.7,
    top: isIphoneXorAbove() ? 50 : 0,
    width: screen.w,
    marginBottom: 0,
  },
  buttons: {
    marginBottom: 25,
  },
});
