import React, {useState} from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import colors, {
  defaultScreenStyles,
  screen,
  backButtonStyles,
} from './../../../global/constants';
import PrimaryButton from './../../../components/Global/Buttons/PrimaryButton';
import CustomInput from './../../../components/Global/TextInput/CustomInput';
import {isIphoneXorAbove} from '../../../global/functions';

class WelcomeScreen extends React.Component {
  constructor() {
    super();
    this.state = {
      login: '',
      mdp: '',
    };
  }
  goToRegistration = () => {
    this.props.navigation.navigate('Registration');
  };
  goBack = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        
        <SafeAreaView
          style={[styles.wrapperContent, {justifyContent: 'flex-end'}]}>
          <TouchableOpacity onPress={this.goBack} style={styles.backPress}>
            <Image
              source={require('./../../../assets/icons/arrowBack.png')}
              resizeMode={'contain'}
              style={styles.backButton}
            />
          </TouchableOpacity>
          <Image
            source={require('./../../../assets/images/fullIllu.png')}
            resizeMode={'cover'}
            style={styles.imageIllu}
          />
          <View style={styles.content}>
            <Text style={styles.title}>Welcome to CoSo</Text>
            <Text style={styles.subTitle}>
              To register on CoSo you need your iGEM username and Team name.
            </Text>
          </View>
          <View style={styles.buttons}>
            <PrimaryButton toDo={this.goToRegistration} label={"Let's start"} />
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

export default WelcomeScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  ...backButtonStyles,
  content: {
    marginBottom: 30,
  },
  appIcon: {
    height: 120,
    width: 120,
    marginBottom: 40,
    alignSelf: 'center',
    borderWidth: 1,
    backgroundColor: colors.lightGrey,
  },
  imageIllu: {
    position: 'absolute',
    bottom: 0,
    height: screen.h*.65,
    top: isIphoneXorAbove() ? 50 : 0,
    width: screen.w,
    marginBottom: 0,
  },
  title: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
    marginBottom: 25,
  },
  subTitle: {
    fontFamily: 'Montserrat-Regular',
    paddingHorizontal: 40,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17,
    color: colors.lightGrey,
  },
  buttons: {
    marginBottom: 25,
  },
});
