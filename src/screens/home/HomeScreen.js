import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  Alert,
  AsyncStorage,
  ActivityIndicator,
  TouchableWithoutFeedback,
  AppState
} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../global/constants';
import {HeaderMain} from './../../components/Global/Headers/HeaderMain';
import { isIphoneXorAbove } from '../../global/functions';

import messaging, { AuthorizationStatus } from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app'
import CalendarStrip from 'react-native-slideable-calendar-strip';

import Consumer from '../../components/Global/Contexts/ConfigContext';
import TaskToAdd from '../../components/TaskRecords/TaskToAdd';
import { getLogs, getAllTasks } from '../../services/api/tasks/functions';
import { getAllNews } from '../../services/api/news/functions';
import { CommonActions } from '@react-navigation/native';
import ModifyModal from '../tasks/ModifyModal';
import { getUniqueId, getModel, getBrand } from 'react-native-device-info';
import { getUserPhones, postPhone, modifyTokenPhone } from '../../services/api/phones/functions';

import GestureRecognizer from 'react-native-swipe-gestures';
import TasksModal from '../tasks/TasksModal';
import moment from 'moment';

const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

function addDays(selectedDate, days) {
  const year = selectedDate.getFullYear();
  const month = selectedDate.getMonth();
  const day = selectedDate.getDate();

  return new Date(year, month, day + days);
}

/*

Type of language config for the calendar if needed

-----

LocaleConfig.locales['fr'] = {
    monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
    monthNamesShort: ['Janv.','Févr.','Mars','Avril','Mai','Juin','Juil.','Août','Sept.','Oct.','Nov.','Déc.'],
    dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
    dayNamesShort: ['Dim.','Lun.','Mar.','Mer.','Jeu.','Ven.','Sam.'],
    today: 'Aujourd\'hui'
};
LocaleConfig.defaultLocale = 'fr';

*/


class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  createDate = () => {
    var date = new Date();
    var offset = date.getTimezoneOffset();
    return new Date(date.getTime() - offset);
  }

  state={
      currentDate: this.createDate(),
      currentFormattedDate : this.createDate(),
      selectedDate: this.createDate(),
      sameDay:true,
      activities:[],
      modalVisible: false,
      tested: false,
      openQuestion: false,
      idClient: "Undefined",
      isStarted: false,
      dayData:[],
      user: null,
      displayed:[],
      logsList:[],
      todayList:[],
      allTasks: [],
      loading: true,
      loadingData : false,
      logged: false,
      isBLEActivated: false,
      unreadCount: 0,
      news: []
  }

  createPhone = async() => {
    let os, imei, brand, model, foundPhone;
    os = Platform.OS;
    imei = getUniqueId();
    brand = getBrand();
    model = getModel();
    foundPhone = false;

    let result_phones = await getUserPhones();
    if(result_phones.status == 200) {
      for(let i = 0; i < result_phones.data.length; i++) {
        if (result_phones.data[i].imei === imei) {
          if (result_phones.data[i].os === os) {
            if (result_phones.data[i].brand === brand) {
              if (result_phones.data[i].model === model) {
                console.log('YOU1', result_phones.data[i].id)
                await AsyncStorage.setItem('@phone_id', result_phones.data[i].id.toString());
                foundPhone = true;
                break;
              }
            }
          }
        }
      }

      if(!foundPhone) {
        let result_postPhone = await postPhone(imei, os, brand, model, "");
        if(result_postPhone.status == 201) {
          console.log('YOU', result_postPhone.data.id)
          await AsyncStorage.setItem('@phone_id', result_postPhone.data.id.toString());
        }
      }
    }
  }

  saveTokenNotification = async(token) =>{
    let phoneId = await AsyncStorage.getItem('@phone_id');
    await modifyTokenPhone(phoneId, token);
  }

  requestUserPermission = async() => {
    const authStatus = await messaging().requestPermission();
    const enabled = authStatus === 1 || authStatus === 2;

    if (enabled) {
      console.log('Authorization status:', authStatus);
      console.log(messaging().isDeviceRegisteredForRemoteMessages)
      messaging().getToken().then(
        token => this.saveTokenNotification(token)
      )
    }else{
      console.log('not enabled')
    }

  }

  componentWillUnmount = async() => {
    this._refreshNews();
  }

  componentDidMount = async()=>{

    if(!this.state.logged) {
      await this.createPhone();
      await this.requestUserPermission();
    }
    /*
    * We getting all the activities at the start in order to not have a
    * loading when you want to declare your daily activities
    */


    await getAllTasks().then(
      async res=>{
        if (res.status===200) {
          this.setState({
            logged: true
          })
        } else{
          await this.props.navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                { name: 'AuthLoading' }
              ],
            })
          );
        }
      }
    )

    await getAllNews()
    .then( async res => {
      var count = 0;
      res.data.forEach((item, i) => {
        if (!item.unread) {
          count += 1;
        }
      });
      this.setState({unreadCount: count, news: res.data})
    })

    this._refreshNews = this.props.navigation.addListener('focus', () => {
      getAllNews()
      .then( async res => {
        var count = 0;
        res.data.forEach((item, i) => {
          if (!item.unread) {
            count += 1;
          }
        });
        this.setState({unreadCount: count, news: res.data.reverse()})
      })
    });

    /*
    * The calendar is managing dates without time values, they are all set to 0. So we have to initialize it
    */

    var currentFormattedDate = this.createDate();
    currentFormattedDate.setDate(this.state.currentDate.getDate());
    currentFormattedDate.setMonth(this.state.currentDate.getMonth());
    currentFormattedDate.setFullYear(this.state.currentDate.getFullYear());
    currentFormattedDate.setHours(0)
    currentFormattedDate.setMinutes(0)
    currentFormattedDate.setSeconds(0)
    currentFormattedDate.setMilliseconds(0)

    this.setState({
      currentFormattedDate : currentFormattedDate,
    }, ()=>{
      this.setState({
        selectedDate: this.state.currentFormattedDate
      }, async()=>{
        this.getDataFromBdd()
      })
    })

    // Needed to send notifications on iOS
    await messaging().registerDeviceForRemoteMessages();

     //  messaging().onNotificationOpenedApp(remoteMessage => {
     //   console.log(
     //     'Notification caused app to open from background state:',
     //     remoteMessage.notification,
     //   );
     //   //navigation.navigate(remoteMessage.data.type);
     // });

     // Check whether an initial notification is available
     // messaging()
     //   .getInitialNotification()
     //   .then(remoteMessage => {
     //     if (remoteMessage) {
     //       console.log(
     //         'Notification caused app to open from quit state:',
     //         remoteMessage.notification,
     //       );
     //       //setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
     //     }
     //     //setLoading(false);
     //   });

  }

  // Detection of swiping day

  onSwipeLeft = async(context) => {
    await this.updateDate(addDays(this.state.selectedDate ,1), context)
  }

  onSwipeRight = async(context) => {
    await this.updateDate(addDays(this.state.selectedDate, -1), context)
  }

  getDataFromBdd = async() =>{
    await this.setState({
      loading: true
    })

    // console.log("GET DATA")
    // console.log(this.state.selectedDate);
    // console.log("Date: " + this.state.selectedDate.toLocaleDateString());
    // console.log("Date moment: " + moment(this.state.selectedDate).format("YYYY-MM-DD 00:00:00"))

    let result = await getLogs(moment(this.state.selectedDate).format("YYYY-MM-DD 00:00:00")).then(
      res=>{
        //console.log(res.data);
        if (res.status === 200) {
          this.setState({
            logsList: res.data
          },()=>{
            setTimeout(
              ()=> this.compareData(), 500
            )
          })
        }
      }
    )
  }

  // Check if we need to get new data from the database

  compareData = async () =>{
    AsyncStorage.getItem('@logsList').then(async (value) => {
      if (value !== null) {
        if (value===JSON.stringify(this.state.logsList)) {
          this.dispatchLogs()

        } else{
          await AsyncStorage.removeItem('@logsList');
          await AsyncStorage.setItem('@logsList', JSON.stringify(this.state.logsList));
          this.dispatchLogs()
        }
      } else {
        await AsyncStorage.removeItem('@logsList');
        await AsyncStorage.setItem('@logsList', JSON.stringify(this.state.logsList));
        this.dispatchLogs()
      }
    });
  }

  /*
  * If you need to manage cache here you have to modify how the data are treated
  */

  dispatchLogs = async() =>{
    let buffer = []
    this.state.logsList.map(async(element, i)=>{
      let taskDate = new Date(element.task_done_at)

      taskDate.setHours(0)
      taskDate.setMinutes(0)
      taskDate.setSeconds(0)
      taskDate.setMilliseconds(0)

      if (buffer.includes(taskDate.toLocaleDateString())) {
        await AsyncStorage.getItem(taskDate.toLocaleDateString()).then(async(value) => {
          let newValue = JSON.parse(value)
          newValue.push(element)
          if (value !== null) {
            await AsyncStorage.setItem(taskDate.toLocaleDateString(), JSON.stringify(newValue));
          }

        }).catch(
          e=>console.error(e)
        )
      } else{
        buffer.push(taskDate.toLocaleDateString())
        let newValue = [element]
        await AsyncStorage.setItem(taskDate.toLocaleDateString(), JSON.stringify(newValue));
      }
    })

    setTimeout(async()=>{
      this.displayDataFunction()

    },1000)
  }

  displayDataFunction = async()=>{
    await this.setState({
      todayList: this.state.logsList
    }, async() => {
      await this.setState({
        loading: false
      })
    })
  }

  updateDate = async(date, ctx) =>{
    // var timestamp = Date.parse(date);
    // timestamp -= new Date().getTimezoneOffset() * 60 * 1000;

    await this.setState({
      selectedDate : date,
      todayList:[],
      loading: true
    },async()=>{
      await this.setState({
        sameDay: this.state.currentFormattedDate === this.state.selectedDate
      },async()=>{
        ctx.changeDate(this.state.selectedDate)
        if (this.state.currentFormattedDate<this.state.selectedDate) {
          ctx.toggleClickable(false)
        } else{
          ctx.toggleClickable(true)
        }
        await this.displayDataFunction()
      })
    })
  }

  setModal = ()=>{
    this.setState(prevState=>({
      modalVisible: !prevState.modalVisible
    }))
  }

  endModify = async() =>{
    await this.setState({logsList: [], todayList: []});
    await this.setModal()
    await this.getDataFromBdd()
  }

  endSave = async() =>{
    await this.setState({logsList: [], todayList: []});
    await this.getDataFromBdd()
  }

  update = (idClient, isStarted) =>{
    this.setState({idClient: idClient, isStarted: isStarted});
  }


  render() {
      let {activities, currentDate, modalVisible, tested, openQuestion, currentFormattedDate, sameDay, selectedDate, logged, todayList, loading}= this.state

      const items = this.state.todayList.map((item, i) =>{
        return (<TaskToAdd setModal={this.setModal} key={i} index={i} show={true} task={item} label={false} peopleTasks={this.state.dayData}/>)
      });

      const openNews = () => {
        this.props.navigation.navigate("NewsStack", {screen: "NewsMain", params:{ news: this.state.news}})
      }

      // console.log("selected date")
      // console.log(selectedDate)
      // console.log("current formatted date")
      // console.log(currentFormattedDate)
      //
      // console.log(selectedDate.getDay())
      // console.log(selectedDate.getDate())
      // console.log(selectedDate.getMonth())

      // Display the messages when there is no entry on the day selected
      let disPlayEmpty = () =>{
          if (this.state.todayList.length === 0 && !this.state.loading) {
            if (selectedDate > currentFormattedDate) {
              return(
                <View style={styles.noData}>
                  <Image
                    source={require('./../../assets/images/futur.png')}
                    resizeMode={'contain'}
                    style={styles.imageIllu}
                  />
                  <Text style={styles.description}>
                      Seems like you’re in the future!{'\n'}
                      Wait a little bit before adding your activities.
                  </Text>
                </View>
              )
            }else{
              if (selectedDate < currentFormattedDate) {
                return(
                  <View style={styles.noData}>
                    <Image
                      source={require('./../../assets/images/past.png')}
                      resizeMode={'contain'}
                      style={styles.imageIllu}
                    />
                    <Text style={styles.description}>
                        Seems like you’re back in the past!{'\n'}
                        What nice things have you done?{'\n'}
                        Tap the "+" icon to complete your logbook!
                    </Text>
                  </View>
                )
              }else{
                return(
                  <View style={styles.noData}>
                    <Image
                      source={require('./../../assets/images/noActivity.png')}
                      resizeMode={'contain'}
                      style={styles.imageIllu}
                    />
                    <Text style={styles.description}>
                        Hello, good to see you :{')\n'}
                        You didn’t record any activities for today.
                        Tap the "+" icon to complete your logbook!
                    </Text>
                  </View>
                )
              }
            }
          }
      }

      const config = {
        velocityThreshold: 0.3,
        directionalOffsetThreshold: 80
      };

    return (
        <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
          {
            logged &&
            <>
              {!loading && modalVisible && <ModifyModal updateData={this.endModify} workList={this.state.todayList} visible={modalVisible} setModal={this.setModal}/>}
              {/* Get the global context for the modal where you record your activities  */}
              <Consumer>
                {
                  context=>{
                    return(
                      <>
                        {context.opened && <TasksModal updateData={this.endSave} date={context.date} logsList={[]} visible={context.opened} setModal={context.toggleOpening}/>}
                      </>
                    )
                  }
                }
              </Consumer>
              <SafeAreaView style={styles.wrapperContent}>
                  <View style={{flexDirection: 'row'}}>
                  <HeaderMain sameDay={sameDay} dayLabel={days[selectedDate.getDay()]} day={selectedDate.getDate()} month={selectedDate.getMonth()}/>
                  <TouchableOpacity
                    style={styles.newsIcon}
                    onPress={openNews}
                  >
                  <Image
                    source={require('./../../assets/images/bell.png')}
                    resizeMode={'contain'}
                    style={styles.newsIcon}

                  />
                  </TouchableOpacity>
                  {this.state.unreadCount > 0 &&
                    <Text style={styles.unreadCount}> {this.state.unreadCount} </Text>
                  }
                  </View>
                      <Text style={styles.activitiesNumber}>
                          {
                              this.state.todayList.length === 0 ? 'No activities' : this.state.todayList.length === 1 ? `${this.state.todayList.length} activity`:`${this.state.todayList.length} activities`
                          }
                      </Text>

                  <View style={styles.content}>
                    <Consumer>
                        {
                          context=>{
                            return(
                              <>
                                <CalendarStrip
                                  selectedDate= {this.state.selectedDate}
                                  onPressDate={async(date) => {
                                    await this.setState({logsList: [], todayList: []});
                                    await this.updateDate(date, context)
                                    await this.getDataFromBdd()
                                  }}
                                  onPressGoToday={async(today) => {
                                    await this.setState({logsList: [], todayList: []});
                                    await this.updateDate(today, context)
                                    await this.getDataFromBdd()
                                  }}
                                  /* onSwipeDown={() => {
                                    alert('onSwipeDown');
                                  }} */
                                  markedDate={[]}
                                  weekStartsOn={1} // 0,1,2,3,4,5,6 for S M T W T F S, defaults to 0
                                />
                                <GestureRecognizer
                                    onSwipeLeft={async(state) => {
                                      await this.setState({logsList: [], todayList: []});
                                      await this.onSwipeLeft(context)
                                      await this.getDataFromBdd()
                                    }}
                                    onSwipeRight={async(state) => {
                                      await this.setState({logsList: [], todayList: []});
                                      await this.onSwipeRight(context)
                                      await this.getDataFromBdd()
                                    }}
                                    config={config}
                                    style={{
                                      flex: 1,
                                      zIndex: 1
                                    }}
                                    >
                                      <TouchableWithoutFeedback>
                                        <ScrollView style={styles.scroll} contentContainerStyle={styles.containerScrollview}>
                                          <TouchableOpacity style={styles.refresh} onPress={this.getDataFromBdd}>
                                            {
                                              this.state.loading ? <ActivityIndicator size="small" color={colors.primaryButton} />:<Image
                                              source={require('./../../assets/icons/refresh.png')}
                                              resizeMode={'contain'}
                                              style={styles.icon}
                                            />
                                            }
                                          </TouchableOpacity>
                                          {items}
                                          {disPlayEmpty()}
                                        </ScrollView>
                                      </TouchableWithoutFeedback>

                                  </GestureRecognizer>

                              </>
                            )
                          }
                        }
                    </Consumer>
                  </View>

              </SafeAreaView>
            </>
          }
      </View>
    );
  }
}

export default HomeScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  bluetoothWrapper:{
    position: 'absolute',
    top: isIphoneXorAbove() ? 55 : 20,
    right: 20,
    zIndex: 100
  },
  bluetooth:{
    width: 27,
    height:30,
  },
  content: {
    minWidth: screen.w,
  },
  containerScrollview:{
    paddingTop: 30,
    paddingHorizontal: 25,
    paddingBottom: 300,
  },
  scroll:{
    width: screen.w,
    zIndex: 1,
  },
  activitiesNumber:{
    fontFamily: 'Montserrat-Regular',
    paddingHorizontal: 50,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17,
    color: colors.lightGrey,
  },
  imageIllu: {
    height: screen.h*.22,
    width: screen.w - 80,
    marginBottom: 0,
    alignSelf: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  description: {
    fontFamily: 'Montserrat-Medium',
    paddingHorizontal: 5,
    marginTop: 30,
    textAlign: 'center',
    lineHeight: 21,
    fontSize: 14,
    color: colors.lightGrey,
  },
  refresh:{
    alignSelf: 'flex-end',
    marginBottom: 5,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
  icon:{
    width: 20,
    height: 20,
  },
  unreadCount: {
    height: 20,
    width: 20,
    marginBottom: 0,
    alignSelf: 'flex-end',
    textAlign:"center",
    color:"#fff",
    marginLeft: 'auto',
    marginRight: 60,
    marginBottom: 20,
    backgroundColor: "#ee2200",
    borderRadius:5
  },
  newsIcon: {
    height: 30,
    width: 30,
    marginBottom: 0,
    alignSelf: 'flex-end',
    marginLeft: 'auto',
    marginRight: 20,
  }
});
