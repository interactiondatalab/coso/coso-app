import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../global/constants';
import {HeaderMain} from './../../components/Global/Headers/HeaderMain';
import NewsSelect from '../../components/News/NewsSelect';
import { getAllNews } from '../../services/api/news/functions';
import { isIphoneXorAbove } from '../../global/functions';

class NewsMainScreen extends React.Component {

  constructor(props){
    super(props)
    console.log(props.route.params)
    this.state={
      news: props.route.params.news,
      loading: props.route.params.news.length === 0 ? true : false
    }
  }

  componentDidMount(){
    getAllNews().then(
      res=>{
        if (res.status===200) {
          const tmp = res.data
          tmp.reverse()
          this.setState({
            news: tmp,
            loading: false
          })
        }
      }
    )
    .catch(error => {
      this.setState(
        {loading: false}
      )
    })
  }

  openNews = (news) => {
    this.props.navigation.navigate('NewsWebView', {
      newsId: news.id
    })
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {

    let newsList = this.state.news.map((news,i)=> {
      return(
        <NewsSelect openNews={() => {this.openNews(news)}} news={news}/>
      )
    })

    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView style={[styles.wrapperContent, {justifyContent: 'space-between'}]}>
          <ScrollView style={styles.scroll} contentContainerStyle={{justifyContent: 'space-between', alignItems: 'center', minHeight: screen.h*.75}}>
            <TouchableOpacity onPress={this.goBack} style={styles.backPress}>
              <Image
                source={require('./../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
            <View>
              <HeaderMain title="News" />
              <View style={styles.content}>
                  <Image
                    source={require('./../../assets/images/heyThere.png')}
                    resizeMode={'contain'}
                    style={styles.imageIllu}
                  />
                  <Text style={styles.title}>
                    {this.state.news.length === 0 ? 'Nothing new here for now.':'Hey there, here are the \nnews.'}

                  </Text>
                  <Text style={styles.description}>
                      The CoSo team will inform you of new surveys, new insights, as well as general information on the study of iGEM team here.
                  </Text>


              </View>
            </View>
            <View style={styles.selectSurveysWrapper}>

                {
                  this.state.loading ?
                  <View style={styles.wait}>
                    <ActivityIndicator size="small" color={colors.primaryButton}/>
                  </View>
                  :newsList
                }

            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

export default NewsMainScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  content: {
    width: screen.w,
    marginTop: screen.h / 15,
  },
  wait:{
    width: screen.w,
    paddingHorizontal: 20,
    paddingVertical: 15,
    alignItems: 'center',
    marginBottom: 5
  },
  scroll:{
    width: screen.w,
    position: 'relative',
  },
  imageIllu: {
    height: 170,
    width: screen.w - 80,
    marginBottom: 40,
    alignSelf: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  description: {
    fontFamily: 'Montserrat-Medium',
    paddingHorizontal: 50,
    marginVertical: 30,
    textAlign: 'center',
    lineHeight: 21,
    fontSize: 14,
    color: colors.lightGrey,
  },
  selectSurveysWrapper:{
      marginBottom: 30
  },
  backPress: {
      position: 'absolute',
      top: isIphoneXorAbove() ? 50 : 20,
      left: 20,
      padding: 10,
      zIndex: 10
  },
  backButton: {
    width: 11,
    height: 20,
  },
});
