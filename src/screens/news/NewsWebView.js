import React, { Component, useState, useEffect } from 'react';
import { WebView } from 'react-native-webview';
import { AsyncStorage, Text, Linking } from 'react-native';

class SurveyWebView extends Component {
  constructor(props){
      super(props)
      this.state = {
          newsId: props.route.params.newsId,
          view: <Text>Loading...</Text>,
          token: "",
          frontend_url: "https://coso-frontend.herokuapp.com"
          // frontend_url: "http://10.0.2.2:3000"
      }
  }

  _retrieveData = async () => {
    try {
      const token = await AsyncStorage.getItem('@user');
      if (token !== null) {
        // We have data!!
        const url = `${this.state.frontend_url}/views/news/${this.state.newsId}?token=${token.replace("Bearer ", "")}`;
        this.setState({token: token})
        this.setState({view:
          <WebView
            ref={ref => (this.webview = ref)}
            style={{backgroundColor:"#e4f5f6"}}
            source={{ uri: url }}
            onMessage={event => {
              if (event.nativeEvent.data === "back") {
                this.props.navigation.navigate("NewsMain")
              }
              if (event.nativeEvent.data.includes("openURL")) {
                const action = JSON.parse(event.nativeEvent.data)
                Linking.openURL(action.openURL)
              }
            }}
          />
        })
        console.log(`${this.state.frontend_url}/views/news/${this.state.newsId}?token=${token.replace("Bearer ", "")}`)
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount(){
    this._retrieveData();
  }


  render() {
    return (
      this.state.view
    );
  }
}

export default SurveyWebView
