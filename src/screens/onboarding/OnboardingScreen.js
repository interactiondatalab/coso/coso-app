import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  SafeAreaView,
  Image,
  Platform,
  AsyncStorage,
  TouchableOpacity
} from 'react-native';
import colors, {defaultScreenStyles, screen, backButtonStyles} from './../../global/constants';
import Swiper from 'react-native-swiper';
import slides from './../../components/OnBoarding/OnBoardingContent';
import PlainButton from './../../components/Global/Buttons/PlainButton';
import {request, PERMISSIONS} from 'react-native-permissions';
import { ScrollView } from 'react-native-gesture-handler';

export class OnboardingScreen extends React.Component {
  constructor(props){
    super(props)
    this.state={
      fromAccount: false
    }
  }
  componentDidMount(){
    console.log(this.props.route.params)
    if (this.props.route.params === undefined) {
    } else{
      if (this.props.route.params.account === true) {
        this.setState({
          fromAccount: true
        })
      }
    }
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  join = async () => {
    try {
      await AsyncStorage.setItem('@tuto_seen', 'true');
      this.props.navigation.navigate('AuthLoading');
    } catch (e) {
      // saving error
    }
  };

  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView style={styles.wrapperContent}>
          {
            this.state.fromAccount &&
            <TouchableOpacity onPress={this.goBack} style={styles.backPress}>
              <Image
                source={require('./../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
          }

          <Swiper
            loop={false}
            showsButtons={false}
            dotColor={colors.dotColor}
            activeDotColor={colors.activeDot}
            paginationStyle={{bottom: 10}}>
              {/* slides contains the slides content to show */}
            {slides.map(({image, title, subtitle, buttonLabel}, key) => (
              <View key={key} style={styles.card}>
                <Image
                  source={image}
                  resizeMode={'contain'}
                  style={styles.imageIllu}
                />
                <Text style={styles.title}>{title}</Text>
                <ScrollView contentContainerStyle={{paddingBottom: 100}}>
                  <Text style={styles.description}>{subtitle}</Text>
                </ScrollView>
                {buttonLabel && !this.state.fromAccount && (
                  <PlainButton customStyle={{bottom: 30}} toDo={this.join} label={buttonLabel} />
                )}
              </View>
            ))}
          </Swiper>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  ...backButtonStyles,
  skipContainer: {
    alignSelf: 'flex-end',
    marginTop: 35,
    marginRight: 40,
    marginBottom: screen.h / 13,
  },
  skip: {
    fontFamily: 'Montserrat-Medium',
    color: colors.black,
  },
  card: {
    flex: 1,
    width: screen.w,
    //minHeight: screen.h - screen.h / 4,
    paddingTop: screen.h/13
  },
  imageIllu: {
    height: screen.h/3,
    width: screen.w - 80,
    marginBottom: 40,
    alignSelf: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  description: {
    fontFamily: 'Montserrat-Regular',
    paddingHorizontal: 50,
    marginTop: 20,
    textAlign: 'center',
    lineHeight: 30,
    fontSize: 17,
    color: colors.lightGrey,
  },
});
