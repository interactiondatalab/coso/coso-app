import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  AsyncStorage,
  Linking,
  Alert,
  TouchableWithoutFeedback
} from 'react-native';
import colors, {defaultScreenStyles, screen, backButtonStyles} from '../../global/constants';
import {HeaderMain} from '../../components/Global/Headers/HeaderMain';
import CustomInput from './../../components/Global/TextInput/CustomInput';
import ItemMyAccount from '../../components/Settings/ItemMyAccount';
import LittleButton from '../../components/Global/Buttons/LittleButton';
import { CommonActions, Link } from '@react-navigation/native';
import { connectedChangePassword } from '../../services/api/signup/functions';

class MyAccountScreen extends React.Component {

  constructor(props){
    super()
    this.state={
      id: '',
      username:'',
      teamName:'',
      email: '',
      mdp: '',
      changing: false,
      loading: false
    }
  }

  componentDidMount(){
    this.getDatas()
  }

  getDatas = async() => {
    await AsyncStorage.getItem('@myInfos').then(
      (value)=>{
        if (value !== null) {
          let val = JSON.parse(value)
          console.log(val.id)
          this.setState({
            id: val.id,
            username: val.username,
            email: val.email,
            teamName: val.team.name
          })
        }
      }
    )
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  editFunction = () =>{
    alert('Edition')
  }

  changePassword = () =>{
    this.setState({
      changing: true
    })
  }

  cancelPassword = () =>{
    this.setState({
      changing: false
    })
  }

  validate = async() =>{
    this.setState({
      loading: true
    })
    await connectedChangePassword(this.state.id, this.state.mdp).then(
      val => {
        if(val.status === 200){
          Alert.alert('Password', 'Your password has been updated.')
          this.setState({
            changing: false
          })
        } else{
          Alert.alert('Password', `Your password ${val.data.password[0]}`)
        }
        this.setState({
          loading: false
        })
      }
    ).catch(
      error=>{
        this.setState({
          loading: false
        })
        Alert.alert('Password', 'An error occured, please retry later.')
      }
    )
  }

  updateState = (stateLabel, value) => {
    this.setState({
      [stateLabel]: value,
    });
  };

  render() {
    let {username, email, teamName, changing, loading} = this.state
    return (
      <TouchableWithoutFeedback style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView style={[styles.wrapperContent, {backgroundColor: colors.paleGrey}]}>
          <TouchableOpacity onPress={this.goBack} style={styles.backPress}>
              <Image
                source={require('./../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
          <HeaderMain title="My account" />
          <ScrollView style={styles.scroll} contentContainerStyle={{paddingBottom: 200}} showsVerticalScrollIndicator={false}>
            <ItemMyAccount editable={false} label={'Username'} content={username} edit={'Edit'} change={this.editFunction}/>
            <ItemMyAccount editable={false} label={'Team name'} content={teamName} edit={'Edit'} change={this.editFunction}/>
            <ItemMyAccount editable={false} label={'Email'} content={email} edit={'Edit'} change={this.editFunction}/>
            {
              changing ? 
              <View style={styles.password}>
                <CustomInput
                  placeholder={'Password'}
                  stateName={'mdp'}
                  update={this.updateState}
                  customStyle={{marginBottom: 0, width: screen.w- 50}}
                  label={'Password'}
                  password={true}
                />
                <View style={styles.buttons}>
                  <TouchableOpacity style={styles.button} onPress={this.cancelPassword}>
                      <Text style={styles.textButton}>Cancel</Text>
                  </TouchableOpacity>
                  <LittleButton
                    loading={loading}
                    toDo={this.validate}
                    label={'Save'}
                    customStyle={{alignSelf:'flex-end'}}
                  />
                </View>
                
              </View>
              :<ItemMyAccount editable={true} noLabel={true} content={'PASSWORD'} edit={'Change'} change={this.changePassword}/>
            }
          </ScrollView>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    );
  }
}

export default MyAccountScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  ...backButtonStyles,
  content: {
    width: screen.w,
    marginTop: screen.h / 15,
  },
  scroll:{
    marginTop: screen.h/15
  },
  buttons:{
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20, 
  },
  password:{
    marginRight: 25,
    alignItems: 'flex-end',
    marginTop: 10
  },
  button:{
    marginRight: 15,
  },
  textButton:{
    fontSize: 15,
    fontWeight: '500',
    color: colors.primaryButton,
  },
});
