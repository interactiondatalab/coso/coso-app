import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  AsyncStorage,
  Linking,
  Alert
} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../global/constants';
import {HeaderMain} from './../../components/Global/Headers/HeaderMain';
import ItemSettings from '../../components/Settings/ItemSettings';
import { CommonActions, Link } from '@react-navigation/native';
import { modifyTokenPhone } from '../../services/api/phones/functions';

class SettingsScreen extends React.Component {

  removeItemValue= async(key) => {
    try {
        await AsyncStorage.removeItem('@myInfos');
        await AsyncStorage.removeItem('@logsList');
        await AsyncStorage.removeItem('@user');
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch(exception) {
      console.error(exception)
        return false;
    }
  }

  logout = async() => {
    if (this.removeItemValue('@user')) {
      let phoneId = await AsyncStorage.getItem("@phone_id");
      await modifyTokenPhone(phoneId, "");

      this.props.navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            { name: 'AuthLoading' }
          ],
        })
      );
    }else{
      Alert.alert("Error", "An error occured, please retry.")
    }
    //app.logout();
  }

  instagram = () =>{
    Linking.openURL('instagram://user?username=igem.ties')
    .catch(
      Linking.openURL('https://www.instagram.com/igem.ties/')
    )
  }

  twitter = () =>{
    Linking.openURL('twitter://IgemTies')
    .catch(
      Linking.openURL('https://twitter.com/IgemTies')
    )
  }

  privacy = () =>{
    Linking.openURL('https://igem.org/Privacy_Policy')
  }

  coso = () =>{
    Linking.openURL('https://igem-ties.info/')
  }

  contact = () =>{
    Alert.alert(
      "Contact us",
      "To give us some feedbacks or to ask something, you can send us an email at igem-ties@cri-paris.org",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Open mail", onPress: () => Linking.openURL(`mailto:igem-ties@cri-paris.org`)}
      ],
      { cancelable: false }
    );
  }

  account = () =>{
    this.props.navigation.navigate('MyAccount')
  }

  how = () =>{
    this.props.navigation.navigate('HowWorks', {account: true})
  }

  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView style={styles.wrapperContent}>
          <HeaderMain title="Settings" />
          <ScrollView style={styles.scroll} contentContainerStyle={{paddingBottom: 100}} showsVerticalScrollIndicator={false}>
            <ItemSettings press={this.account} icon={require('./../../assets/icons/account.png')} label={'My account'}/>
            <ItemSettings press={this.logout} icon={require('./../../assets/icons/power.png')} label={'Logout'} without={true}/>
            <View style={{marginVertical: 10}}/>
            <ItemSettings press={this.how} icon={require('./../../assets/icons/help.png')} label={'How it works'}/>
            <ItemSettings press={this.contact} icon={require('./../../assets/icons/contact.png')} label={'Contact us'} without={true}/>
            <View style={{marginVertical: 10}}/>
            <ItemSettings press={this.instagram} icon={require('./../../assets/icons/instagram.png')} label={'Follow us on Instagram'} without={true}/>
            <ItemSettings press={this.twitter} icon={require('./../../assets/icons/twitter.png')} label={'Follow us on Twitter'} without={true}/>
            <View style={{marginVertical: 10}}/>
            <ItemSettings press={this.coso} icon={require('./../../assets/icons/info.png')} label={'About iGEM TIES'} without={true}/>
            <ItemSettings press={this.privacy} icon={require('./../../assets/icons/shield.png')} label={'Privacy Policy'} without={true}/>
            <View style={{marginVertical: 10}}/>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

export default SettingsScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  content: {
    width: screen.w,
    marginTop: screen.h / 15,
  },
  scroll:{
    marginTop: screen.h/15
  }
});
