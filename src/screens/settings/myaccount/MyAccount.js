import React from 'react';
import {AsyncStorage} from 'react-native'
import {Container} from './MyAccount.styled';
import {Item} from '@components/Item';
import { Alert } from 'react-native';
import {HeaderMain} from './../../../components/Global/Headers/HeaderMain';



export const MyAccountScreen = ({navigation}) => {
  let removeItemValue= async(key) =>{
      try {
          await AsyncStorage.removeItem('@myInfos');
          await AsyncStorage.removeItem('@logsList');
          await AsyncStorage.removeItem(key);
          return true;
      }
      catch(exception) {
          return false;
      }
  }
  async function logout() {
    if (removeItemValue('@user')) {
      navigation.navigate('AuthLoading');
    }else{
      Alert.alert("Error", "An error occured, please retry.")
    }
    //app.logout();
  }

  return (
    <Container>
      <HeaderMain title={'My account'} />
      <Item
        title={'Logout'}
        icon="power-settings-new"
        iconColor="red"
        onPress={logout}
      />
    </Container>
  );
};
