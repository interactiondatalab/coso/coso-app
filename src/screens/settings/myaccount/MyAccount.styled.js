import React from 'react';
import styled from 'styled-components/native';
import { isIphoneXorAbove } from '../../../global/functions';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  background-color: rgb(228, 245, 246);
  padding-top: ${isIphoneXorAbove() ? 30:0}

`;
