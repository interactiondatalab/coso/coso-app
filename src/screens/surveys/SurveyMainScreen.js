import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../global/constants';
import {HeaderMain} from './../../components/Global/Headers/HeaderMain';
import SurveySelect from '../../components/Surveys/SurveySelect';
import { getAllSurveys, getIsDone } from '../../services/api/surveys/functions';


class SurveyMainScreen extends React.Component {

  constructor(props){
    super(props)
    this.state={
      surveys:[]
    }
  }

  componentDidMount(){
    setTimeout(
      ()=>{
        getAllSurveys().then(
          res=>{
            if (res.status===200) {
              this.setState({
                surveys: res.data
              })
            }
          }
        )
      },3000
    )
  }

  goToResume = (parametres) =>{
    this.props.navigation.navigate('ResumeScreen', {
      parametres
    })
  }
  render() {

    let surveyList = this.state.surveys.map((survey,i)=>{
      //console.log('survey', survey)
      return(
        i===0 ? <SurveySelect next={this.goToResume} done={false} survey={survey} fields={survey.survey_fields} new={true} title={survey.name} minutes={"12 minutes"} timeToSpend={1}/>
        :<SurveySelect next={this.goToResume} done={false}  survey={survey} fields={survey.survey_fields} new={false} title={survey.name} minutes={"12 minutes"} timeToSpend={1}/>
      )
    })

    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView style={[styles.wrapperContent, {justifyContent: 'space-between'}]}>
          <ScrollView style={styles.scroll} contentContainerStyle={{justifyContent: 'space-between', alignItems: 'center', minHeight: screen.h*.75}}>
            <View>
              <HeaderMain title="Surveys" />
              <View style={styles.content}>
              {
                  // <Image
                  //   source={require('./../../assets/images/heyThere.png')}
                  //   resizeMode={'contain'}
                  //   style={styles.imageIllu}
                  // />
                  // <Text style={styles.title}>
                  // {this.state.surveys.length === 0 ? 'Sorry, there is no surveys \navailable.':'Hey there, a new survey is \navailable.'}
                  //
                  // </Text>
                }
                  <Text style={styles.description}>
                      By completing surveys you will help us understand the mechanics of team performance,
                      so that you can be an inspiration for future iGEM teams!
                  </Text>


              </View>
            </View>
            <View style={styles.selectSurveysWrapper}>

                {
                  surveyList.length === 0 ?
                  <View style={styles.wait}>
                    <ActivityIndicator size="small" color={colors.primaryButton}/>
                  </View>
                  :surveyList
                }

            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}

export default SurveyMainScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  content: {
    width: screen.w,
    marginTop: screen.h / 50,
  },
  wait:{
    width: screen.w,
    paddingHorizontal: 20,
    paddingVertical: 15,
    alignItems: 'center',
    marginBottom: 5
  },
  scroll:{
    width: screen.w,
    position: 'relative',
  },
  imageIllu: {
    height: 170,
    width: screen.w - 80,
    marginBottom: 40,
    alignSelf: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  description: {
    fontFamily: 'Montserrat-Medium',
    paddingHorizontal: 50,
    marginVertical: 30,
    textAlign: 'center',
    lineHeight: 21,
    fontSize: 14,
    color: colors.lightGrey,
  },
  selectSurveysWrapper:{
      marginBottom: 30
  },
  selectSurveys:{

  }
});
