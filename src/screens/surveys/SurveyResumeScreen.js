import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  SafeAreaView,
  Image,
  ScrollView
} from 'react-native';
import colors, {defaultScreenStyles, screen} from './../../global/constants';
import {HeaderMain} from './../../components/Global/Headers/HeaderMain';
import SurveySelect from '../../components/Surveys/SurveySelect';
import PrimaryButton from '../../components/Global/Buttons/PrimaryButton';
import LittleButton from '../../components/Global/Buttons/LittleButton';
import PlainButton from '../../components/Global/Buttons/PlainButton';
import { isIphoneXorAbove } from '../../global/functions';

class SurveyResumeScreen extends React.Component {

  constructor(props){
    super(props)
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  startSurvey = () =>{
    this.props.navigation.navigate('Survey', {
      surveyData: this.props.route.params.parametres.data
    })
  }

  render() {
    return (
      <View style={[styles.container, {backgroundColor: colors.paleGrey}]}>
        <SafeAreaView style={[styles.wrapperContent, {justifyContent: 'space-between'}]}>
          <TouchableOpacity onPress={this.goBack} style={styles.backPress}>
              <Image
                source={require('./../../assets/icons/arrowBack.png')}
                resizeMode={'contain'}
                style={styles.backButton}
              />
            </TouchableOpacity>
            <View>
              <HeaderMain title={this.props.route.params.parametres.title} />
              <ScrollView showsVerticalScrollIndicator={false} style={styles.scroll} contentContainerStyle={{justifyContent: 'space-between', alignItems: 'center', minHeight: screen.h*.7, paddingBottom: 100}}>

                <View style={styles.content}>

                    <Text style={styles.label}>
                      {this.props.route.params.parametres.date}
                    </Text>
                    <Image
                      source={require('./../../assets/images/robot.png')}
                      resizeMode={'contain'}
                      style={styles.imageIllu}
                    />
                    <Text style={styles.title}>Thanks for taking the time to{'\n'}answer this survey.</Text>
                    <Text style={styles.description}>
                      Please make sure you have a few minutes ahead so that you can complete it in one streak!
                      {'\n\n'}
                      Make sure to submit at the end of the survey! Your answers will help us better understand the mechanics of team performance.
                    </Text>

                </View>
                <PrimaryButton toDo={this.startSurvey} label={"Let's start"} />

                </ScrollView>

            </View>

        </SafeAreaView>
      </View>
    );
  }
}

export default SurveyResumeScreen;

const styles = StyleSheet.create({
  ...defaultScreenStyles,
  content: {
    width: screen.w,
    marginTop: screen.h / 35,
  },
  scroll:{
    width: screen.w,
    position: 'relative',
  },
  imageIllu: {
    height: screen.h*.2,
    width: screen.w - 80,
    marginBottom: 40,
    alignSelf: 'center',
  },
  label:{
    marginBottom: 40,
    fontFamily: 'Montserrat-Medium',
    color: colors.inactiveText,
    marginLeft: 25,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingHorizontal: 30,
    color: colors.titleBlack,
  },
  description: {
    fontFamily: 'Montserrat-Medium',
    paddingHorizontal: 50,
    marginVertical: 30,
    textAlign: 'center',
    lineHeight: 21,
    fontSize: 14,
    color: colors.lightGrey,
  },
  selectSurveysWrapper:{
      marginTop: screen.h/8
  },
  selectSurveys:{

  },
  backPress: {
    position: 'absolute',
    top: isIphoneXorAbove() ? 50 : 20,
    left: 20,
    padding: 10,
    zIndex: 10
  },
  backButton: {
    width: 11,
    height: 20,
  },
});
