import React, { Component, useState, useEffect } from 'react';
import { WebView } from 'react-native-webview';
import { AsyncStorage, Text } from 'react-native';

class SurveyWebView extends Component {
  constructor(props){
      super(props)
      this.state = {
          survey: props.route.params.surveyData,
          view: <Text>Loading...</Text>,
          token: "",
          frontend_url: "https://coso-frontend.herokuapp.com"
          // frontend_url: "http://10.0.2.2:3000"
      }
  }

  _retrieveData = async () => {
    try {
      const token = await AsyncStorage.getItem('@user');
      if (token !== null) {
        // We have data!!
        this.setState({token: token})
        this.setState({view:
          <WebView
            ref={ref => (this.webview = ref)}
            style={{backgroundColor:"#e4f5f6"}}
            source={{ uri: `${this.state.frontend_url}/views/surveys/${this.state.survey.id}?token=${token.replace("Bearer ", "")}` }}
            onMessage={event => {
              if (event.nativeEvent.data === "close") {
                this.props.navigation.navigate("SurveyMain")
              }
              if (event.nativeEvent.data === "back") {
                this.props.navigation.navigate("SurveyMain")
              }
            }}
          />
        })
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount(){
    this._retrieveData();
  }


  render() {
    return (
      this.state.view
    );
  }
}

export default SurveyWebView
