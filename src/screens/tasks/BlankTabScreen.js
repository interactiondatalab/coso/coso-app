import React, {useEffect} from 'react';
import { View, Text } from 'react-native';

const BlankTab = ({ navigation }) =>{
    React.useEffect(() => {
      const unsubscribe = navigation.addListener('tabPress', e => {
        // Prevent default behavior
        e.preventDefault();

        // Do something manually
        // ...
        
      });
  
      return unsubscribe;
    }, [navigation]);
  
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Blank</Text>
      </View>
    );
}

export default BlankTab