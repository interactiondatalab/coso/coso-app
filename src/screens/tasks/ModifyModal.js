/*
* Same modal than the activity modal but for modifying.
* Had to do it because, this one is not using a context
* And we need to pass the infos that we are modifying to all the components needed to be able to
* re-use the one we used for the activity record.
*/


import React from 'react';
import Modal from 'react-native-modalbox';

import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Alert
} from 'react-native';

import colors, {screen} from './../../global/constants'
import { isIphoneXorAbove } from '../../global/functions';
import Introduction from '../../components/TaskRecords/Introduction';
import WorksSelection from '../../components/TaskRecords/WorksSelection';
import AddPeople from '../../components/TaskRecords/AddPeople';
import AddPeopleToTask from '../../components/TaskRecords/AddPeopleSupplies/AddPeopleToTask';
import {getAllTasks, registerTask, modifyTask} from './../../services/api/tasks/functions'
// import AsyncStorage from '@react-native-community/async-storage';

export default class ModifyModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3,
      workDid: [],
      step: 0,
      allTasks:[],
      tasksList: []
    };
  }

  componentDidMount = async()=>{
  }

  onClose = ()=>{
    this.props.setModal()
  }

  next = ()=>{
    this.setState(prevState=>({
      step: prevState.step+1
    }))
  }

  previous = ()=>{
    this.setState(prevState=>({
      step: prevState.step-1
    }))
  }

  updateWork = (list) =>{
    this.setState({
      workDid: list
    })
    this.next()
  }

  registerTask = (tasksInfos) =>{
    this.setState({
      allTasks: tasksInfos
    }, ()=>{
      Object.keys(this.state.allTasks).map(async(element)=>{
          this.props.workList.map(async(task)=>{
              if (task.id == element) {
                  console.log(this.state.allTasks[element])
                let result = await modifyTask(task.id, this.state.allTasks[element], task.task_done_at).then(
                    async(res)=>{
                        if (res.status===200) {
                            console.log(res.data)
                            //Alert.alert("Error", "Your modifications may has not been saved, sorry about that.")
                        }
                    }
                )
                .catch(
                    e=>Alert.alert("Error", "An error occured, please retry")
                )
              }
          })
      })
      setTimeout(
          ()=>{
            this.props.updateData()
          },1000
      )
    })
  }

  render() {

    return (
      <View style={styles.wrapper}>

        <Modal
            propagateSwipe={true}
            coverScreen={true}
            animationType={'fade'}
            isOpen={this.props.visible}
            style={[styles.modal, styles.wrapper]}
            backdropOpacity={.32}
            swipeToClose={true}
            onClosed={this.onClose}>
            <View style={styles.content}>
              <View style={styles.knob}/>
              {this.props.workList &&
              <AddPeople
                modifying={true}
                noPrevious={true}
                workList={this.props.workList}
                next={this.next}
                back={this.previous}
                registerTask={this.registerTask}/>}
            </View>
        </Modal>
      </View>
    );

  }

}

const styles = StyleSheet.create({

  wrapper: {
    width: screen.w,
    height: screen.h,
    flex: 1,
    position: 'absolute',
    zIndex: 1000000,
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
  },
  content:{
    backgroundColor: colors.paleGrey,
    width: screen.w,
    height: screen.h,
    alignItems: 'center',
    zIndex: 1000000,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1000000,
  },
  knob:{
    width: 50,
    height:4,
    borderRadius: 2,
    backgroundColor: colors.borderColor,
    marginTop: 10,
    marginBottom: 35
  },
});
