import React from 'react';
import Modal from 'react-native-modalbox';

import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import colors, {screen} from './../../global/constants'
import { isIphoneXorAbove } from '../../global/functions';
import Introduction from '../../components/TaskRecords/Introduction';
import WorksSelection from '../../components/TaskRecords/WorksSelection';
import AddPeople from '../../components/TaskRecords/AddPeople';
import AddPeopleToTask from '../../components/TaskRecords/AddPeopleSupplies/AddPeopleToTask';
import AddTask from '../../components/TaskRecords/AddTask';
import {getAllTasks, registerTask} from './../../services/api/tasks/functions'
import moment from 'moment';
// import AsyncStorage from '@react-native-community/async-storage';

export default class TasksModal extends React.Component {

  constructor(props) {
    super(props);

    console.log("Taskmoal PROPS! !!!")
    console.log(props.logsList)
    this.state = {
      isOpen: true,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3,
      workDid: props.logsList,
      step: 0,
      allTasks:[],
      tasksList: [],
    };
  }



  componentDidMount = async()=>{
    await this.refreshTasks();
    const value = await AsyncStorage.getItem('@skipIntro')
    console.log({value})
    console.log(value === "true")
    if (value === "true") {
      this.next();
    }
  }

  refreshTasks = () => {
    let result = getAllTasks().then(
      (res)=>{
        console.log("Liste:")
        console.log(this.state.workDid)
        console.log(res.data)
        this.setState({
          tasksList: res.data
        })
      }
    )
  }

  onClose = ()=>{
    this.props.setModal()
  }

  next = ()=>{
    this.setState(prevState=>({
      step: prevState.step+1
    }))
  }

  setStep = (step) => {
    this.setState({step: step})
  }

  previous = ()=>{
    this.setState(prevState=>({
      step: prevState.step-1
    }))
  }

  updateWork = (list) =>{
    this.setState({
      workDid: list
    })
    this.next()
  }

  registerTask = async (tasksInfos) =>{
    this.setState({
      allTasks: tasksInfos
    }, async()=>{
      Object.keys(this.state.allTasks).map(async(element)=>{
        let result = await registerTask(element, this.state.allTasks[element], moment(this.props.date).format("YYYY-MM-DD 00:00:00")).then(
          async(res)=>{
            /* await AsyncStorage.getItem('@logs').then(async (value) => {
              if (value !== null) {
                let logsArray = JSON.parse(value)
                logsArray.push(res.data.id)
                console.log(logsArray)
                await AsyncStorage.setItem('@logs', JSON.stringify(logsArray));
              }else{
                await AsyncStorage.setItem('@logs', JSON.stringify([res.data.id]));
              }
            }); */
          }
        )

      })
      setTimeout(
        ()=>{
          this.props.updateData()
        },1000
      )
    })



  }

  getPart = () =>{
    switch (this.state.step) {
      case 0:
        return <Introduction next={this.next} workList={this.state.workDid}/>
        break;
      case 1:
        return <WorksSelection workList={this.state.workDid}  tasksList={this.state.tasksList} initialList={this.state.workDid} updateWork={this.updateWork} next={this.next} setStep={this.setStep}/>
        break;
      case 2:
          return <AddPeople workList={this.state.workDid} next={this.next} back={this.previous} registerTask={this.registerTask}/>
          break;
      case 3:
        return <AddTask tasksList={this.state.tasksList} setStep={this.setStep} refreshTasks={this.refreshTasks}/>
        break;

      default:
        break;
    }
  }

  render() {

    return (
      <View style={styles.wrapper}>

        <Modal
            propagateSwipe={true}
            isOpen={this.props.visible}
            style={[styles.modal, styles.wrapper]}
            backdropOpacity={.32}
            swipeToClose={true}
            coverScreen={true}
            onClosed={this.onClose}>
            <View style={styles.content}>
              <View style={styles.knob}/>
              {this.getPart()}
            </View>
        </Modal>
      </View>
    );

  }

}

const styles = StyleSheet.create({

  wrapper: {
    width: screen.w,
    height: screen.h,
    flex: 1,
    position: 'absolute',
    zIndex: 1000000,
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
  },
  content:{
    backgroundColor: colors.paleGrey,
    width: screen.w,
    height: screen.h,
    alignItems: 'center'
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1000000,
  },
  knob:{
    width: 50,
    height:4,
    borderRadius: 2,
    backgroundColor: colors.borderColor,
    marginTop: 10,
    marginBottom: 35
  },
});
