import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';
import api from '../index'

export const getUserPhones = async() => {
  return await api(`phones`, "GET");
}

export const postToken = async(phoneId, token) => {
  return await api("phones", "POST", {
    "fcm_device": {
		"phone_id": phoneId,
		"device_id": token
	}
  });
}