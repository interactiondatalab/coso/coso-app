import axios from 'axios';
import { AsyncStorage } from 'react-native';

const API = "https://coso-backend.herokuapp.com";
// const API = "http://10.0.2.2:3000";

const axiosAPI = axios.create({
  baseURL: API,
  timeout: 5000,
});

let userToken = null;

/*
* About requests, sometimes you need the user session token, sometimes you don't
* When you need it your have to pass it in the parameter of your fonction
* If you mention it, we wiil get the token to send it to the server
*/

const api = async (uri, method, data = null, avoidToken=false) => {

  if (avoidToken) {
    userToken = null
  } else{
    await AsyncStorage.getItem('@user').then(async (value) => {
      if (value !== null) {
        userToken = value
        //console.log('Btoken', userToken)
      }
    });
  }

  let result
  await axiosAPI({
    url: `api/${uri}`,
    method,
    headers: {'Content-Type': 'application/json', 'Authorization': userToken },
    data
  }).then((data)=> { result = data; }).catch((e) => { result = e.response;console.log(e) });
  return result
}

export default api
