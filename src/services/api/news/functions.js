import { AsyncStorage } from 'react-native';
import axios from 'axios';
import api from '../index'



export const getAllNews = async() => {
  return await api("news", "GET");
}
