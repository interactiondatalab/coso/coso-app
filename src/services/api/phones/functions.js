import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';
import api from '../index'

export const getUserPhones = async() => {
  return await api(`phones`, "GET");
}

export const modifyTokenPhone = async(id, token) => {
  return await api(`phones/${id}`, "PUT", {
    "device_token": token
  });
}

export const postPhone = async(imei, os, brand, model, token) => {
  return await api("phones", "POST", {
    "imei": imei,
    "os": os,
    "brand": brand,
    "model": model,
    "device_token": token
  });
}
