import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';
import api from '../index'

export const postPing = async(phone_id, record) => {
  let date_record = new Date(record[2]);

  return await api("pings", "POST", {
    "ping": {
      "phone_id": Number(phone_id),
      "target_phone_id": Number(record[0]),
      "rssi": record[1],
      "ping_at": date_record.toJSON()
    }
  });
}
