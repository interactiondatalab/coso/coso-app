import axios from 'axios';
import api from '../index'


export const createRelationCategory = async(name) => {
  return await api("relation_categories", "POST", {
    "relation_categories": {
      "name": name,
    }
  });
}
