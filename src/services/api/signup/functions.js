import { AsyncStorage } from 'react-native';
import axios from 'axios';
import api from '../index'

export const getUserByIgemId = async(igemId) => {
  return await api(`igem_users/exist?username=${igemId}`, "GET");
}

export const getTeamByName = async(teamName) =>{
  return await api(`teams/exist?name=${teamName}`, "GET");
}

export const signUpUSer = async(userData) => {
  return await api("signup", "POST", {
    "user" : userData
  }, true);
}

export const loginUser = async(login, mdp) => {
  return await api("login", "POST", {
    "user": {
      "email": login,
      "password": mdp
    }
  }, true);
}

export const connectedChangePassword = async(id, newPassword) => {
  return await api(`users/${id}`, "PUT", {
    "user": {
      "password": newPassword,
      "password_confirmation": newPassword
    }
  });
}

export const recoverPassword = async(email) => {
  return await api('password', "POST", {
    "user": {
      "email": email
    }
  }, true);
}

export const changePassword = async(token, password, confirmation) => {
  console.log(token, password)
  return await api('password', "PUT", {
    "user": {
      "reset_password_token": token,
      "password": password,
      "password_confirmation": confirmation
    }
  }, true);
}
