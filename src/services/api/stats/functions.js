import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';
import api from '../index'

export const getStatsStreak = async() => {
  return await api(`stats/streak`, "GET");
}

export const getStatsTasks = async() => {
  return await api(`stats/tasks`, "GET");
}

export const getStatsCollaborators = async() => {
  return await api(`stats/collaborators`, "GET");
}

export const getStatsGlobal = async() => {
  return await api(`stats/global`, "GET");
}
