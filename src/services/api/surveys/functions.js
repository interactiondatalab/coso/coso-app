import { AsyncStorage } from 'react-native';
import axios from 'axios';
import api from '../index'



export const getAllSurveys = async() => {
  return await api("surveys", "GET");
}

export const getIsDone = async(id, data) => {
  return await api(`surveys/${id}/is_completed`, "GET");
}

export const postSurvey = async(id, data) => {
  return await api(`surveys/${id}/answer`, "POST", {
    "data": data
  });
}

