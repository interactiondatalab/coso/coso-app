import { AsyncStorage } from 'react-native';
import axios from 'axios';
import api from '../index';
import moment from 'moment';

export const getAllTasks = async() => {
  return await api("tasks", "GET");
}

export const getTask = async(id) => {
  return await api(`tasks/${id}`, "GET");
}

export const deleteLog = async(id) => {
  return await api(`logs/${id}`, "DELETE");
}

export const getLogs = async(selectedDate) => {
  return await api(`logs?current_date=${selectedDate}`, "GET");
}

export const getLog = async(id) => {
  return await api("logs", "GET");
}

export const createTask = async(name, category, description) => {
  return await api("tasks", "POST", {
    "task": {
      "name": name,
      "category": category,
      "description": description,
    }
  });
}

export const registerTask = async(id, task, selectedDate) => {

  let teamId = null;
  let userId = null;

  await AsyncStorage.getItem('@myInfos').then(async (value) => {
    if (value !== null) {
      userId = JSON.parse(value).igem_user.id
      teamId = JSON.parse(value).team.id
    }
  });
  console.log({
    "log": {
      "user_id": 1,
      "task_id": id,
      "why": "thehell",
      "team_id": teamId,
      "task_done_at": selectedDate,
      "task_finished_at": moment(task.task_finished_at).format("YYYY-MM-DD 00:00:00"),
      "igem_users": task.igem_users,
      "users": task.users,
      "relation_categories": task.relation_categories
    }
  })

  return await api("logs", "POST", {
    "log": {
      "user_id": 1,
      "task_id": id,
      "why": "thehell",
      "team_id": teamId,
      "task_done_at": selectedDate,
      "task_finished_at": moment(task.task_finished_at).format("YYYY-MM-DD 00:00:00"),
      "igem_users": task.igem_users,
      "users": task.users,
      "relation_categories": task.relation_categories
    }
  });
}

export const modifyTask = async(id, task, selectedDate) => {

  let teamId = null;
  let userId = null;

  await AsyncStorage.getItem('@myInfos').then(async (value) => {
    if (value !== null) {
      userId = JSON.parse(value).igem_user.id
      teamId = JSON.parse(value).team.id
    }
  });

  return await api(`logs/${id}`, "PUT", {
    "log": {
      "task_done_at": selectedDate.toLocaleString(),
      "task_finished_at": task.task_finished_at,
      "igem_users": task.igem_users,
      "users": task.users,
      "relation_categories": task.relation_categories
    }
  });
}
