import { AsyncStorage } from 'react-native';
import axios from 'axios';
import api from '../index'

export const createIgemUser = async(username) => {
  return await api("igem_users/add", "POST", {
    "igem_user": {
      "username": username
    }
  });
}

export const getTeams = async() => {
  return await api('teams/list');
}

export const existUser = async(username) => {
  return await api(`users/exist?username=${username}`, "GET");
}

export const getUserTeam = async() => {
  let teamId = null;

  await AsyncStorage.getItem('@myInfos').then(async (value) => {
    if (value !== null) {
      teamId = JSON.parse(value).team.id
    }
  });

  return await api(`teams/${teamId}`, "GET");
}

export const getRelationCategories = async() => {
    return await api("relation_categories", "GET");
}
